package data;

import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;

import data.primitives.DataEntry;
import data.primitives.DataEntryType;
import data.primitives.SemesterBasicInfo;
import data.primitives.StudyYear;
import data.primitives.SubjectType;
import data.primitives.YearInfo;

public class SpecializationEntry extends DataEntry {
	
	Map<StudyYear, YearInfo> yearsInfo;
	
	public SpecializationEntry(String val) {
		super(val, DataEntryType.SPECIALIZATION);
		yearsInfo = Collections.synchronizedMap(new EnumMap<StudyYear, YearInfo>(StudyYear.class));
		initYearInfo();
	}
	
	private final void initYearInfo() {
		for (StudyYear sy : StudyYear.values()) {
			yearsInfo.put(sy, new YearInfo());
		}
	}
	
	public YearInfo getYearInfo(StudyYear sy) { return yearsInfo.get(sy); }
	
	public SemesterBasicInfo getSemesterInfo (StudyYear sy, SubjectType st) {
		return yearsInfo.get(sy).getYearBasicInfo(st);
	}
	
	public String toString() { return super.getName(); }
}