package data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.Main;
import data.primitives.DataEntry;
import data.primitives.DataEntryType;
import data.primitives.SemesterBasicInfo;
import data.primitives.SemesterType;
import data.primitives.StudyYear;
import data.primitives.SubjectType;
import data.primitives.YearInfo;
import misc.Utils;

public class DepartmentEntry extends DataEntry {
	private static final Logger logger = LogManager.getLogger();
	
	List<SpecializationEntry> specializations = new ArrayList<SpecializationEntry>();
	List<EmployeeEntry> employees = new ArrayList<EmployeeEntry>();
	
	// https://docs.oracle.com/javase/6/docs/api/java/util/EnumMap.html#EnumMap(java.lang.Class)
	Map<StudyYear, YearInfo> yearsInfo;
	
	public DepartmentEntry(String val) {
		super(val, DataEntryType.DEPARTMENT);
		yearsInfo = Collections.synchronizedMap(new EnumMap<StudyYear, YearInfo>(StudyYear.class));
		initYearInfo();
		addEmptySpec();
	}
	
	void addEmptySpec() {
		specializations.add(misc.Utils.getEmptySpecializationEntry());
	}
	
	private final void initYearInfo() {
		for (StudyYear sy : StudyYear.values()) {
			yearsInfo.put(sy, new YearInfo());
		}
	}
	
	public SemesterBasicInfo getSemesterInfo(StudyYear sy, SubjectType st) {
		return yearsInfo.get(sy).getYearBasicInfo(st);
	}
	
	public boolean addEmployee(EmployeeEntry ee) {
		if (null == ee) throw new IllegalArgumentException("Obiekt pracownika jest pusty");
		return employees.add(ee);
	}
	
	public boolean addEmployee(String name, String Surname, String degree,
			String position, boolean staffResource, long jobTime)
	throws IllegalArgumentException {
		if (null == name || null == Surname) throw new IllegalArgumentException("Brak imienia i nazwiska");
		EmployeeEntry ee = new EmployeeEntry(name, Surname);
		ee.setDegree(degree);
		ee.setPosition(position);
		ee.setCyvilContract(staffResource);
		ee.setJobTime(jobTime);
		return employees.add(ee);
	}
	
	public EmployeeEntry getEmployee(String name, String Surname)
	throws IllegalArgumentException {
		if (null == name || null == Surname) throw new IllegalArgumentException("Brak imienia i nazwiska");
		for (EmployeeEntry ee : employees) {
			if (ee.getName().equals(name) && ee.getSurname().equals(Surname))
				return ee;
		}
		return null;
	}

	public boolean addSpecialization(String name)
	throws IllegalArgumentException {
		if (null == name) return false;
		for (SpecializationEntry se : specializations) {
			if (se.getName().equals(name))
				throw new IllegalArgumentException("Specjalizacja " + name + " już istnieje");
		}
		return specializations.add(new SpecializationEntry(name));
	}
	
	public SpecializationEntry getSpecialization(String name)
	throws IllegalArgumentException {
		if (null == name) return null;
		for (SpecializationEntry se : specializations) {
			if (se.getName().equals(name))
				return se;
		}
		throw new IllegalArgumentException("Specjalizacja " + name + " nie istnieje");
	}
		
	public long numberOfSpecializations() {
		return specializations.size();
	}
	
	@Override
	public String toString() {
		return super.getName();
	}

	public int getEmployeesNumber() {
		return employees.size();
	}

	public List<EmployeeEntry> getEmployees() {
		return employees;
	}

	public List<SpecializationEntry> getSpecializations() {
		return specializations;
	}

	public boolean removeEmployee(EmployeeEntry ee) {
		return employees.remove(ee);
	}

	public boolean checkIfSpecExists(String text) {
		for (SpecializationEntry se : specializations) {
			if (se.getName().equals(text)) return true;
		}
		return false;
	}
	
	public static boolean checkIfSpecUsed(List <EmployeeEntry> lst, SpecializationEntry spec) {
		for (EmployeeEntry ee : lst) {
			for (SubjectEntry se : ee.getSubjects()) {
				if (se.getSpec().getName().equals(spec.getName())) return true;
			}
		}
		return false;
	}

	public boolean removeSpec(SpecializationEntry se) {
		return specializations.remove(se);
	}
}