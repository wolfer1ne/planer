package data.primitives;

public abstract class DataEntry {
	protected String value;
	protected DataEntryType type;
	protected long idGlobal;
	
	public DataEntry(String val, DataEntryType t) {
		value = val;
		type = t;
	}
	
	public String getName() { return value; }
	public void   setName(String v) { value = v; }

	public DataEntryType getType() {
		return type;
	}
}
