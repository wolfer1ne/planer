package data.primitives;

public enum SemesterType {
	WINTER,
	SUMMER;
	
	@Override
	public String toString() {
		switch (this) {
			case WINTER: return "zimowy";
			case SUMMER: return "letni";
			default: throw new IllegalArgumentException();
		}
	}
}