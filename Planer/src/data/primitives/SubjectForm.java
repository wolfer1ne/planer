package data.primitives;

public enum SubjectForm {
	// wziete z karty okresowej osiągniac studenta
	A,L,LE,P,PD,PR,PZ,S,SK,T,W,WA,ZP,C;
	
	@Override
	public String toString() {
		switch (this) {
			case A:  return "ćwiczenia audytatoryjne";
			case L:  return "ćwiczenia labolatoryjne";
			case LE: return "lektorat";
			case P:  return "ćwiczenia praktyczne";
			case PD: return "praca dyplomowa";
			case PR: return "ćwiczenia projektowe";
			case PZ: return "praktyki zawodowe";
			case S:  return "seminarium";
			case SK: return "samokształcenie";
			case T:  return "ćwiczenia terenowe";
			case W:  return "wykład";
			case WA: return "warsztaty";
			case ZP: return "zajęcia praktyczne";
			case C:  return "ćwiczenia";
			default: throw new IllegalArgumentException();
		}
	}
}
