package data.primitives;

public enum StudyYear {
	I, II, III, IV;

	@Override
	public String toString() {
		switch (this) {
			case I:   return "I";
			case II:  return "II";
			case III: return "III";
			case IV:  return "IV";
			default: throw new IllegalArgumentException();
		}
	}
}
