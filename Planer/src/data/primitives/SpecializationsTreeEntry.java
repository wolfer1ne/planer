package data.primitives;

public class SpecializationsTreeEntry extends DataEntry {
	public SpecializationsTreeEntry() {
		super("SPECJALIZACJE", DataEntryType.SPECIALIZATIONS);
	}
	
	public String toString() { return super.getName(); }
}