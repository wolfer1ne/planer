package data.primitives;

public enum EmployeeActivityType {
	NONDIDACTIC,
	ORGANIZATIONAL,
	MISC;
	
	@Override
	public String toString() {
		switch (this) {
			case NONDIDACTIC:    return "pozadydakcyjne";
			case ORGANIZATIONAL: return "organizacyjne";
			case MISC:           return "inne";
			default: throw new IllegalArgumentException();
		}
	}
}
