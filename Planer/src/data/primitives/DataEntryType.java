package data.primitives;

public enum DataEntryType {
	YEAR,
	YEAR_INFO,
	INSTITUTE,
	DEPARTMENT,
	SUBJECT,
	SPECIALIZATION,
	SPECIALIZATIONS, // Tree
	EMPLOYEE,
	EMPLOYEES, // Tree
	ACTIVITY;
}