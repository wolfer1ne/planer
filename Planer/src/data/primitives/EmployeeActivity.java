package data.primitives;

public class EmployeeActivity extends DataEntry {
	protected EmployeeActivityType eat;
	
	public EmployeeActivity(String v, EmployeeActivityType t) {
		super(v, DataEntryType.ACTIVITY);
		eat = t;
	}
	
	public EmployeeActivityType getActivityType() {
		return eat;
	}
}
