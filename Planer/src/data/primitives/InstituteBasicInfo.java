package data.primitives;

public class InstituteBasicInfo {
	short ammountOfStudents; // Liczba studentów
	byte groupsAuditory; // Audytoryjne
	byte groupsLabolatory; // Labolatoryjne
	byte groupsForeignLanguages; // Jezykow obcych
	byte groupsPE; // Physical education
	
	public short getAmmountOfStudents() {
		return ammountOfStudents;
	}
	public byte getGroupsAuditory() {
		return groupsAuditory;
	}
	public byte getGroupsLabolatory() {
		return groupsLabolatory;
	}
	public byte getGroupsForeignLanguages() {
		return groupsForeignLanguages;
	}
	public byte getGroupsPE() {
		return groupsPE;
	}
	public void setAmmountOfStudents(short ammountOfStudents) {
		this.ammountOfStudents = ammountOfStudents;
	}
	public void setGroupsAuditory(byte groupsAuditory) {
		this.groupsAuditory = groupsAuditory;
	}
	public void setGroupsLabolatory(byte groupsLabolatory) {
		this.groupsLabolatory = groupsLabolatory;
	}
	public void setGroupsForeignLanguages(byte groupsForeignLanguages) {
		this.groupsForeignLanguages = groupsForeignLanguages;
	}
	public void setGroupsPE(byte groupsPE) {
		this.groupsPE = groupsPE;
	}
}