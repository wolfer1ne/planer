package data.primitives;

public enum SemesterNumber {
	I, II, III, IV,	V, VI, VII;
	
	@Override
	public String toString() {
		switch (this) {
				case I:   return "1";
				case II:  return "2";
				case III: return "3";
				case IV:  return "4";
				case V:   return "5";
				case VI:  return "6";
				case VII: return "7";
				default: throw new IllegalArgumentException();
		}
	}
}
