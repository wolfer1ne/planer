package data.primitives;

public enum SubjectType {
	FULLTIME,
	PARTTIME;

	@Override
	public String toString() {
		switch (this) {
			case FULLTIME: return "stacjonarne";
			case PARTTIME: return "niestacjonarne";
			default: throw new IllegalArgumentException();
		}
	}
}
