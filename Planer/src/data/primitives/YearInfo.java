package data.primitives;

import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;

public class YearInfo extends DataEntry {
	// https://docs.oracle.com/javase/6/docs/api/java/util/EnumMap.html#EnumMap(java.lang.Class)
	Map<SubjectType, SemesterBasicInfo> basicInfo;
	
	public YearInfo() {
		super(YearInfo.class.getName(), DataEntryType.YEAR_INFO);
		basicInfo = Collections.synchronizedMap(new EnumMap<SubjectType, SemesterBasicInfo>(SubjectType.class));
		initBasicInfo();
	}
	
	private final void initBasicInfo() {
		for (SubjectType st : SubjectType.values()) {
			basicInfo.put(st, new SemesterBasicInfo());
		}
	}
	
	public Map<SubjectType, SemesterBasicInfo> getBasicInfo() {
		return basicInfo;
	}

	public SemesterBasicInfo getYearBasicInfo(SubjectType st) {
		return basicInfo.get(st);
	}		
}