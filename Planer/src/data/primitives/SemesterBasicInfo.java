package data.primitives;

public class SemesterBasicInfo {
	long ammountOfStudents; // Liczba studentów
	long groupsAuditory; // Audytoryjne
	long groupsLabolatory; // Labolatoryjne
	long groupsForeignLanguages; // Jezykow obcych
	long groupsPE; // Physical education
	long groupsSeminary; // Seminaryjne
	
	public long getAmmountOfStudents() {
		return ammountOfStudents;
	}
	public long getGroupsAuditory() {
		return groupsAuditory;
	}
	public long getGroupsLabolatory() {
		return groupsLabolatory;
	}
	public long getGroupsForeignLanguages() {
		return groupsForeignLanguages;
	}
	public long getGroupsPE() {
		return groupsPE;
	}
	public void setAmmountOfStudents(long ammountOfStudents) {
		this.ammountOfStudents = ammountOfStudents;
	}
	public void setGroupsAuditory(long groupsAuditory) {
		this.groupsAuditory = groupsAuditory;
	}
	public void setGroupsLabolatory(long groupsLabolatory) {
		this.groupsLabolatory = groupsLabolatory;
	}
	public void setGroupsForeignLanguages(long groupsForeignLanguages) {
		this.groupsForeignLanguages = groupsForeignLanguages;
	}
	public void setGroupsPE(long groupsPE) {
		this.groupsPE = groupsPE;
	}
	public long getGroupsSeminary() {
		return groupsSeminary;
	}
	public void setGroupsSeminary(long groupsSeminary) {
		this.groupsSeminary = groupsSeminary;
	}
}