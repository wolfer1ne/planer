package data;

import java.util.ArrayList;
import java.util.List;

import data.primitives.DataEntry;
import data.primitives.DataEntryType;

public class InstituteEntry extends DataEntry {
	List<DepartmentEntry> departments = new ArrayList<DepartmentEntry>();
	
	public InstituteEntry(String val) {
		super(val, DataEntryType.INSTITUTE);
	}
	
	public boolean addDepartment(String name)
	throws IllegalArgumentException {
		if (null == name) return false;
		for (DepartmentEntry ie : departments) {
			if (ie.getName().equals(name)) 
				throw new IllegalArgumentException("Kierunek " + name + " już istnieje");
		}
		return departments.add(new DepartmentEntry(name));
	}
	
	public DepartmentEntry getDepartment(String name) 
	throws IllegalArgumentException {
		if (null == name) return null;
		for (DepartmentEntry de : departments) {
			if (de.getName().equals(name))
				return de;
		}
		throw new IllegalArgumentException("Kierunek " + name + " nie istnieje");
	}
	
	public long numberOfDepartments() {
		return departments.size();
	}
	
	@Override
	public String toString() {
		return "Instytut " + super.getName();
	}

	public List<DepartmentEntry> getDepartments() {
		return departments;
	}

	public boolean removeDepartment(DepartmentEntry de) {
		return departments.remove(de);
	}

	public void setName(String text) {
		this.value = text;
	}

	public boolean checkIfExists(String text) {
		for (DepartmentEntry de : departments) {
			if (de.getName().equals(text)) return true;
		}
		return false;
	}
}