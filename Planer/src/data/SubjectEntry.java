package data;

import data.primitives.DataEntry;
import data.primitives.DataEntryType;
import data.primitives.SemesterNumber;
import data.primitives.SemesterType;
import data.primitives.StudyYear;
import data.primitives.SubjectForm;
import data.primitives.SubjectType;

public class SubjectEntry extends DataEntry {
	SubjectType    subType;
	SubjectForm    subForm;
	SemesterType   semType;
	StudyYear      studyYear;
	SemesterNumber semNum;
	
	long numOfStudents;
	long numOfGroups;
	long hoursWeekly;
	long hoursSemester;
	
	SpecializationEntry spec = null;
	
	public SubjectEntry(String val) {
		super(val, DataEntryType.SUBJECT);
	}

	public SubjectType getSubType() {
		return subType;
	}

	public SubjectForm getSubForm() {
		return subForm;
	}

	public SemesterType getSemType() {
		return semType;
	}

	public StudyYear getStudyYear() {
		return studyYear;
	}

	public SemesterNumber getSemNum() {
		return semNum;
	}

	public long getNumOfStudents() {
		return numOfStudents;
	}

	public long getNumOfGroups() {
		return numOfGroups;
	}

	public void setNumOfGroups(long numOfGroups) {
		this.numOfGroups = numOfGroups;
	}

	public long getHoursWeekly() {
		return hoursWeekly;
	}

	public long getHoursSemester() {
		return hoursSemester;
	}

	public SpecializationEntry getSpec() {
		return spec;
	}

	public void setSubType(SubjectType subType) {
		this.subType = subType;
	}

	public void setSubForm(SubjectForm subForm) {
		this.subForm = subForm;
	}

	public void setSemType(SemesterType semType) {
		this.semType = semType;
	}

	public void setStudyYear(StudyYear studyYear) {
		this.studyYear = studyYear;
	}

	public void setSemNum(SemesterNumber semNum) {
		this.semNum = semNum;
	}

	public void setNumOfStudents(long numOfStudentsOrGroups) {
		this.numOfStudents = numOfStudentsOrGroups;
	}

	public void setHoursWeekly(long hoursWeekly) {
		this.hoursWeekly = hoursWeekly;
	}

	public void setHoursSemester(long hoursSemester) {
		this.hoursSemester = hoursSemester;
	}

	public void setSpec(SpecializationEntry spec) {
		this.spec = spec;
	}
	
	public String toString() {
		return super.getName();
	}
}