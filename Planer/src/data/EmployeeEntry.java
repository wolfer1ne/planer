package data;

import java.util.ArrayList;
import java.util.List;

import data.primitives.DataEntry;
import data.primitives.DataEntryType;
import data.primitives.EmployeeActivity;

public class EmployeeEntry extends DataEntry {
	String name;
	String surname;
	String degree;
	String position;
	long JobTime;
	
	long hoursFullTimeStudies;
	long hoursPartTimeStudies;
	
	DepartmentEntry department;
	
	boolean cyvilContract;
	
	List<EmployeeActivity> nondidactical  = null;
	List<EmployeeActivity> organizational = null;
	List<EmployeeActivity> miscActivities = null;
	List<SubjectEntry> subjects = null;
	
	public EmployeeEntry(String name, String surname) {
		super(null, DataEntryType.EMPLOYEE);
		this.name = name;
		this.surname = surname;
		
		nondidactical  = new ArrayList<EmployeeActivity>();
		organizational = new ArrayList<EmployeeActivity>();
		miscActivities = new ArrayList<EmployeeActivity>();
		
		subjects = new ArrayList<SubjectEntry>();
	}
	
	public boolean addNondidacticalActivity(EmployeeActivity ea) {
		return nondidactical.add(ea);
	}
	
	public List<EmployeeActivity> getNondidactical() {
		return nondidactical;
	}

	public boolean addOrganizationalActivity(EmployeeActivity ea) {
		return organizational.add(ea);
	}

	public List<EmployeeActivity> getOrganizational() {
		return organizational;
	}
	
	public boolean addMiscActivity(EmployeeActivity ea) {
		return miscActivities.add(ea);
	}

	public List<EmployeeActivity> getMiscActivities() {
		return miscActivities;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public String getDegree() {
		return degree;
	}

	public String getPosition() {
		return position;
	}

	public long getJobTime() {
		return JobTime;
	}

	public long getHoursFullTimeStudies() {
		return hoursFullTimeStudies;
	}

	public long getHoursPartTimeStudies() {
		return hoursPartTimeStudies;
	}

	public DepartmentEntry getDepartment() {
		return department;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public void setJobTime(long jobTime) {
		JobTime = jobTime;
	}

	public void setHoursFullTimeStudies(long hoursFullTimeStudies) {
		this.hoursFullTimeStudies = hoursFullTimeStudies;
	}

	public void setHoursPartTimeStudies(long hoursPartTimeStudies) {
		this.hoursPartTimeStudies = hoursPartTimeStudies;
	}

	public void setDepartment(DepartmentEntry department) {
		this.department = department;
	}

	public boolean addSubject(SubjectEntry se)
	throws IllegalArgumentException {
		if (null == se) throw new IllegalArgumentException("Nie można dodać pustego wpisu przedmiotu");
		return subjects.add(se);	
	}
	
	public SubjectEntry getSubjectEntryAt(int idx) {
		return subjects.get(idx);
	}
	
	public boolean isCyvilContract() {
		return cyvilContract;
	}

	public void setCyvilContract(boolean cyvilContract) {
		this.cyvilContract = cyvilContract;
	}

	public String toString() {
		return name + " " + surname;
	}

	public List<SubjectEntry> getSubjects() {
		return subjects;
	}
}