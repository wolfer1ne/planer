package data;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import data.primitives.DataEntry;
import data.primitives.DataEntryType;

public class YearEntry extends DataEntry {
	private static Logger logger = LogManager.getLogger();

	List<InstituteEntry> institutes = new ArrayList<InstituteEntry>();
	
	public YearEntry(String val) {
		super(val, DataEntryType.YEAR);
	}
	
	public long numberOfInstitutes() {
		return institutes.size();
	}
	
	public boolean addInstitute(String name)
			throws IllegalArgumentException {
		if (null == name) return false;
		for (InstituteEntry ie : institutes) {
			if (ie.getName().equals(name)) 
				throw new IllegalArgumentException("Instytut " + name + " już istnieje");
		}
		return institutes.add(new InstituteEntry(name));
	}
	
	public InstituteEntry getInstitute(String name)
			throws IllegalArgumentException {
		if (null == name) return null;
		for (InstituteEntry ie : institutes) {
			if (ie.getName().equals(name)) 
				return ie;
		}
		throw new IllegalArgumentException("Instytut " + name + " nie istnieje");
	}
	
	public boolean
	removeInsitute(InstituteEntry ie)
	{
		return institutes.remove(ie);
	}
	
	public String toString() {
		return super.getName();
	}

	public List<InstituteEntry> getInstitutes() {
		return institutes;
	}

	public boolean checkIfExists(String text) {
		for (InstituteEntry ie : institutes) {
			if (ie.getName().equals(text)) return true;
		}
		return false;
	}

	public boolean addInstitute(InstituteEntry human) {
		return institutes.add(human);
	}
}