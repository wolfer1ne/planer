package models;

import javax.swing.JButton;

import data.primitives.DataEntry;

public class RemoveButton extends JButton {
	DataEntry object;
	
	public
	RemoveButton(DataEntry obj)
	{
		super(misc.Utils.getDefaultRemoveIcon());
		object = obj;
	}
	
	public DataEntry
	getObject()
	{
		return object;
	}
}
