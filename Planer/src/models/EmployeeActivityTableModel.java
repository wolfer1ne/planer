package models;

import java.util.List;

import javax.swing.table.AbstractTableModel;

import data.primitives.EmployeeActivity;

public class EmployeeActivityTableModel extends AbstractTableModel {

	private String[] columnNames = {
			"NAZWA"
	};
	
	private List<EmployeeActivity> activities;
	
	public EmployeeActivityTableModel() {}
	public EmployeeActivityTableModel(List<EmployeeActivity> activities) {
		this.activities = activities;
	}
	
	public void
	setActivities(List<EmployeeActivity> activities)
	{
		this.activities = activities;
	}
	
	public EmployeeActivity getActivityEntryAt(int row) {
		return activities.get(row);
	}
	
	@Override
	public Class getColumnClass(int column)	{
	    return String.class;
	}
	
	@Override
	public boolean isCellEditable(int row, int column) {
	    return true;
	}
	
	@Override
    public String getColumnName(int column) {
        return columnNames[0];
    }
	
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return activities.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return getActivityEntryAt(rowIndex).getName();
	}
	
	@Override
	public void setValueAt(Object value, int row, int col) {
		EmployeeActivity ea = getActivityEntryAt(row);
		ea.setName((String) value);
		fireTableCellUpdated(row, col);
	}
	
	public void addRow(Object object) {
		activities.add((EmployeeActivity) object);
		fireTableDataChanged();
	}
	
	public void removeRow(int row) {
		activities.remove(row);
		fireTableRowsDeleted(row,row);
	}

}
