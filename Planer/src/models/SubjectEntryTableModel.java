package models;

import java.util.ArrayList;
import java.util.List;

import javax.swing.table.AbstractTableModel;

import data.SpecializationEntry;
import data.SubjectEntry;
import data.primitives.SemesterNumber;
import data.primitives.SemesterType;
import data.primitives.StudyYear;
import data.primitives.SubjectForm;
import data.primitives.SubjectType;

public class SubjectEntryTableModel extends AbstractTableModel {
	
	 private String[] columnNames = {
			 "NAZWA",
			 "ROK",
			 "SEMESTR",
			 "SPECJALIZACJA",
			 "TYP",
			 "RODZAJ",
			 "FORMA",
			 "GRUPY",
			 "STUDENCI",
			 "GODZ. TYGODNIOWO",
			 "GODZ. W SEMESTRZE"
	 };
	 
	private List<SubjectEntry> subjects;
	
	public SubjectEntryTableModel() {}
	public SubjectEntryTableModel(List<SubjectEntry> s) {
		subjects = s;
	}
	
	public void
	setSubjects(List<SubjectEntry> s) {
		subjects = s;
	}
	
	public SubjectEntry getSubjectEntryAt(int row) {
	    return subjects.get(row);
	}
	
	@Override
	public Class getColumnClass(int column)	{
	    switch (column) {
	    	case 1: return StudyYear.class;
	        case 2: return SemesterNumber.class;
	        case 3: return SpecializationEntry.class;
	        case 4: return SemesterType.class;
	        case 5: return SubjectType.class;
	        case 6: return SubjectForm.class;
	        case 7: return long.class;
	        case 8: return long.class;
	        case 9: return long.class;
	        case 10: return long.class;
	        default: return String.class;
	    }
	}

	@Override
	public boolean isCellEditable(int row, int column) {
	    return false;
	}
	 
	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
    public String getColumnName(int column) {
        return columnNames[column];
    }
	
	@Override
    public int getRowCount() {
        return subjects.size();
    }

	@Override
	public Object getValueAt(int row, int column) {
		SubjectEntry subject = getSubjectEntryAt(row);
		 
	    switch (column) {
	        case 0: return subject.getName();
	        case 1: return subject.getStudyYear();
	        case 2: return subject.getSemNum();
	        case 3: return subject.getSpec();
	        case 4: return subject.getSemType();
	        case 5: return subject.getSubType();
	        case 6: return subject.getSubForm();
	        case 7: return subject.getNumOfGroups();
	        case 8: return subject.getNumOfStudents();
	        case 9: return subject.getHoursWeekly();
	        case 10: return subject.getHoursSemester();
	        default: return null;
	    }
	}

	public void addRow(Object object) {
		subjects.add((SubjectEntry) object);
		fireTableDataChanged();
	}
	
	public void removeRow(int row) {
		subjects.remove(row);
		fireTableRowsDeleted(row,row);
	}
}