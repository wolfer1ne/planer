package models;

import java.awt.Font;
import java.util.Arrays;
import java.util.Enumeration;

import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.SwingUtilities;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.DataContainer;
import data.DepartmentEntry;
import data.EmployeeEntry;
import data.InstituteEntry;
import data.SpecializationEntry;
import data.YearEntry;
import data.primitives.SpecializationsTreeEntry;
import misc.Utils;
import views.pnlDepartmentOverview;
import views.pnlEmployeeOverview;
import views.pnlInstituteOverview;
import views.pnlYearOverview;

public class planerTree implements TreeSelectionListener {
	private Logger logger = LogManager.getLogger(planerTree.class.getName());
	JTree tree;
	JSplitPane jsp;
	DefaultMutableTreeNode root;
	DataContainer dc;
	TreePath p;
	DefaultMutableTreeNode elm;
	
	public planerTree(DataContainer dc, JSplitPane jsp) {
		root = new DefaultMutableTreeNode("ROOT");
		this.jsp = jsp;
		this.dc = dc;
		
		tree = new JTree(root);
		generateTree();
		
		tree.addTreeSelectionListener(this);
		tree.setShowsRootHandles(true);
		tree.setRootVisible(false);
		tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		
		final Font currentFont = tree.getFont();
		final Font bigFont = new Font(currentFont.getName(), currentFont.getStyle(), currentFont.getSize() + 2);
		tree.setFont(bigFont);		
	}
	
	public void generateTree() {
		if (null == this.dc) return;
		//if (dc.getYearsAmmount() == 0) return;
		generateData(this.root, this.dc);
		this.refresh();
	}
	
	private void clearTree() {
		logger.debug("Kasacja drzewa");
		root.removeAllChildren();
		refresh();
		//tree = new JTree(root);
	}
	
	private void generateData(DefaultMutableTreeNode top, DataContainer dc) {
		clearTree();
		for (YearEntry ye : dc.getYears()) {
			DefaultMutableTreeNode year = new DefaultMutableTreeNode(ye);
			top.add(year);
			for (InstituteEntry ie : ye.getInstitutes()) {
				DefaultMutableTreeNode institute = new DefaultMutableTreeNode(ie);
				year.add(institute);
				for (DepartmentEntry de : ((InstituteEntry) ie).getDepartments()) {
					DefaultMutableTreeNode department = new DefaultMutableTreeNode(de);
					institute.add(department);
					for (EmployeeEntry ee : de.getEmployees()) {
						DefaultMutableTreeNode employee = new DefaultMutableTreeNode(ee);
						department.add(employee);
					}
					/*DefaultMutableTreeNode specializations = new DefaultMutableTreeNode(new SpecializationsTreeEntry());
					department.add(specializations);
					for (SpecializationEntry se : de.getSpecializations()) {
						DefaultMutableTreeNode specialization = new DefaultMutableTreeNode(se);
						specializations.add(specialization);
					}*/
				}				
			}
		}
		logger.debug("Ilosc w dc {}; ilosc w drzewie {}", dc.getYearsAmmount(), top.getChildCount());
		refresh();
		expandCurrentPath(p);
	}

	public void refresh() {
		DefaultTreeModel model = (DefaultTreeModel)tree.getModel();
		DefaultMutableTreeNode root = (DefaultMutableTreeNode)model.getRoot();
		model.reload(root);
		//root.fireTreeStructureChanged();
	}
	
	public JTree getTree() {
		return tree;
	}
	
	void
	expandCurrentPath(TreePath p)
	{
		if (null == p) return;
		logger.debug("EXPAND {}; ROWS {}", String.valueOf(p), tree.getRowCount());
	    tree.scrollPathToVisible(p.getParentPath());
	    tree.expandPath(p);
	    tree.setSelectionPath(p);
	}
	
	public DepartmentEntry getSelectedDepartment() {
		TreePath p = tree.getSelectionPath();
		Object o = p.getPathComponent(3);
		DefaultMutableTreeNode d = (DefaultMutableTreeNode) p.getPathComponent(3);
		return (DepartmentEntry) d.getUserObject();
	}
	
	public InstituteEntry getSelectedInstitute() {
		TreePath p = tree.getSelectionPath();
		Object o = p.getPathComponent(2);
		DefaultMutableTreeNode d = (DefaultMutableTreeNode) p.getPathComponent(2);
		return (InstituteEntry) d.getUserObject();
	}
	
	public YearEntry getSelectedYear() {
		TreePath p = tree.getSelectionPath();
		Object o = p.getPathComponent(1);
		DefaultMutableTreeNode d = (DefaultMutableTreeNode) p.getPathComponent(1);
		return (YearEntry) d.getUserObject();
	}

	@Override
	public void valueChanged(TreeSelectionEvent e) {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                tree.getLastSelectedPathComponent();

		if (node == null) return;

		elm = node;
		/* retrieve the node that was selected */ 
		Object nodeInfo = node.getUserObject();
		
		if (nodeInfo instanceof YearEntry) {
			jsp.setRightComponent(new pnlYearOverview((YearEntry)nodeInfo));
		} else if (nodeInfo instanceof InstituteEntry) {
			jsp.setRightComponent(new pnlInstituteOverview((InstituteEntry) nodeInfo));
		} else if (nodeInfo instanceof DepartmentEntry) {
			jsp.setRightComponent(new pnlDepartmentOverview((DepartmentEntry) nodeInfo));
		} else if (nodeInfo instanceof EmployeeEntry) {
			jsp.setRightComponent(new pnlEmployeeOverview(getSelectedDepartment(),
					(EmployeeEntry) nodeInfo));
		}
		
		p = tree.getSelectionPath();
		
		expandCurrentPath(p);
		
		logger.debug("{}; {}", nodeInfo.getClass().getName(),String.valueOf(tree.getSelectionPath()));
	}

	public void generateTree(DataContainer dc2) {
		this.dc = dc2;
		this.generateTree();
	}
}