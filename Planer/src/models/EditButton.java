package models;

import javax.swing.JButton;
import javax.swing.SwingConstants;

import data.primitives.DataEntry;

public class EditButton extends JButton {
	DataEntry object;

	public EditButton(String txt, DataEntry object) {
		super(txt, misc.Utils.getDefaultEditIcon());
		this.object = object;
		setStyle();
	}
	
	private void
	setStyle()
	{
		setVerticalTextPosition(SwingConstants.CENTER);
	    setHorizontalTextPosition(SwingConstants.RIGHT);
	}
	
	public DataEntry
	getObject()
	{
		return object;
	}
}
