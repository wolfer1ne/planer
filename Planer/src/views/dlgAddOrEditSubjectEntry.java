package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import data.DepartmentEntry;
import data.EmployeeEntry;
import data.SpecializationEntry;
import data.SubjectEntry;
import data.primitives.SemesterNumber;
import data.primitives.SubjectForm;
import data.primitives.SubjectType;
import misc.Utils;
import models.SubjectEntryTableModel;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;

import java.awt.GridLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class dlgAddOrEditSubjectEntry extends JDialog {
	private static final Logger logger = LogManager.getLogger(dlgAddOrEditSubjectEntry.class.getName());
	private final JPanel contentPanel = new JPanel();
	private JComboBox<SpecializationEntry> cmBoxSpecialization;
	private JLabel lblTitleInput;
	private JTextField txtSubjectName;
	private JComboBox<SubjectForm> cmBoxLectureType;
	private JFormattedTextField fmtTxtGroupsAmmount;
	private JFormattedTextField fmtTxtStudentsNumber;
	private JFormattedTextField fmtTxtWeeklyHours;
	private JComboBox<SemesterNumber> cmBoxSemester;
	private JComboBox<SubjectType> cmBoxSubjectType;
	private JFormattedTextField fmtTxtSemesterHours;
	
	private EmployeeEntry ee;
	private DepartmentEntry de;
	SubjectEntry se;
	SubjectEntryTableModel setm;
	private JButton btnOk;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		try {
			dlgAddSubjectEntry dialog = new dlgAddSubjectEntry();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * Create the dialog.
	 */
	public dlgAddOrEditSubjectEntry() {
		setTitle("NOWY WPIS PRZEDMIOTU");
		setBounds(100, 100, 600, 400);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{10, 0, 0, 0, 0, 50, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 23, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		getContentPane().setLayout(gridBagLayout);
		{
			JLabel lblYearInput = new JLabel("");
			lblYearInput.setFont(new Font("Tahoma", Font.BOLD, 15));
			GridBagConstraints gbc_lblYearInput = new GridBagConstraints();
			gbc_lblYearInput.gridwidth = 2;
			gbc_lblYearInput.insets = new Insets(0, 0, 5, 5);
			gbc_lblYearInput.gridx = 1;
			gbc_lblYearInput.gridy = 1;
			getContentPane().add(lblYearInput, gbc_lblYearInput);
		}
		{
			lblTitleInput = new JLabel("[NAME SURNAME]");
			lblTitleInput.setFont(new Font("Tahoma", Font.BOLD, 20));
			GridBagConstraints gbc_lblTitleInput = new GridBagConstraints();
			gbc_lblTitleInput.gridwidth = 2;
			gbc_lblTitleInput.insets = new Insets(0, 0, 5, 5);
			gbc_lblTitleInput.gridx = 5;
			gbc_lblTitleInput.gridy = 1;
			getContentPane().add(lblTitleInput, gbc_lblTitleInput);
		}
		{
			JLabel lblInstituteInput = new JLabel("");
			lblInstituteInput.setFont(new Font("Tahoma", Font.BOLD, 15));
			GridBagConstraints gbc_lblInstituteInput = new GridBagConstraints();
			gbc_lblInstituteInput.gridwidth = 5;
			gbc_lblInstituteInput.insets = new Insets(0, 0, 5, 5);
			gbc_lblInstituteInput.gridx = 1;
			gbc_lblInstituteInput.gridy = 2;
			getContentPane().add(lblInstituteInput, gbc_lblInstituteInput);
		}
		{
			JLabel lblDepartmentInput = new JLabel("");
			lblDepartmentInput.setFont(new Font("Tahoma", Font.BOLD, 15));
			GridBagConstraints gbc_lblDepartmentInput = new GridBagConstraints();
			gbc_lblDepartmentInput.insets = new Insets(0, 0, 5, 5);
			gbc_lblDepartmentInput.gridx = 6;
			gbc_lblDepartmentInput.gridy = 2;
			getContentPane().add(lblDepartmentInput, gbc_lblDepartmentInput);
		}
		{
			JSeparator separator = new JSeparator();
			separator.setForeground(Color.BLACK);
			separator.setBackground(Color.BLACK);
			GridBagConstraints gbc_separator = new GridBagConstraints();
			gbc_separator.fill = GridBagConstraints.BOTH;
			gbc_separator.gridwidth = 6;
			gbc_separator.insets = new Insets(0, 0, 5, 5);
			gbc_separator.gridx = 1;
			gbc_separator.gridy = 3;
			getContentPane().add(separator, gbc_separator);
		}
		{
			JLabel lblPrzedmiot = new JLabel("PRZEDMIOT");
			lblPrzedmiot.setFont(new Font("Tahoma", Font.BOLD, 15));
			GridBagConstraints gbc_lblPrzedmiot = new GridBagConstraints();
			gbc_lblPrzedmiot.anchor = GridBagConstraints.EAST;
			gbc_lblPrzedmiot.gridwidth = 2;
			gbc_lblPrzedmiot.insets = new Insets(0, 0, 5, 5);
			gbc_lblPrzedmiot.gridx = 4;
			gbc_lblPrzedmiot.gridy = 4;
			getContentPane().add(lblPrzedmiot, gbc_lblPrzedmiot);
		}
		{
			txtSubjectName = new JTextField();
			GridBagConstraints gbc_txtSubjectName = new GridBagConstraints();
			gbc_txtSubjectName.insets = new Insets(0, 0, 5, 5);
			gbc_txtSubjectName.fill = GridBagConstraints.HORIZONTAL;
			gbc_txtSubjectName.gridx = 6;
			gbc_txtSubjectName.gridy = 4;
			getContentPane().add(txtSubjectName, gbc_txtSubjectName);
		}
		{
			JLabel lblFullTimeStudies = new JLabel("TYP");
			lblFullTimeStudies.setFont(new Font("Tahoma", Font.BOLD, 15));
			GridBagConstraints gbc_lblFullTimeStudies = new GridBagConstraints();
			gbc_lblFullTimeStudies.anchor = GridBagConstraints.EAST;
			gbc_lblFullTimeStudies.gridwidth = 2;
			gbc_lblFullTimeStudies.insets = new Insets(0, 0, 5, 5);
			gbc_lblFullTimeStudies.gridx = 4;
			gbc_lblFullTimeStudies.gridy = 5;
			getContentPane().add(lblFullTimeStudies, gbc_lblFullTimeStudies);
		}
		{
			cmBoxSubjectType = new JComboBox<>();
			cmBoxSubjectType.setModel(new DefaultComboBoxModel<>(SubjectType.values()));
			GridBagConstraints gbc_cmBoxSubjectType = new GridBagConstraints();
			gbc_cmBoxSubjectType.insets = new Insets(0, 0, 5, 5);
			gbc_cmBoxSubjectType.fill = GridBagConstraints.HORIZONTAL;
			gbc_cmBoxSubjectType.gridx = 6;
			gbc_cmBoxSubjectType.gridy = 5;
			getContentPane().add(cmBoxSubjectType, gbc_cmBoxSubjectType);
		}
		{
			JLabel lblSemester = new JLabel("SEMESTR");
			lblSemester.setFont(new Font("Tahoma", Font.BOLD, 15));
			GridBagConstraints gbc_lblSemester = new GridBagConstraints();
			gbc_lblSemester.anchor = GridBagConstraints.EAST;
			gbc_lblSemester.gridwidth = 2;
			gbc_lblSemester.insets = new Insets(0, 0, 5, 5);
			gbc_lblSemester.gridx = 4;
			gbc_lblSemester.gridy = 6;
			getContentPane().add(lblSemester, gbc_lblSemester);
		}
		{
			cmBoxSemester = new JComboBox<>();
			cmBoxSemester.setModel(new DefaultComboBoxModel<>(SemesterNumber.values()));
			GridBagConstraints gbc_cmBoxSemester = new GridBagConstraints();
			gbc_cmBoxSemester.fill = GridBagConstraints.HORIZONTAL;
			gbc_cmBoxSemester.insets = new Insets(0, 0, 5, 5);
			gbc_cmBoxSemester.gridx = 6;
			gbc_cmBoxSemester.gridy = 6;
			getContentPane().add(cmBoxSemester, gbc_cmBoxSemester);
		}
		{
			JLabel lblRodzajZaj = new JLabel("RODZAJ ZAJĘĆ");
			lblRodzajZaj.setFont(new Font("Tahoma", Font.BOLD, 15));
			GridBagConstraints gbc_lblRodzajZaj = new GridBagConstraints();
			gbc_lblRodzajZaj.anchor = GridBagConstraints.EAST;
			gbc_lblRodzajZaj.gridwidth = 2;
			gbc_lblRodzajZaj.insets = new Insets(0, 0, 5, 5);
			gbc_lblRodzajZaj.gridx = 4;
			gbc_lblRodzajZaj.gridy = 7;
			getContentPane().add(lblRodzajZaj, gbc_lblRodzajZaj);
		}
		{
			cmBoxLectureType = new JComboBox<>();
			cmBoxLectureType.setModel(new DefaultComboBoxModel<>(SubjectForm.values()));
			GridBagConstraints gbc_cmBoxLectureType = new GridBagConstraints();
			gbc_cmBoxLectureType.insets = new Insets(0, 0, 5, 5);
			gbc_cmBoxLectureType.fill = GridBagConstraints.HORIZONTAL;
			gbc_cmBoxLectureType.gridx = 6;
			gbc_cmBoxLectureType.gridy = 7;
			getContentPane().add(cmBoxLectureType, gbc_cmBoxLectureType);
		}
		{
			JLabel lblGroups = new JLabel("ILOŚĆ GRUP");
			lblGroups.setFont(new Font("Tahoma", Font.BOLD, 15));
			GridBagConstraints gbc_lblGroups = new GridBagConstraints();
			gbc_lblGroups.anchor = GridBagConstraints.EAST;
			gbc_lblGroups.gridwidth = 2;
			gbc_lblGroups.insets = new Insets(0, 0, 5, 5);
			gbc_lblGroups.gridx = 4;
			gbc_lblGroups.gridy = 8;
			getContentPane().add(lblGroups, gbc_lblGroups);
		}
		{
			fmtTxtGroupsAmmount = new JFormattedTextField();
			GridBagConstraints gbc_fmtTxtGroupsAmmount = new GridBagConstraints();
			gbc_fmtTxtGroupsAmmount.insets = new Insets(0, 0, 5, 5);
			gbc_fmtTxtGroupsAmmount.fill = GridBagConstraints.HORIZONTAL;
			gbc_fmtTxtGroupsAmmount.gridx = 6;
			gbc_fmtTxtGroupsAmmount.gridy = 8;
			getContentPane().add(fmtTxtGroupsAmmount, gbc_fmtTxtGroupsAmmount);
		}
		{
			JLabel lblStudentsNumber = new JLabel("LICZBA STUDENTÓW");
			lblStudentsNumber.setFont(new Font("Tahoma", Font.BOLD, 15));
			GridBagConstraints gbc_lblStudentsNumber = new GridBagConstraints();
			gbc_lblStudentsNumber.gridwidth = 2;
			gbc_lblStudentsNumber.anchor = GridBagConstraints.EAST;
			gbc_lblStudentsNumber.insets = new Insets(0, 0, 5, 5);
			gbc_lblStudentsNumber.gridx = 4;
			gbc_lblStudentsNumber.gridy = 9;
			getContentPane().add(lblStudentsNumber, gbc_lblStudentsNumber);
		}
		{
			fmtTxtStudentsNumber = new JFormattedTextField();
			GridBagConstraints gbc_fmtTxtStudentsNumber = new GridBagConstraints();
			gbc_fmtTxtStudentsNumber.insets = new Insets(0, 0, 5, 5);
			gbc_fmtTxtStudentsNumber.fill = GridBagConstraints.HORIZONTAL;
			gbc_fmtTxtStudentsNumber.gridx = 6;
			gbc_fmtTxtStudentsNumber.gridy = 9;
			getContentPane().add(fmtTxtStudentsNumber, gbc_fmtTxtStudentsNumber);
		}
		{
			JLabel lblWeeklyHours = new JLabel("GODZINY TYGODNIOWO");
			lblWeeklyHours.setFont(new Font("Tahoma", Font.BOLD, 15));
			GridBagConstraints gbc_lblWeeklyHours = new GridBagConstraints();
			gbc_lblWeeklyHours.gridwidth = 2;
			gbc_lblWeeklyHours.insets = new Insets(0, 0, 5, 5);
			gbc_lblWeeklyHours.gridx = 4;
			gbc_lblWeeklyHours.gridy = 10;
			getContentPane().add(lblWeeklyHours, gbc_lblWeeklyHours);
		}
		{
			fmtTxtWeeklyHours = new JFormattedTextField();
			GridBagConstraints gbc_fmtTxtWeeklyHours = new GridBagConstraints();
			gbc_fmtTxtWeeklyHours.insets = new Insets(0, 0, 5, 5);
			gbc_fmtTxtWeeklyHours.fill = GridBagConstraints.HORIZONTAL;
			gbc_fmtTxtWeeklyHours.gridx = 6;
			gbc_fmtTxtWeeklyHours.gridy = 10;
			getContentPane().add(fmtTxtWeeklyHours, gbc_fmtTxtWeeklyHours);
		}
		{
			JLabel lblSemesterHours = new JLabel("GODZIN W SEMESTRZE");
			lblSemesterHours.setFont(new Font("Tahoma", Font.BOLD, 15));
			GridBagConstraints gbc_lblSemesterHours = new GridBagConstraints();
			gbc_lblSemesterHours.anchor = GridBagConstraints.EAST;
			gbc_lblSemesterHours.gridwidth = 2;
			gbc_lblSemesterHours.insets = new Insets(0, 0, 5, 5);
			gbc_lblSemesterHours.gridx = 4;
			gbc_lblSemesterHours.gridy = 11;
			getContentPane().add(lblSemesterHours, gbc_lblSemesterHours);
		}
		{
			fmtTxtSemesterHours = new JFormattedTextField();
			GridBagConstraints gbc_fmtTxtSemesterHours = new GridBagConstraints();
			gbc_fmtTxtSemesterHours.insets = new Insets(0, 0, 5, 5);
			gbc_fmtTxtSemesterHours.fill = GridBagConstraints.HORIZONTAL;
			gbc_fmtTxtSemesterHours.gridx = 6;
			gbc_fmtTxtSemesterHours.gridy = 11;
			getContentPane().add(fmtTxtSemesterHours, gbc_fmtTxtSemesterHours);
		}
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		GridBagConstraints gbc_contentPanel = new GridBagConstraints();
		gbc_contentPanel.anchor = GridBagConstraints.EAST;
		gbc_contentPanel.fill = GridBagConstraints.VERTICAL;
		gbc_contentPanel.insets = new Insets(0, 0, 5, 5);
		gbc_contentPanel.gridx = 0;
		gbc_contentPanel.gridy = 12;
		getContentPane().add(contentPanel, gbc_contentPanel);
		{
			JLabel lblSpecjalizacja = new JLabel("SPECJALIZACJA");
			lblSpecjalizacja.setFont(new Font("Tahoma", Font.BOLD, 15));
			GridBagConstraints gbc_lblSpecjalizacja = new GridBagConstraints();
			gbc_lblSpecjalizacja.anchor = GridBagConstraints.EAST;
			gbc_lblSpecjalizacja.insets = new Insets(0, 0, 5, 5);
			gbc_lblSpecjalizacja.gridx = 5;
			gbc_lblSpecjalizacja.gridy = 12;
			getContentPane().add(lblSpecjalizacja, gbc_lblSpecjalizacja);
		}
		{
			cmBoxSpecialization = new JComboBox<>();
			//cmBoxSpecialization.setModel(new DefaultComboBoxModel<>(SpecializationEntry.values()));
			GridBagConstraints gbc_cmBoxSpecialization = new GridBagConstraints();
			gbc_cmBoxSpecialization.insets = new Insets(0, 0, 5, 5);
			gbc_cmBoxSpecialization.fill = GridBagConstraints.HORIZONTAL;
			gbc_cmBoxSpecialization.gridx = 6;
			gbc_cmBoxSpecialization.gridy = 12;
			getContentPane().add(cmBoxSpecialization, gbc_cmBoxSpecialization);
		}
		{
			JPanel buttonPane = new JPanel();
			GridBagConstraints gbc_buttonPane = new GridBagConstraints();
			gbc_buttonPane.insets = new Insets(0, 0, 5, 5);
			gbc_buttonPane.gridwidth = 6;
			gbc_buttonPane.gridx = 1;
			gbc_buttonPane.gridy = 13;
			getContentPane().add(buttonPane, gbc_buttonPane);
			buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.X_AXIS));
			{
				btnOk = new JButton("DODAJ");
				btnOk.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						logger.info("Dodawanie przedmiotu");
						try {
							SubjectEntry se = new SubjectEntry(txtSubjectName.getText());
							se.setSemNum((SemesterNumber) cmBoxSemester.getSelectedItem());
							se.setStudyYear(Utils.SemToYear(se.getSemNum()));
							se.setSubType((SubjectType) cmBoxSubjectType.getSelectedItem());
							se.setSubForm((SubjectForm) cmBoxLectureType.getSelectedItem());
							se.setSpec((SpecializationEntry) cmBoxSpecialization.getSelectedItem());
							se.setSemType(misc.Utils.SemNumToSemType((SemesterNumber) cmBoxSemester.getSelectedItem()));
							
							se.setHoursWeekly(Long.valueOf(fmtTxtWeeklyHours.getText()));
							se.setNumOfStudents(Long.valueOf(fmtTxtStudentsNumber.getText()));
							se.setHoursSemester(Long.valueOf(fmtTxtSemesterHours.getText()));
							se.setHoursWeekly(Long.valueOf(fmtTxtWeeklyHours.getText()));
							se.setNumOfGroups(Long.valueOf(fmtTxtGroupsAmmount.getText()));
							
							/*ee.addSubject(se);
							pnlEmployeeOverview.setm.addRow(se);*/
							setm.addRow(se);
							//pnlEmployeeOverview.refreshTables();
							dispose();
						} catch (NumberFormatException nfe) {
							logger.info("BLAD: {}", nfe.getMessage());
							JOptionPane.showMessageDialog(null, nfe.getMessage());
							dispose();
						} catch (IllegalArgumentException iae) {
							logger.info("BLAD: {}", iae.getMessage());
							JOptionPane.showMessageDialog(null, iae.getMessage());
							dispose();
						}					
					}
				});
				btnOk.setFont(new Font("Tahoma", Font.BOLD, 11));
				btnOk.setActionCommand("OK");
				buttonPane.add(btnOk);
				getRootPane().setDefaultButton(btnOk);
			}
			{
				JButton btnCancel = new JButton("ANULUJ");
				btnCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				btnCancel.setFont(new Font("Tahoma", Font.BOLD, 11));
				btnCancel.setActionCommand("Cancel");
				buttonPane.add(btnCancel);
			}
		}
	}
	
	private void initSpecs() {
		for (SpecializationEntry se : de.getSpecializations()) {
			cmBoxSpecialization.addItem(se);
		}
	}

	public dlgAddOrEditSubjectEntry(EmployeeEntry ee, DepartmentEntry selectedDepartment, SubjectEntryTableModel setm) {
		this();
		
		this.ee = ee;
		this.de = selectedDepartment;
		this.setm = setm;
		
		lblTitleInput.setText("Przedmiot dla: " + ee.toString());
		initSpecs();
	}
	
	private void setSubject() {
		 //cmBoxSpecialization.setSelectedItem(se.getSpec());
		 
		 cmBoxLectureType.setSelectedItem(se.getSubForm()); 
		 cmBoxSemester.setSelectedItem(se.getSemNum());
		 cmBoxSubjectType.setSelectedItem(se.getSubType());
		 
		 txtSubjectName.setText(se.getName());
		 
		 fmtTxtGroupsAmmount.setText(String.valueOf(se.getNumOfGroups()));
		 fmtTxtStudentsNumber.setText(String.valueOf(se.getNumOfStudents()));
		 fmtTxtWeeklyHours.setText(String.valueOf(se.getHoursWeekly()));
		 fmtTxtSemesterHours.setText(String.valueOf(se.getHoursSemester()));
		 
		 for (SpecializationEntry s : de.getSpecializations()) {
			if (se.getSpec().getName().equals(s.getName())) {
				cmBoxSpecialization.setSelectedItem(s);
			}
		}
	}

	public dlgAddOrEditSubjectEntry(EmployeeEntry ee, DepartmentEntry de, SubjectEntryTableModel setm, SubjectEntry se) {
		this();
		
		this.ee = ee;
		this.de = de;
		this.se = se;
		this.setm = setm;
		
		setTitle("EDYCJA PRZEDMIOTU: " + se.getName());
		lblTitleInput.setText("Przedmiot dla: " + ee.toString());
		btnOk.setText("EDYTUJ");
		misc.Utils.removeListeners(btnOk);
		
		initSpecs();
		
		setSubject();
		
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					logger.info("Edycja przedmiotu ({})", se.getName());
					se.setName(txtSubjectName.getText());
					se.setSemNum((SemesterNumber) cmBoxSemester.getSelectedItem());
					se.setStudyYear(Utils.SemToYear(se.getSemNum()));
					se.setSubType((SubjectType) cmBoxSubjectType.getSelectedItem());
					se.setSubForm((SubjectForm) cmBoxLectureType.getSelectedItem());
					se.setSpec((SpecializationEntry) cmBoxSpecialization.getSelectedItem());
					se.setSemType(misc.Utils.SemNumToSemType((SemesterNumber) cmBoxSemester.getSelectedItem()));
					
					se.setHoursWeekly(Long.valueOf(fmtTxtWeeklyHours.getText()));
					se.setNumOfStudents(Long.valueOf(fmtTxtStudentsNumber.getText()));
					se.setHoursSemester(Long.valueOf(fmtTxtSemesterHours.getText()));
					se.setHoursWeekly(Long.valueOf(fmtTxtWeeklyHours.getText()));
					se.setNumOfGroups(Long.valueOf(fmtTxtGroupsAmmount.getText()));

					setm.fireTableDataChanged();
					dispose();
				} catch (IllegalArgumentException iae) {
					JOptionPane.showMessageDialog(null, "BLAD: " + iae.getMessage());
					dispose();
				}
			}
		});
	}
}