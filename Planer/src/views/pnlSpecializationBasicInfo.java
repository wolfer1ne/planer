package views;

import javax.swing.JPanel;
import javax.swing.BoxLayout;
import javax.swing.JSplitPane;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JFormattedTextField;
import java.awt.Font;
import javax.swing.SwingConstants;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import data.primitives.SemesterBasicInfo;

public class pnlSpecializationBasicInfo extends JPanel {
	private static final Logger logger = LogManager.getLogger();
	private JFormattedTextField fxtStudentAmmount;
	private JFormattedTextField fxtGroupsAuditory;
	private JFormattedTextField fxtGroupsLabolatory;
	
	SemesterBasicInfo sbi;
	
	public
	void saveData()
	{
		try {
			sbi.setAmmountOfStudents(Long.valueOf(fxtStudentAmmount.getText()));
			sbi.setGroupsAuditory(Long.valueOf(fxtGroupsAuditory.getText()));
			sbi.setGroupsLabolatory(Long.valueOf(fxtGroupsLabolatory.getText()));
		} catch (NumberFormatException nfe) {
			logger.info("BLAD: {}", nfe.getMessage());
			JOptionPane.showMessageDialog(null, nfe.getMessage());
		}
	}

	/**
	 * Create the panel.
	 */
	public pnlSpecializationBasicInfo() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.5);
		add(splitPane);
		
		JLabel lblStudentsAmmount = new JLabel("LICZBA STUDENTÓW");
		lblStudentsAmmount.setFont(new Font("Tahoma", Font.BOLD, 12));
		splitPane.setLeftComponent(lblStudentsAmmount);
		
		fxtStudentAmmount = new JFormattedTextField();
		fxtStudentAmmount.setHorizontalAlignment(SwingConstants.CENTER);
		fxtStudentAmmount.setFont(new Font("Tahoma", Font.BOLD, 12));
		splitPane.setRightComponent(fxtStudentAmmount);
		
		JSplitPane splitPane_1 = new JSplitPane();
		splitPane_1.setResizeWeight(0.5);
		add(splitPane_1);
		
		JLabel lblGroupsAuditory = new JLabel("GRUPY AUDYTORYJNE");
		lblGroupsAuditory.setFont(new Font("Tahoma", Font.BOLD, 12));
		splitPane_1.setLeftComponent(lblGroupsAuditory);
		
		fxtGroupsAuditory = new JFormattedTextField();
		fxtGroupsAuditory.setHorizontalAlignment(SwingConstants.CENTER);
		fxtGroupsAuditory.setFont(new Font("Tahoma", Font.BOLD, 12));
		splitPane_1.setRightComponent(fxtGroupsAuditory);
		
		JSplitPane splitPane_2 = new JSplitPane();
		splitPane_2.setResizeWeight(0.5);
		add(splitPane_2);
		
		JLabel lblGroupsLabolatory = new JLabel("GRUPY LABOLATORYJNE");
		lblGroupsLabolatory.setFont(new Font("Tahoma", Font.BOLD, 12));
		splitPane_2.setLeftComponent(lblGroupsLabolatory);
		
		fxtGroupsLabolatory = new JFormattedTextField();
		fxtGroupsLabolatory.setFont(new Font("Tahoma", Font.BOLD, 12));
		fxtGroupsLabolatory.setHorizontalAlignment(SwingConstants.CENTER);
		splitPane_2.setRightComponent(fxtGroupsLabolatory);

	}
	
	public pnlSpecializationBasicInfo(SemesterBasicInfo ibi) {
		this();
		
		this.sbi = ibi;
		
		fxtStudentAmmount.setValue(String.valueOf(this.sbi.getAmmountOfStudents()));
		fxtGroupsAuditory.setValue(String.valueOf(this.sbi.getGroupsAuditory()));
		fxtGroupsLabolatory.setValue(String.valueOf(this.sbi.getGroupsLabolatory()));
	}

	public void BlockAllFields() {
		fxtGroupsAuditory.setEnabled(false);
		fxtGroupsLabolatory.setEditable(false);
		fxtStudentAmmount.setEditable(false);
	}
}