package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.Main;
import data.DepartmentEntry;
import data.SpecializationEntry;

import java.awt.GridLayout;
import javax.swing.JSplitPane;
import javax.swing.BoxLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.CardLayout;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JOptionPane;

import java.awt.Component;
import java.awt.ComponentOrientation;
import net.miginfocom.swing.MigLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.ListSelectionModel;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class dlgAddOrEditSpecialization extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField txtSubjectInput;
	DepartmentEntry de;
	private static final Logger logger = LogManager.getLogger(dlgAddOrEditSpecialization.class.getName());
	private JButton btnOk;
	private JLabel lblSpecialization;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		try {
			dlgAddSpecialization dialog = new dlgAddSpecialization();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * Create the dialog.
	 */
	public dlgAddOrEditSpecialization() {
		setTitle("NOWA SPECJALIZACJA");
		setBounds(100, 100, 462, 100);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		contentPanel.setAlignmentX(Component.RIGHT_ALIGNMENT);
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{732, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0};
		gbl_contentPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JSplitPane splitPane = new JSplitPane();
			GridBagConstraints gbc_splitPane = new GridBagConstraints();
			gbc_splitPane.fill = GridBagConstraints.BOTH;
			gbc_splitPane.gridx = 0;
			gbc_splitPane.gridy = 0;
			contentPanel.add(splitPane, gbc_splitPane);
			{
				lblSpecialization = new JLabel("SPECJALIZACJA");
				lblSpecialization.setFont(new Font("Tahoma", Font.BOLD, 14));
				splitPane.setLeftComponent(lblSpecialization);
			}
			{
				txtSubjectInput = new JTextField();
				txtSubjectInput.setFont(new Font("Tahoma", Font.BOLD, 14));
				splitPane.setRightComponent(txtSubjectInput);
				txtSubjectInput.setColumns(10);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			buttonPane.setLayout(new GridLayout(0, 2, 0, 0));
			{
				btnOk = new JButton("OK");
				btnOk.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) throws IllegalArgumentException {
						try {
							logger.info("Dodawanie nowej specjalizacji {}", txtSubjectInput.getText());
							de.addSpecialization(txtSubjectInput.getText());
							Main.t.generateTree();
							dispose();
						} catch (IllegalArgumentException iae) {
							logger.info("BLAD: {}", iae.getMessage());
							JOptionPane.showMessageDialog(null, iae.getMessage());
							dispose();
							throw iae;
						}
					}
				});
				btnOk.setFont(new Font("Tahoma", Font.BOLD, 11));
				btnOk.setActionCommand("OK");
				buttonPane.add(btnOk);
				getRootPane().setDefaultButton(btnOk);
			}
			{
				JButton btnCancel = new JButton("Anuluj");
				btnCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				btnCancel.setFont(new Font("Tahoma", Font.BOLD, 11));
				btnCancel.setActionCommand("Cancel");
				buttonPane.add(btnCancel);
			}
		}
	}

	public dlgAddOrEditSpecialization(DepartmentEntry de) {
		this();
		this.de = de;
	}

	public dlgAddOrEditSpecialization(DepartmentEntry de, SpecializationEntry se) {
		this(de);
		setTitle("EDYCJA SPECJALIZACJA");
		txtSubjectInput.setText(se.getName());
		btnOk.setText("EDYTUJ");
		misc.Utils.removeListeners(btnOk);
		
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					logger.info("Edycja specjalizacji ({})", se.getName());
					if (de.checkIfSpecExists(txtSubjectInput.getText())) {
						JOptionPane.showMessageDialog(null, "Taka specjalizacja już istnieje");
					} else {
						se.setName(txtSubjectInput.getText());
						dispose();
					}
					dispose();
				} catch (IllegalArgumentException iae) {
					dispose();
				}
			}
		});
	}

}
