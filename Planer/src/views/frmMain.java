package views;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;

import java.awt.Dimension;
import java.awt.Rectangle;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import javax.swing.BoxLayout;
import javax.swing.JFileChooser;
import javax.swing.JSplitPane;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.DataContainer;
import app.Main;
import data.YearEntry;
import models.planerTree;

import javax.swing.tree.DefaultMutableTreeNode;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

public class frmMain extends JFrame {

	private static Logger logger = LogManager.getLogger(frmMain.class.getName());
	private JPanel contentPane;
	public JSplitPane splitPane;

	/**
	 * Create the frame.
	 */
	public frmMain() {
		setBounds(new Rectangle(0, 28, 800, 600));
		setSize(new Dimension(800, 600));
		setTitle("Planer godzin dydaktycznych");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 894, 600);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("PLIK");
		menuBar.add(mnFile);
		
		JMenuItem mntmNew = new JMenuItem("Nowa baza");
		mntmNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Main.dc = new DataContainer();
				logger.info("Tworzenie nowej bazy; ilosc w dc: {}", Main.dc.getYearsAmmount());
				//Main.t = new planerTree(Main.dc, Main.frame.splitPane);
				//Main.frame.splitPane.setLeftComponent(Main.t.getTree());
				Main.dc.newData();
				//Main.t.refresh();
				Main.t.generateTree();
			}
		});
		mnFile.add(mntmNew);
		
		JMenuItem mntmOpen = new JMenuItem("Otwórz");
		mntmOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					logger.info("Odczyt danych");
					
					JFileChooser chooser = new JFileChooser();
					chooser.setFileFilter(new FileNameExtensionFilter("json file","json"));
					chooser.setDialogType(JFileChooser.OPEN_DIALOG);
					
					int retrival = chooser.showOpenDialog(null);
					if (retrival == JFileChooser.APPROVE_OPTION) {
						logger.debug("Nazwa pliku: {}", chooser.getSelectedFile().toString());
						File selectedFile = chooser.getSelectedFile();
					    if (selectedFile.canRead() && selectedFile.exists()) {
					    	Main.dc.newData();
					    	Main.dc = DataContainer.loadData(selectedFile.toString());
					    	JOptionPane.showMessageDialog(null,
									"Wczytano dane z  pliku: " + chooser.getSelectedFile().toString());
					    	Main.t.generateTree(Main.dc);
					    }
					}
				} catch (Exception e1) {
					logger.info("BLAD {}", e1.getMessage());
					e1.printStackTrace();
					JOptionPane.showMessageDialog(null,
							"Błąd przy odczycie: " + e1.getMessage());
				}
			}
		});
		mnFile.add(mntmOpen);
		
		JMenuItem mntmSave = new JMenuItem("Zapisz");
		mntmSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					logger.info("Zapis danych");
					
					JFileChooser chooser = new JFileChooser();
					chooser.setDialogType(JFileChooser.SAVE_DIALOG);
					chooser.setSelectedFile(new File("baza.json"));
					chooser.setFileFilter(new FileNameExtensionFilter("json file","json"));
					
					int retrival = chooser.showSaveDialog(null);
					if (retrival == JFileChooser.APPROVE_OPTION) {
						logger.debug("Nazwa pliku: {}", chooser.getSelectedFile().toString());
						DataContainer.saveData(chooser.getSelectedFile().toString(), Main.dc);
						JOptionPane.showMessageDialog(null,
								"Zapisano do pliku: " + chooser.getSelectedFile().toString());
					}
				} catch (Exception e) {
					logger.info("BLAD {}", e.getMessage());
					JOptionPane.showMessageDialog(null,
							"Błąd przy zapisie: " + e.getMessage());
					
				}
			}
		});
		mnFile.add(mntmSave);
		
		JMenuItem mntmClose = new JMenuItem("Zamknij");
		mntmClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		mnFile.add(mntmClose);
		
		JMenu mnRokAkademicki = new JMenu("ROK AKADEMICKI");
		menuBar.add(mnRokAkademicki);
		
		JMenuItem mntmDodaj = new JMenuItem("DODAJ");
		mntmDodaj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new dlgAddOrEditAcademicYear().setVisible(true);
			}
		});
		mnRokAkademicki.add(mntmDodaj);
		
		JMenuItem mntmEdytuj = new JMenuItem("EDYTUJ");
		mntmEdytuj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				YearEntry ye = Main.t.getSelectedYear();
				if (null == ye) {
					JOptionPane.showMessageDialog(null, "Nie zaznaczono wpisu w drzewie");
				}
				new dlgAddOrEditAcademicYear(ye).setVisible(true);
			}
		});
		mnRokAkademicki.add(mntmEdytuj);
		
		JMenu mnAdd = new JMenu("DODAJ");
		menuBar.add(mnAdd);
		
		JMenuItem mntmAddYear = new JMenuItem("Rok akademicki");
		mntmAddYear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new dlgAddOrEditAcademicYear().setVisible(true);
			}
		});
		mnAdd.add(mntmAddYear);
		
		JMenuItem mntmAddInstitute = new JMenuItem("Instytut");
		mnAdd.add(mntmAddInstitute);
		
		JMenuItem mntmAddDepartment = new JMenuItem("Wydział");
		mnAdd.add(mntmAddDepartment);
		
		JMenuItem mntmAddDegree = new JMenuItem("Kierunek");
		mnAdd.add(mntmAddDegree);
		
		JMenuItem mntmAddEmployee = new JMenuItem("Pracownika");
		mnAdd.add(mntmAddEmployee);
		
		JMenuItem mntmAddSpecialization = new JMenuItem("Specjalizacje");
		mnAdd.add(mntmAddSpecialization);
		
		JMenuItem mntmAddSubject = new JMenuItem("Przedmiot");
		mnAdd.add(mntmAddSubject);
		
		JMenu mnGenerate = new JMenu("GENERUJ");
		menuBar.add(mnGenerate);
		
		JMenuItem mnTabelZbiorcz = new JMenuItem("Tabelę zbiorczą");
		mnGenerate.add(mnTabelZbiorcz);
		
		JMenu mnPrzedmioty = new JMenu("Przedmioty");
		mnGenerate.add(mnPrzedmioty);
		
		JMenuItem mnStacjonarne = new JMenuItem("Stacjonarne");
		mnPrzedmioty.add(mnStacjonarne);
		
		JMenuItem mnNiestacjonarne = new JMenuItem("Niestacjonarne");
		mnPrzedmioty.add(mnNiestacjonarne);
		
		JMenuItem mnPlanZatrudnienia = new JMenuItem("Plan zatrudnienia");
		mnGenerate.add(mnPlanZatrudnienia);
		
		JMenuItem mnRaportIndywidualny = new JMenuItem("Raport indywidualny");
		mnGenerate.add(mnRaportIndywidualny);
		
		JMenu mnHelp = new JMenu("POMOC");
		menuBar.add(mnHelp);
		
		JMenuItem mntmUsedLibs = new JMenuItem("Użyte biblioteki");
		mntmUsedLibs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog (null,
						"Apache log4j2 (Apache License, Version 2.0)\n"
						+ "GSon (Apache License, Version 2.0)\n"
						+ "iText 5.5.13 (GNU AGPL 3)",
						"Użyte zewnętrzne biblioteki",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		mnHelp.add(mntmUsedLibs);
		
		JMenuItem mntmAbout = new JMenuItem("O programie");
		mntmAbout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane.showMessageDialog (null,
						"Program na pracę inżynierską (2014-2018)\nWykonał Krystian Jedziniak",
						"O programie",
						JOptionPane.INFORMATION_MESSAGE);
			}
		});
		mnHelp.add(mntmAbout);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		
		splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.15);
		contentPane.add(splitPane);
		
		JTree treeMain = new JTree();
		/*treeMain.setModel(new DefaultTreeModel(
			new DefaultMutableTreeNode("JTree") {
				{
					DefaultMutableTreeNode node_1;
					node_1 = new DefaultMutableTreeNode("colors");
						node_1.add(new DefaultMutableTreeNode("blue"));
						node_1.add(new DefaultMutableTreeNode("violet"));
						node_1.add(new DefaultMutableTreeNode("red"));
						node_1.add(new DefaultMutableTreeNode("yellow"));
					add(node_1);
					node_1 = new DefaultMutableTreeNode("sports");
						node_1.add(new DefaultMutableTreeNode("basketball"));
						node_1.add(new DefaultMutableTreeNode("soccer"));
						node_1.add(new DefaultMutableTreeNode("football"));
						node_1.add(new DefaultMutableTreeNode("hockey"));
					add(node_1);
					node_1 = new DefaultMutableTreeNode("food");
						node_1.add(new DefaultMutableTreeNode("hot dogs"));
						node_1.add(new DefaultMutableTreeNode("pizza"));
						node_1.add(new DefaultMutableTreeNode("ravioli"));
						node_1.add(new DefaultMutableTreeNode("bananas"));
					add(node_1);
				}
			}
		));*/
		/*treeMain.setShowsRootHandles(true);
		treeMain.setRootVisible(false);
		splitPane.setLeftComponent(treeMain);*/
		
		JPanel pnlContent = new JPanel();
		splitPane.setRightComponent(pnlContent);
		
		logger.info("Okno utworzone");
	}

}
