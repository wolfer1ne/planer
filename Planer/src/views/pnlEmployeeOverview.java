package views;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.util.List;
import java.awt.Font;
import javax.swing.JSeparator;
import java.awt.Color;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.BoxLayout;
import javax.swing.InputMap;
import javax.swing.table.DefaultTableModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.Main;
import data.DepartmentEntry;
import data.EmployeeEntry;
import data.SubjectEntry;
import data.primitives.EmployeeActivity;
import data.primitives.EmployeeActivityType;
import generators.IndividualRaport;
import generators.PartTimeRaport;
import models.EmployeeActivityTableModel;
import models.SubjectEntryTableModel;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;

public class pnlEmployeeOverview extends JPanel {
	private static final Logger logger = LogManager.getLogger(pnlEmployeeOverview.class.getName());
	private JLabel lblTitleInput;
	private JLabel lblInstituteInput;
	private JLabel lblDepartmentInput;
	private JLabel lblYearInput;
	private JScrollPane scrollPane;
	
	DepartmentEntry de;
	EmployeeEntry ee;
	
	private JTable tblNondidactic;
	InputMap inputMap1;
	ActionMap actionMap1;
	public static EmployeeActivityTableModel eatmNonDidactic;
	
	private JTable tblOrganizational;
	InputMap inputMap2;
	ActionMap actionMap2;
	public static EmployeeActivityTableModel eatmOrganizational;
	
	private JTable tblSubjects;
	InputMap inputMap;
	ActionMap actionMap;
	public static SubjectEntryTableModel setm;
	
	private JButton btnAddSubject;
	private JButton btnAddMiscActivity;
	
	private JTable tblMisc;
	InputMap inputMap3;
	ActionMap actionMap3;
	public static EmployeeActivityTableModel eatmMisc;
	
	void
	addSubjectDeleteKeymap()
	{
		inputMap = tblSubjects.getInputMap(WHEN_FOCUSED);
		actionMap = tblSubjects.getActionMap();
		
		inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "delete");
		actionMap.put("delete", new AbstractAction() {
		    public void actionPerformed(ActionEvent evt) {
		    	int row = tblSubjects.getSelectedRow();
		    	logger.info("Usuwanie przedmiotu {}", setm.getSubjectEntryAt(row).getName());
		    	setm.removeRow(row);
		    }
		});
	}
	
	void
	addEmployeeActivityDeleteKeymap(InputMap im, ActionMap am, JTable tbl, EmployeeActivityTableModel eatm)
	{
		im = tbl.getInputMap(WHEN_FOCUSED);
		am = tbl.getActionMap();
		
		im.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), "delete");
		am.put("delete", new AbstractAction() {
		    public void actionPerformed(ActionEvent evt) {
		    	int row = tbl.getSelectedRow();
		    	EmployeeActivity ea;
		    	try {
		    		ea = eatm.getActivityEntryAt(row);
		    	} catch (ArrayIndexOutOfBoundsException aioobe) {
		    		return;
		    	}
		    	logger.info("Usuwanie dodatkowej aktywności {}:{}",
		    			ea.getType().toString(),
		    			ea.toString()
		    			);
		    	eatm.removeRow(row);
		    }
		});
	}
	
	/**
	 * Create the panel.
	 */
	public pnlEmployeeOverview() {	
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 50, 50, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 369, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 1.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		lblTitleInput = new JLabel("[DEGREE NAME SURNAME]");
		lblTitleInput.setFont(new Font("Tahoma", Font.BOLD, 20));
		GridBagConstraints gbc_lblTitleInput = new GridBagConstraints();
		gbc_lblTitleInput.gridwidth = 5;
		gbc_lblTitleInput.insets = new Insets(0, 0, 5, 0);
		gbc_lblTitleInput.gridx = 0;
		gbc_lblTitleInput.gridy = 0;
		add(lblTitleInput, gbc_lblTitleInput);
		
		lblInstituteInput = new JLabel("");
		lblInstituteInput.setFont(new Font("Tahoma", Font.BOLD, 16));
		GridBagConstraints gbc_lblInstituteInput = new GridBagConstraints();
		gbc_lblInstituteInput.gridwidth = 3;
		gbc_lblInstituteInput.insets = new Insets(0, 0, 5, 5);
		gbc_lblInstituteInput.gridx = 0;
		gbc_lblInstituteInput.gridy = 1;
		add(lblInstituteInput, gbc_lblInstituteInput);
		
		lblDepartmentInput = new JLabel("");
		lblDepartmentInput.setFont(new Font("Tahoma", Font.BOLD, 16));
		GridBagConstraints gbc_lblDepartmentInput = new GridBagConstraints();
		gbc_lblDepartmentInput.gridwidth = 2;
		gbc_lblDepartmentInput.insets = new Insets(0, 0, 5, 0);
		gbc_lblDepartmentInput.gridx = 3;
		gbc_lblDepartmentInput.gridy = 1;
		add(lblDepartmentInput, gbc_lblDepartmentInput);
		
		JLabel lblPrzedmioty = new JLabel("PRZEDMIOTY");
		lblPrzedmioty.setFont(new Font("Tahoma", Font.BOLD, 16));
		GridBagConstraints gbc_lblPrzedmioty = new GridBagConstraints();
		gbc_lblPrzedmioty.anchor = GridBagConstraints.EAST;
		gbc_lblPrzedmioty.insets = new Insets(0, 0, 5, 5);
		gbc_lblPrzedmioty.gridx = 0;
		gbc_lblPrzedmioty.gridy = 2;
		add(lblPrzedmioty, gbc_lblPrzedmioty);
		
		btnAddSubject = new JButton("DODAJ PRZEDMIOT");
		btnAddSubject.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new dlgAddOrEditSubjectEntry(ee,
						de, // TODO
						setm).setVisible(true);
			}
		});
		btnAddSubject.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_btnAddSubject = new GridBagConstraints();
		gbc_btnAddSubject.anchor = GridBagConstraints.EAST;
		gbc_btnAddSubject.insets = new Insets(0, 0, 5, 5);
		gbc_btnAddSubject.gridx = 1;
		gbc_btnAddSubject.gridy = 2;
		add(btnAddSubject, gbc_btnAddSubject);
		
		JButton btnIndividualRaport = new JButton("RAPORT INDYWIDUALNY");
		btnIndividualRaport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {			
				try {
					JFileChooser jfc = misc.Utils.getPdfSaveChooser();
					int retrival = jfc.showSaveDialog(null);
					if (retrival == JFileChooser.APPROVE_OPTION) {
						logger.debug("Raport indywidualny, plik: {}", jfc.getSelectedFile().toString());
						logger.debug("Sciezka pliku {}", pnlEmployeeOverview.class.getClassLoader().getResource(misc.Utils.LOGO_FILE_NAME).getPath());
						IndividualRaport ir = new IndividualRaport(de,
								ee,
								pnlEmployeeOverview.class.getClassLoader().getResource(misc.Utils.LOGO_FILE_NAME).getPath());
						ir.Generate(jfc.getSelectedFile().toString());
						JOptionPane.showMessageDialog(null,
								"Wygenerowano do pliku: " + jfc.getSelectedFile().toString());
					}
				} catch (Exception e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null,
							"Błąd w trakcie tworzenia tabeli " +e.getMessage());
				}
			}
		});
		btnIndividualRaport.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_btnIndividualRaport = new GridBagConstraints();
		gbc_btnIndividualRaport.insets = new Insets(0, 0, 5, 5);
		gbc_btnIndividualRaport.gridx = 2;
		gbc_btnIndividualRaport.gridy = 2;
		add(btnIndividualRaport, gbc_btnIndividualRaport);
		
		lblYearInput = new JLabel("");
		lblYearInput.setFont(new Font("Tahoma", Font.BOLD, 20));
		GridBagConstraints gbc_lblYearInput = new GridBagConstraints();
		gbc_lblYearInput.insets = new Insets(0, 0, 5, 0);
		gbc_lblYearInput.gridx = 4;
		gbc_lblYearInput.gridy = 2;
		add(lblYearInput, gbc_lblYearInput);
		
		JSeparator separator = new JSeparator();
		separator.setBackground(Color.BLACK);
		separator.setForeground(Color.BLACK);
		GridBagConstraints gbc_separator = new GridBagConstraints();
		gbc_separator.fill = GridBagConstraints.BOTH;
		gbc_separator.gridwidth = 5;
		gbc_separator.insets = new Insets(0, 0, 5, 0);
		gbc_separator.gridx = 0;
		gbc_separator.gridy = 3;
		add(separator, gbc_separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setForeground(Color.BLACK);
		separator_1.setBackground(Color.BLACK);
		GridBagConstraints gbc_separator_1 = new GridBagConstraints();
		gbc_separator_1.fill = GridBagConstraints.BOTH;
		gbc_separator_1.gridwidth = 5;
		gbc_separator_1.insets = new Insets(0, 0, 5, 0);
		gbc_separator_1.gridx = 0;
		gbc_separator_1.gridy = 4;
		add(separator_1, gbc_separator_1);
		
		scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridwidth = 5;
		gbc_scrollPane.insets = new Insets(0, 0, 5, 0);
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 5;
		add(scrollPane, gbc_scrollPane);
		
		JLabel lblNondidactical = new JLabel("POZADYDAKTYCZNE");
		GridBagConstraints gbc_lblNondidactical = new GridBagConstraints();
		gbc_lblNondidactical.insets = new Insets(0, 0, 5, 5);
		gbc_lblNondidactical.gridx = 0;
		gbc_lblNondidactical.gridy = 6;
		add(lblNondidactical, gbc_lblNondidactical);
		lblNondidactical.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		JButton btnOtherDuties = new JButton("DODAJ POZADYDAKTYCZNE");
		btnOtherDuties.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new dlgAddOrEditEmployeeActivity(ee,
						EmployeeActivityType.NONDIDACTIC,
						eatmNonDidactic).setVisible(true);
			}
		});
		
		JScrollPane scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.gridheight = 3;
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridwidth = 3;
		gbc_scrollPane_1.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane_1.gridx = 1;
		gbc_scrollPane_1.gridy = 6;
		add(scrollPane_1, gbc_scrollPane_1);
		
		tblNondidactic = new JTable();
		//tblNondidactic.setDefaultEditor(Object.class, null);
		scrollPane_1.setViewportView(tblNondidactic);
		btnOtherDuties.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_btnOtherDuties = new GridBagConstraints();
		gbc_btnOtherDuties.insets = new Insets(0, 0, 5, 5);
		gbc_btnOtherDuties.gridx = 0;
		gbc_btnOtherDuties.gridy = 7;
		add(btnOtherDuties, gbc_btnOtherDuties);
		
		JLabel lblOrganizational = new JLabel("ORGANIZACYJNE");
		GridBagConstraints gbc_lblOrganizational = new GridBagConstraints();
		gbc_lblOrganizational.insets = new Insets(0, 0, 5, 5);
		gbc_lblOrganizational.gridx = 0;
		gbc_lblOrganizational.gridy = 9;
		add(lblOrganizational, gbc_lblOrganizational);
		lblOrganizational.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		JButton btnOrganizationDuties = new JButton("DODAJ ORGANIZACYJNE");
		btnOrganizationDuties.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new dlgAddOrEditEmployeeActivity(ee,
						EmployeeActivityType.ORGANIZATIONAL,
						eatmOrganizational).setVisible(true);
			}
		});
		
		JScrollPane scrollPane_2 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_2 = new GridBagConstraints();
		gbc_scrollPane_2.gridheight = 3;
		gbc_scrollPane_2.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_2.gridwidth = 3;
		gbc_scrollPane_2.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane_2.gridx = 1;
		gbc_scrollPane_2.gridy = 9;
		add(scrollPane_2, gbc_scrollPane_2);
		
		tblOrganizational = new JTable();
		//tblOrganizational.setDefaultEditor(Object.class, null);
		scrollPane_2.setViewportView(tblOrganizational);
		btnOrganizationDuties.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_btnOrganizationDuties = new GridBagConstraints();
		gbc_btnOrganizationDuties.insets = new Insets(0, 0, 5, 5);
		gbc_btnOrganizationDuties.gridx = 0;
		gbc_btnOrganizationDuties.gridy = 10;
		add(btnOrganizationDuties, gbc_btnOrganizationDuties);
		
		JLabel lblMiscDuties = new JLabel("INNE TYTUŁY");
		lblMiscDuties.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_lblMiscDuties = new GridBagConstraints();
		gbc_lblMiscDuties.insets = new Insets(0, 0, 5, 5);
		gbc_lblMiscDuties.gridx = 0;
		gbc_lblMiscDuties.gridy = 12;
		add(lblMiscDuties, gbc_lblMiscDuties);
		
		JScrollPane scrollPane_3 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_3 = new GridBagConstraints();
		gbc_scrollPane_3.gridheight = 3;
		gbc_scrollPane_3.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_3.gridwidth = 3;
		gbc_scrollPane_3.insets = new Insets(0, 0, 5, 5);
		gbc_scrollPane_3.gridx = 1;
		gbc_scrollPane_3.gridy = 12;
		add(scrollPane_3, gbc_scrollPane_3);
		
		tblMisc = new JTable();
		scrollPane_3.setViewportView(tblMisc);
		
		btnAddMiscActivity = new JButton("DODAJ INNE");
		btnAddMiscActivity.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new dlgAddOrEditEmployeeActivity(ee,
						EmployeeActivityType.MISC,
						eatmMisc).setVisible(true);
			}
		});
		btnAddMiscActivity.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_btnAddMiscActivity = new GridBagConstraints();
		gbc_btnAddMiscActivity.insets = new Insets(0, 0, 5, 5);
		gbc_btnAddMiscActivity.gridx = 0;
		gbc_btnAddMiscActivity.gridy = 13;
		add(btnAddMiscActivity, gbc_btnAddMiscActivity);

	}

	public pnlEmployeeOverview(DepartmentEntry de, EmployeeEntry nodeInfo) {
		this();
		this.de = de;
		ee = nodeInfo;
		
		lblInstituteInput.setText("");
		lblDepartmentInput.setText("");
		lblYearInput.setText("");
		lblTitleInput.setText(nodeInfo.getDegree() + " " + nodeInfo.getName() + " " + nodeInfo.getSurname());
		
		//tblSubjects = new JTable();
		setm = new SubjectEntryTableModel(nodeInfo.getSubjects());
		tblSubjects = new JTable(setm);
		scrollPane.setViewportView(tblSubjects);
		
		// Podwojne klikniecie do edycji
		tblSubjects.addMouseListener(new MouseAdapter(){
		    public void mouseClicked(MouseEvent evnt) {
		        if (evnt.getClickCount() == 2) {
                    int row = tblSubjects.getSelectedRow();
                    SubjectEntry se = setm.getSubjectEntryAt(row);
                    
                    new dlgAddOrEditSubjectEntry(ee,
    						de, // TODO
    						setm,
    						se).setVisible(true);
		         }
		     }
		});

		eatmNonDidactic = new EmployeeActivityTableModel(ee.getNondidactical());
		tblNondidactic.setModel(eatmNonDidactic);
		addEmployeeActivityDeleteKeymap(inputMap1, actionMap1, tblNondidactic, eatmNonDidactic);
		
		eatmOrganizational = new EmployeeActivityTableModel(ee.getOrganizational());
		tblOrganizational.setModel(eatmOrganizational);
		addEmployeeActivityDeleteKeymap(inputMap2, actionMap2, tblOrganizational, eatmOrganizational);
		
		eatmMisc = new EmployeeActivityTableModel(ee.getMiscActivities());
		tblMisc.setModel(eatmMisc);
		addEmployeeActivityDeleteKeymap(inputMap3, actionMap3, tblMisc, eatmMisc);
		
		addSubjectDeleteKeymap();
	}

}
