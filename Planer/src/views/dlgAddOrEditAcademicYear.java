package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.Main;
import data.YearEntry;

import javax.swing.BoxLayout;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Dimension;
import javax.swing.JFormattedTextField;
import java.awt.Font;
import javax.swing.JSplitPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class dlgAddOrEditAcademicYear extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JFormattedTextField fmtYearTxtInput;
	private static final Logger logger = LogManager.getLogger(dlgAddOrEditAcademicYear.class.getName());
	private JButton btnOk;
	private JLabel lblYearBegin;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		try {
			dlgAddOrEditAcademicYear dialog = new dlgAddOrEditAcademicYear();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * Create the dialog.
	 */
	public dlgAddOrEditAcademicYear() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setTitle("NOWY ROK AKADEMICKI");
		setBounds(100, 100, 200, 130);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			lblYearBegin = new JLabel("Rok rozpoczęcia");
			lblYearBegin.setFont(new Font("Tahoma", Font.BOLD, 20));
			contentPanel.add(lblYearBegin, BorderLayout.NORTH);
		}
		{
			fmtYearTxtInput = new JFormattedTextField();
			fmtYearTxtInput.setFont(new Font("Tahoma", Font.BOLD, 15));
			fmtYearTxtInput.setColumns(6);
			contentPanel.add(fmtYearTxtInput, BorderLayout.WEST);
		}
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			buttonPane.setLayout(new GridLayout(0, 2, 0, 0));
			{
				btnOk = new JButton("OK");
				btnOk.setFont(new Font("Tahoma", Font.BOLD, 11));
				btnOk.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							logger.info("Dodawanie nowego roku {}", fmtYearTxtInput.getText());
							Main.dc.addYear(fmtYearTxtInput.getText());
							Main.t.generateTree();
							dispose();
						} catch (IllegalArgumentException iae) {
							logger.info("BLAD: {}", iae.getMessage());
							JOptionPane.showMessageDialog(null, iae.getMessage());
							dispose();
						}
					}
				});
				btnOk.setActionCommand("OK");
				buttonPane.add(btnOk);
				getRootPane().setDefaultButton(btnOk);
			}
			{
				JButton btnCancel = new JButton("Anuluj");
				btnCancel.setFont(new Font("Tahoma", Font.BOLD, 11));
				btnCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				btnCancel.setActionCommand("Cancel");
				buttonPane.add(btnCancel);
			}
		}
	}

	public dlgAddOrEditAcademicYear(YearEntry ye) {
		this();
		
		setTitle("EDYCJA ROKU AKADEMICKIEGO");
		lblYearBegin.setText("Zmiana: " + ye.getName());
		fmtYearTxtInput.setText(ye.getName());
		btnOk.setText("EDYTUJ");
		misc.Utils.removeListeners(btnOk);
		
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					logger.info("Edycja roku akademickiego ({})", ye.getName());
					if (Main.dc.checkIfExists(fmtYearTxtInput.getText())) {
						JOptionPane.showMessageDialog(null, "Taki rok już istnieje");
					} else {
						ye.setName(fmtYearTxtInput.getText());
						dispose();
					}
					dispose();
				} catch (IllegalArgumentException iae) {
					dispose();
				}
			}
		});
	}

}
