package views;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Font;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.Main;
import data.InstituteEntry;
import data.YearEntry;
import models.EditButton;
import models.RemoveButton;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;

public class pnlYearOverview extends JPanel implements ActionListener {

	private JLabel lblYearInput;
	private JPanel pnlInstitutes;
	private JScrollPane scrollPane_1;
	private JButton btnDodajInstytut;
	private YearEntry ye;
	
	private static final Logger logger = LogManager.getLogger(pnlYearOverview.class.getName());

	/**
	 * Create the panel.
	 */
	public pnlYearOverview() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 5, 148, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		lblYearInput = new JLabel("[YEAR PLACEHOLDER]");
		lblYearInput.setFont(new Font("Tahoma", Font.BOLD, 20));
		GridBagConstraints gbc_lblYearInput = new GridBagConstraints();
		gbc_lblYearInput.gridwidth = 3;
		gbc_lblYearInput.insets = new Insets(0, 0, 5, 0);
		gbc_lblYearInput.gridx = 0;
		gbc_lblYearInput.gridy = 0;
		add(lblYearInput, gbc_lblYearInput);
		
		JLabel lblInstitutes = new JLabel("LISTA INSTYTUTÓW");
		lblInstitutes.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_lblInstitutes = new GridBagConstraints();
		gbc_lblInstitutes.insets = new Insets(0, 0, 5, 5);
		gbc_lblInstitutes.gridx = 0;
		gbc_lblInstitutes.gridy = 1;
		add(lblInstitutes, gbc_lblInstitutes);
		
		btnDodajInstytut = new JButton("DODAJ INSTYTUT");
		btnDodajInstytut.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new dlgAddOrEditInstitute(ye).setVisible(true);;
			}
		});
		btnDodajInstytut.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_btnDodajInstytut = new GridBagConstraints();
		gbc_btnDodajInstytut.insets = new Insets(0, 0, 5, 5);
		gbc_btnDodajInstytut.gridx = 1;
		gbc_btnDodajInstytut.gridy = 1;
		add(btnDodajInstytut, gbc_btnDodajInstytut);
		
		scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.gridwidth = 3;
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridx = 0;
		gbc_scrollPane_1.gridy = 2;
		add(scrollPane_1, gbc_scrollPane_1);
		
		pnlInstitutes = new JPanel();
		scrollPane_1.setViewportView(pnlInstitutes);
		pnlInstitutes.setLayout(new GridLayout(0, 2, 2, 2));
	}
	
	public pnlYearOverview(YearEntry ye) {
		this();
		this.ye = ye;
		lblYearInput.setText(ye.getName());
		
		for (InstituteEntry ie : ye.getInstitutes()) {
			//pnlInstitutes.add(new JLabel(ie.getName()));
			/*JLabel institute = new JLabel(ie.getName(), SwingConstants.CENTER);
			institute.setFont(new Font("Tahoma", Font.BOLD, 15));*/
			//((JPanel) scrollPane.getViewport().getView()).add(institute);
			//pnlInstitutes.add(institute);
			EditButton eBtn = new EditButton(ie.toString(), ie);
			eBtn.setFont(new Font("Tahoma", Font.BOLD, 20));
			eBtn.addActionListener(this);
			pnlInstitutes.add(eBtn);
			
			RemoveButton rBtn = new RemoveButton(ie);
			rBtn.addActionListener(this);
			pnlInstitutes.add(rBtn);
		}
		//pnlInstitutes.revalidate();
		logger.info("Podgląd roku {}; ilość instytutów {}", ye.getName(), ye.getInstitutes().size());
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		Object e = ae.getSource();
		if (e instanceof RemoveButton) {
			RemoveButton obj = (RemoveButton) e;
			InstituteEntry ie = (InstituteEntry) obj.getObject();
			logger.info("Usuwanie instytutu {}", ie.getName());
			
			if (ye.removeInsitute(ie)) {
				pnlInstitutes.remove(obj);
				Main.t.generateTree();
			}
		}
		else if (e instanceof EditButton) {
			EditButton obj = (EditButton) e;
			InstituteEntry ie = (InstituteEntry) obj.getObject();
			logger.info("Edycja instytutu {}", ie.getName());
			
			new dlgAddOrEditInstitute(ye, ie).setVisible(true);
		}
	}
}
