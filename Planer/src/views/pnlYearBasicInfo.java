package views;

import javax.swing.JPanel;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JSplitPane;
import java.awt.Font;
import javax.swing.JTextField;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.Main;
import data.SpecializationEntry;
import data.primitives.SemesterBasicInfo;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.BorderLayout;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;

public class pnlYearBasicInfo extends JPanel {
	private static final Logger logger = LogManager.getLogger(pnlYearBasicInfo.class.getName());
	private JTextField txtStudentsAmmount;
	private JTextField txtGroupsAuditory;
	private JTextField txtGroupsLabolatory;
	private JTextField txtGroupsLanguages;
	private JTextField txtGroupsPE;
	SemesterBasicInfo sbi;
	private JTextField txtGroupsSeminary;
	
	void
	saveData()
	{
		try {
			sbi.setAmmountOfStudents(Long.valueOf(txtStudentsAmmount.getText()));
			sbi.setGroupsAuditory(Long.valueOf(txtGroupsAuditory.getText()));
			sbi.setGroupsLabolatory(Long.valueOf(txtGroupsLabolatory.getText()));
			sbi.setGroupsForeignLanguages(Long.valueOf(txtGroupsLanguages.getText()));
			sbi.setGroupsPE(Long.valueOf(txtGroupsPE.getText()));
			sbi.setGroupsSeminary(Long.valueOf(txtGroupsSeminary.getText()));
		} catch (NumberFormatException nfe) {
			logger.info("BLAD: {}", nfe.getMessage());
			JOptionPane.showMessageDialog(null, nfe.getMessage());
		}
	}
	
	void
	yearOneSettings ()
	{
		logger.debug("rok 1");
		txtGroupsPE.setEnabled(true);
		txtGroupsPE.setEditable(true);
		txtGroupsLanguages.setEnabled(true);
		txtGroupsLanguages.setEditable(true);
	}
	
	void
	yearTwoSettings()
	{
		logger.debug("rok 2");
		txtGroupsPE.setEnabled(false);
		txtGroupsLanguages.setEnabled(true);
		txtGroupsPE.setEditable(false);
		txtGroupsLanguages.setEditable(true);
	}
	
	void
	yearThreeAndFour()
	{
		logger.debug("rok 3 i 4");
		yearTwoSettings();
		txtGroupsLanguages.setEnabled(false);
		txtGroupsLanguages.setEditable(false);
	}

	/**
	 * Create the panel.
	 */
	public pnlYearBasicInfo() {
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		JSplitPane splitPane = new JSplitPane();
		add(splitPane);
		splitPane.setResizeWeight(0.5);
		
		JLabel lblStudentsAmmount = new JLabel("LICZBA STUDENTÓW");
		lblStudentsAmmount.setFont(new Font("Tahoma", Font.BOLD, 12));
		splitPane.setLeftComponent(lblStudentsAmmount);
		
		txtStudentsAmmount = new JTextField();
		txtStudentsAmmount.setHorizontalAlignment(SwingConstants.CENTER);
		txtStudentsAmmount.setFont(new Font("Tahoma", Font.BOLD, 12));
		splitPane.setRightComponent(txtStudentsAmmount);
		
		JSplitPane splitPane_1 = new JSplitPane();
		add(splitPane_1);
		splitPane_1.setResizeWeight(0.5);
		
		JLabel lblGroupsAuditory = new JLabel("GRUPY AUDYTORYJNE");
		lblGroupsAuditory.setFont(new Font("Tahoma", Font.BOLD, 12));
		splitPane_1.setLeftComponent(lblGroupsAuditory);
		
		txtGroupsAuditory = new JTextField();
		txtGroupsAuditory.setHorizontalAlignment(SwingConstants.CENTER);
		txtGroupsAuditory.setFont(new Font("Tahoma", Font.BOLD, 12));
		splitPane_1.setRightComponent(txtGroupsAuditory);
		
		JSplitPane splitPane_2 = new JSplitPane();
		add(splitPane_2);
		splitPane_2.setResizeWeight(0.5);
		
		JLabel lblGroupsLabolatory = new JLabel("GRUPY LABOLATORYJNE");
		lblGroupsLabolatory.setFont(new Font("Tahoma", Font.BOLD, 12));
		splitPane_2.setLeftComponent(lblGroupsLabolatory);
		
		txtGroupsLabolatory = new JTextField();
		txtGroupsLabolatory.setHorizontalAlignment(SwingConstants.CENTER);
		txtGroupsLabolatory.setFont(new Font("Tahoma", Font.BOLD, 12));
		splitPane_2.setRightComponent(txtGroupsLabolatory);
		
		JSplitPane splitPane_3 = new JSplitPane();
		add(splitPane_3);
		splitPane_3.setResizeWeight(0.5);
		
		JLabel lblGroupsLanguages = new JLabel("GRUPY JĘZYKOWE");
		lblGroupsLanguages.setFont(new Font("Tahoma", Font.BOLD, 12));
		splitPane_3.setLeftComponent(lblGroupsLanguages);
		
		txtGroupsLanguages = new JTextField();
		txtGroupsLanguages.setHorizontalAlignment(SwingConstants.CENTER);
		txtGroupsLanguages.setFont(new Font("Tahoma", Font.BOLD, 12));
		splitPane_3.setRightComponent(txtGroupsLanguages);
		
		JSplitPane splitPane_4 = new JSplitPane();
		add(splitPane_4);
		splitPane_4.setResizeWeight(0.5);
		
		JLabel lblGroupsPE = new JLabel("GRUPY WF");
		lblGroupsPE.setFont(new Font("Tahoma", Font.BOLD, 12));
		splitPane_4.setLeftComponent(lblGroupsPE);
		
		txtGroupsPE = new JTextField();
		txtGroupsPE.setHorizontalAlignment(SwingConstants.CENTER);
		txtGroupsPE.setFont(new Font("Tahoma", Font.BOLD, 12));
		splitPane_4.setRightComponent(txtGroupsPE);
		
		JSplitPane splitPane_5 = new JSplitPane();
		splitPane_5.setResizeWeight(0.5);
		add(splitPane_5);
		
		JLabel lblSeminaryGroup = new JLabel("GRUPY SEMINARYJNE");
		lblSeminaryGroup.setFont(new Font("Tahoma", Font.BOLD, 12));
		splitPane_5.setLeftComponent(lblSeminaryGroup);
		
		txtGroupsSeminary = new JTextField();
		txtGroupsSeminary.setHorizontalAlignment(SwingConstants.CENTER);
		txtGroupsSeminary.setFont(new Font("Tahoma", Font.BOLD, 12));
		splitPane_5.setRightComponent(txtGroupsSeminary);
		
	}

	public pnlYearBasicInfo(SemesterBasicInfo value) {
		this();
		sbi = value;
		
		txtStudentsAmmount.setText(String.valueOf(sbi.getAmmountOfStudents()));
		txtGroupsAuditory.setText(String.valueOf(sbi.getGroupsAuditory()));
		txtGroupsLabolatory.setText(String.valueOf(sbi.getGroupsLabolatory()));
		txtGroupsLanguages.setText(String.valueOf(sbi.getGroupsForeignLanguages()));
		txtGroupsPE.setText(String.valueOf(sbi.getGroupsPE()));
		txtGroupsSeminary.setText(String.valueOf(sbi.getGroupsSeminary()));
	}
}