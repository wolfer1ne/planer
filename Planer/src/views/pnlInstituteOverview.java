package views;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Font;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.Main;
import data.DepartmentEntry;
import data.InstituteEntry;
import models.EditButton;
import models.RemoveButton;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;

public class pnlInstituteOverview extends JPanel implements ActionListener {

	private JLabel lblTitle;
	private JPanel pnlDepartments;
	private InstituteEntry ie;
	private static final Logger logger = LogManager.getLogger(pnlInstituteOverview.class.getName());

	/**
	 * Create the panel.
	 */
	public pnlInstituteOverview() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		lblTitle = new JLabel("[INSTITUTE PLACEHOLDER]");
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 20));
		GridBagConstraints gbc_lblTitle = new GridBagConstraints();
		gbc_lblTitle.gridwidth = 3;
		gbc_lblTitle.insets = new Insets(0, 0, 5, 0);
		gbc_lblTitle.gridx = 0;
		gbc_lblTitle.gridy = 0;
		add(lblTitle, gbc_lblTitle);
		
		JLabel lblListaWydziaw = new JLabel("LISTA KIERUNKÓW");
		lblListaWydziaw.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_lblListaWydziaw = new GridBagConstraints();
		gbc_lblListaWydziaw.insets = new Insets(0, 0, 5, 5);
		gbc_lblListaWydziaw.gridx = 0;
		gbc_lblListaWydziaw.gridy = 1;
		add(lblListaWydziaw, gbc_lblListaWydziaw);
		
		JButton btnAddDepartment = new JButton("DODAJ KIERUNEK");
		btnAddDepartment.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new dlgAddOrEditDepartment(ie).setVisible(true);
			}
		});
		btnAddDepartment.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_btnAddDepartment = new GridBagConstraints();
		gbc_btnAddDepartment.gridwidth = 2;
		gbc_btnAddDepartment.insets = new Insets(0, 0, 5, 5);
		gbc_btnAddDepartment.gridx = 1;
		gbc_btnAddDepartment.gridy = 1;
		add(btnAddDepartment, gbc_btnAddDepartment);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.gridwidth = 3;
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridx = 0;
		gbc_scrollPane_1.gridy = 2;
		add(scrollPane_1, gbc_scrollPane_1);
		
		pnlDepartments = new JPanel();
		scrollPane_1.setViewportView(pnlDepartments);
		pnlDepartments.setLayout(new GridLayout(0, 2, 0, 0));

	}

	public pnlInstituteOverview(InstituteEntry nodeInfo) {
		this();
		ie = nodeInfo;
		lblTitle.setText("Instytut " + ie.getName());
		
		for (DepartmentEntry de : ie.getDepartments()) {
			/*JLabel department = new JLabel(de.getName());
			department.setFont(new Font("Tahoma", Font.BOLD, 15));
			pnlDepartments.add(department);*/
			
			EditButton eBtn = new EditButton(de.toString(), de);
			eBtn.setFont(new Font("Tahoma", Font.BOLD, 20));
			eBtn.addActionListener(this);
			pnlDepartments.add(eBtn);		
			
			RemoveButton rBtn = new RemoveButton(de);
			rBtn.addActionListener(this);
			pnlDepartments.add(rBtn);
		}		
		logger.info("Podgląd instytutu {}; ilość kierunków {}", ie.getName(), ie.getDepartments().size());
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		Object e = ae.getSource();
		if (e instanceof RemoveButton) {
			RemoveButton obj = (RemoveButton) e;
			DepartmentEntry de = (DepartmentEntry) obj.getObject();
			logger.info("Usuwanie kierunku {}", de.getName());
		
			if (ie.removeDepartment(de)) {
				pnlDepartments.remove(obj);
				Main.t.generateTree();
			}
		}
		else if (e instanceof EditButton) {
			EditButton obj = (EditButton) e;
			DepartmentEntry de = (DepartmentEntry) obj.getObject();
			logger.info("Edycja instytutu {}", ie.getName());
			
			new dlgAddOrEditDepartment(ie, de).setVisible(true);
		}
	}
}
