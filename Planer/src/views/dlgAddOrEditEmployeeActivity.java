package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import data.EmployeeEntry;
import data.primitives.EmployeeActivity;
import data.primitives.EmployeeActivityType;
import misc.Utils;
import models.EmployeeActivityTableModel;

import javax.swing.BoxLayout;
import java.awt.GridLayout;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class dlgAddOrEditEmployeeActivity extends JDialog {

	private static final Logger logger = LogManager.getLogger(dlgAddOrEditEmployeeActivity.class.getName());
	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	EmployeeEntry ee;
	EmployeeActivityType eat;
	EmployeeActivityTableModel eatm;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		try {
			dlgAddEmployeeActivity dialog = new dlgAddEmployeeActivity();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * Create the dialog.
	 */
	public dlgAddOrEditEmployeeActivity() {
		setTitle("NOWY WPIS");
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 92);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			textField = new JTextField();
			textField.setFont(new Font("Tahoma", Font.BOLD, 12));
			contentPanel.add(textField);
			textField.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			buttonPane.setLayout(new GridLayout(0, 2, 0, 0));
			{
				JButton btnOk = new JButton("OK");
				btnOk.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						logger.info("Dodawanie wpisu {}", eat.toString());
						try {
							switch (eat) {
								case NONDIDACTIC: { 
									EmployeeActivity ea = Utils.makeNondidacticActivity(textField.getText());
									//ee.addNondidacticalActivity(ea);
									eatm.addRow(ea);
									break;
								}
								case ORGANIZATIONAL: { 
									EmployeeActivity ea = Utils.makeOrganizationalActivity(textField.getText());
									//ee.addOrganizationalActivity(ea);
									eatm.addRow(ea);
									break;
								}
							case MISC: {
								EmployeeActivity ea = Utils.makeMiscActivity(textField.getText());
								eatm.addRow(ea);
								break;
							}
							default:
								break;
							}
							//pnlEmployeeOverview.refreshTables();
							dispose();
						} catch (Exception ex) {
							logger.info("BLAD: {}", ex.getMessage());
							JOptionPane.showMessageDialog(null, ex.getMessage());
							dispose();
						}
					}
				});
				btnOk.setFont(new Font("Tahoma", Font.BOLD, 11));
				btnOk.setActionCommand("OK");
				buttonPane.add(btnOk);
				getRootPane().setDefaultButton(btnOk);
			}
			{
				JButton btnCancel = new JButton("Anuluj");
				btnCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				btnCancel.setFont(new Font("Tahoma", Font.BOLD, 11));
				btnCancel.setActionCommand("Cancel");
				buttonPane.add(btnCancel);
			}
		}
	}

	public dlgAddOrEditEmployeeActivity(EmployeeEntry ee, EmployeeActivityType eat, EmployeeActivityTableModel eatm) {
		this();
		this.ee = ee;
		this.eat = eat;
		this.eatm = eatm;
	}

}
