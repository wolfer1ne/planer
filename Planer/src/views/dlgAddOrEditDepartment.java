package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.Main;
import data.DepartmentEntry;
import data.InstituteEntry;

import java.awt.GridLayout;
import javax.swing.JLabel;
import java.awt.List;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.JTextField;
import javax.swing.BoxLayout;
import java.awt.Panel;
import javax.swing.JSplitPane;
import java.awt.Component;
import javax.swing.AbstractListModel;
import java.awt.Font;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class dlgAddOrEditDepartment extends JDialog {
	private JTextField txtDepartment;
	private static final Logger logger = LogManager.getLogger(dlgAddOrEditDepartment.class.getName());
	InstituteEntry ie;
	private JButton btnOk;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		try {
			dlgAddOrEditDepartment dialog = new dlgAddOrEditDepartment();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * Create the dialog.
	 */
	public dlgAddOrEditDepartment() {
		setTitle("NOWY KIERUNEK");
		setBounds(100, 100, 298, 85);
		getContentPane().setLayout(new BorderLayout());
		{
			txtDepartment = new JTextField();
			getContentPane().add(txtDepartment, BorderLayout.CENTER);
			txtDepartment.setFont(new Font("Tahoma", Font.BOLD, 11));
			txtDepartment.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			buttonPane.setLayout(new GridLayout(0, 2, 0, 0));
			{
				btnOk = new JButton("OK");
				btnOk.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							logger.info("Dodawanie nowego kierunku {}", txtDepartment.getText());
							ie.addDepartment(txtDepartment.getText());
							Main.t.generateTree();
							dispose();
						} catch (IllegalArgumentException iae) {
							logger.info("BLAD: {}", iae.getMessage());
							JOptionPane.showMessageDialog(null, iae.getMessage());
							dispose();
						}
					}
				});
				btnOk.setFont(new Font("Tahoma", Font.BOLD, 11));
				btnOk.setActionCommand("OK");
				buttonPane.add(btnOk);
				getRootPane().setDefaultButton(btnOk);
			}
			{
				JButton btnCancel = new JButton("Anuluj");
				btnCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						dispose();
					}
				});
				btnCancel.setFont(new Font("Tahoma", Font.BOLD, 11));
				btnCancel.setActionCommand("Cancel");
				buttonPane.add(btnCancel);
			}
		}
	}

	public dlgAddOrEditDepartment(InstituteEntry ie) {
		this();
		this.ie = ie;
	}

	public dlgAddOrEditDepartment(InstituteEntry ie, DepartmentEntry de) {
		this();
		setTitle("EDYCJA KIERUNKU: " + de.getName());
		txtDepartment.setText(de.getName());
		btnOk.setText("EDYTUJ");
		misc.Utils.removeListeners(btnOk);
		
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					logger.info("Edycja kierunku ({})", de.getName());
					if (ie.checkIfExists(txtDepartment.getText())) {
						JOptionPane.showMessageDialog(null, "Taki kierunek już istnieje");
					} else {
						de.setName(txtDepartment.getText());
						dispose();
					}
					dispose();
				} catch (IllegalArgumentException iae) {
					dispose();
				}
			}
		});
	}

}
