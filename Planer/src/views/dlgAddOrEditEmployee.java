package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.Main;
import data.DepartmentEntry;
import data.EmployeeEntry;

import javax.swing.BoxLayout;
import java.awt.GridLayout;
import java.awt.Font;
import java.awt.GridBagLayout;
import javax.swing.JSplitPane;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JCheckBox;

public class dlgAddOrEditEmployee extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField txtNameInput;
	private JTextField txtSurnameInput;
	private JTextField txtDegreeInput;
	private JTextField txtPositionInput;
	private JTextField txtJobTimeInput;
	DepartmentEntry de;
	private static final Logger logger = LogManager.getLogger(dlgAddOrEditEmployee.class.getName());
	private JCheckBox chkCyvilContract;
	private JButton btnOk;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		try {
			dlgAddOrEditEmployee dialog = new dlgAddOrEditEmployee();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * Create the dialog.
	 */
	public dlgAddOrEditEmployee() {
		setTitle("NOWY PRACOWNIK");
		setBounds(100, 100, 450, 295);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWidths = new int[]{0, 0};
		gbl_contentPanel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gbl_contentPanel.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPanel.rowWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		contentPanel.setLayout(gbl_contentPanel);
		{
			JSplitPane splitPane = new JSplitPane();
			GridBagConstraints gbc_splitPane = new GridBagConstraints();
			gbc_splitPane.insets = new Insets(0, 0, 5, 0);
			gbc_splitPane.fill = GridBagConstraints.BOTH;
			gbc_splitPane.gridx = 0;
			gbc_splitPane.gridy = 0;
			contentPanel.add(splitPane, gbc_splitPane);
			{
				JLabel lblName = new JLabel("IMIĘ");
				lblName.setFont(new Font("Tahoma", Font.BOLD, 14));
				splitPane.setLeftComponent(lblName);
			}
			{
				txtNameInput = new JTextField();
				txtNameInput.setFont(new Font("Tahoma", Font.BOLD, 14));
				splitPane.setRightComponent(txtNameInput);
				txtNameInput.setColumns(10);
			}
		}
		{
			JSplitPane splitPane = new JSplitPane();
			GridBagConstraints gbc_splitPane = new GridBagConstraints();
			gbc_splitPane.insets = new Insets(0, 0, 5, 0);
			gbc_splitPane.fill = GridBagConstraints.BOTH;
			gbc_splitPane.gridx = 0;
			gbc_splitPane.gridy = 1;
			contentPanel.add(splitPane, gbc_splitPane);
			{
				JLabel lblSurname = new JLabel("NAZWISKO");
				lblSurname.setFont(new Font("Tahoma", Font.BOLD, 14));
				splitPane.setLeftComponent(lblSurname);
			}
			{
				txtSurnameInput = new JTextField();
				txtSurnameInput.setFont(new Font("Tahoma", Font.BOLD, 14));
				splitPane.setRightComponent(txtSurnameInput);
				txtSurnameInput.setColumns(10);
			}
		}
		{
			JSplitPane splitPane = new JSplitPane();
			GridBagConstraints gbc_splitPane = new GridBagConstraints();
			gbc_splitPane.insets = new Insets(0, 0, 5, 0);
			gbc_splitPane.fill = GridBagConstraints.BOTH;
			gbc_splitPane.gridx = 0;
			gbc_splitPane.gridy = 2;
			contentPanel.add(splitPane, gbc_splitPane);
			{
				JLabel lblDegree = new JLabel("STOPIEŃ NAUKOWY");
				lblDegree.setFont(new Font("Tahoma", Font.BOLD, 14));
				splitPane.setLeftComponent(lblDegree);
			}
			{
				txtDegreeInput = new JTextField();
				txtDegreeInput.setFont(new Font("Tahoma", Font.BOLD, 14));
				splitPane.setRightComponent(txtDegreeInput);
				txtDegreeInput.setColumns(10);
			}
		}
		{
			JSplitPane splitPane = new JSplitPane();
			GridBagConstraints gbc_splitPane = new GridBagConstraints();
			gbc_splitPane.insets = new Insets(0, 0, 5, 0);
			gbc_splitPane.fill = GridBagConstraints.BOTH;
			gbc_splitPane.gridx = 0;
			gbc_splitPane.gridy = 3;
			contentPanel.add(splitPane, gbc_splitPane);
			{
				JLabel lblPosition = new JLabel("STANOWISKO");
				lblPosition.setFont(new Font("Tahoma", Font.BOLD, 14));
				splitPane.setLeftComponent(lblPosition);
			}
			{
				txtPositionInput = new JTextField();
				txtPositionInput.setFont(new Font("Tahoma", Font.BOLD, 14));
				splitPane.setRightComponent(txtPositionInput);
				txtPositionInput.setColumns(10);
			}
		}
		{
			JSplitPane splitPane = new JSplitPane();
			GridBagConstraints gbc_splitPane = new GridBagConstraints();
			gbc_splitPane.insets = new Insets(0, 0, 5, 0);
			gbc_splitPane.fill = GridBagConstraints.BOTH;
			gbc_splitPane.gridx = 0;
			gbc_splitPane.gridy = 4;
			contentPanel.add(splitPane, gbc_splitPane);
			{
				JLabel lblJobTime = new JLabel("WYMIAR ETATU");
				lblJobTime.setFont(new Font("Tahoma", Font.BOLD, 14));
				splitPane.setLeftComponent(lblJobTime);
			}
			{
				txtJobTimeInput = new JTextField();
				txtJobTimeInput.setFont(new Font("Tahoma", Font.BOLD, 14));
				splitPane.setRightComponent(txtJobTimeInput);
				txtJobTimeInput.setColumns(10);
			}
		}
		{
			JSplitPane splitPane = new JSplitPane();
			GridBagConstraints gbc_splitPane = new GridBagConstraints();
			gbc_splitPane.fill = GridBagConstraints.BOTH;
			gbc_splitPane.gridx = 0;
			gbc_splitPane.gridy = 5;
			contentPanel.add(splitPane, gbc_splitPane);
			{
				JLabel lblCyvilContract = new JLabel("MINIMUM KADROWE");
				lblCyvilContract.setFont(new Font("Tahoma", Font.BOLD, 14));
				splitPane.setLeftComponent(lblCyvilContract);
			}
			{
				chkCyvilContract = new JCheckBox("TAK");
				chkCyvilContract.setFont(new Font("Tahoma", Font.BOLD, 14));
				splitPane.setRightComponent(chkCyvilContract);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			buttonPane.setLayout(new GridLayout(0, 2, 0, 0));
			{
				btnOk = new JButton("OK");
				btnOk.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							logger.info("Dodawanie pracownika {} {}", txtNameInput.getText(), txtSurnameInput.getText());
							EmployeeEntry ee = new EmployeeEntry(txtNameInput.getText(), txtSurnameInput.getText());
							ee.setDegree(txtDegreeInput.getText());
							ee.setPosition(txtPositionInput.getText());
							ee.setJobTime(Long.valueOf(txtJobTimeInput.getText()));
							ee.setCyvilContract(chkCyvilContract.isSelected());
							de.addEmployee(ee);
							Main.t.generateTree();
							dispose();
						} catch (IllegalArgumentException iae) {
							logger.info("BLAD: {}", iae.getMessage());
							JOptionPane.showMessageDialog(null, iae.getMessage());
							//dispose();
						}
					}
				});
				btnOk.setFont(new Font("Tahoma", Font.BOLD, 11));
				btnOk.setActionCommand("OK");
				buttonPane.add(btnOk);
				getRootPane().setDefaultButton(btnOk);
			}
			{
				JButton btnCancel = new JButton("ANULUJ");
				btnCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				btnCancel.setFont(new Font("Tahoma", Font.BOLD, 11));
				btnCancel.setActionCommand("Cancel");
				buttonPane.add(btnCancel);
			}
		}
	}

	public dlgAddOrEditEmployee(DepartmentEntry de) {
		this();
		this.de = de;
	}

	public dlgAddOrEditEmployee(EmployeeEntry ee) {
		this();
		
		setTitle("EDYTOWANIE DANYCH PRACOWNIKA");
		btnOk.setText("EDYTUJ");
		misc.Utils.removeListeners(btnOk);
		
		txtNameInput.setText(ee.getName());
		txtSurnameInput.setText(ee.getSurname());
		txtDegreeInput.setText(ee.getDegree());
		txtPositionInput.setText(ee.getPosition());
		txtJobTimeInput.setText(String.valueOf(ee.getJobTime()));
		chkCyvilContract.setSelected(ee.isCyvilContract());
		
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					logger.info("Edycja danych pracownika ({})", ee.toString());
					ee.setName(txtNameInput.getText());
					ee.setSurname(txtSurnameInput.getText());
					ee.setDegree(txtDegreeInput.getText());
					ee.setPosition(txtPositionInput.getText());
					ee.setJobTime(Long.valueOf(txtJobTimeInput.getText()));
					ee.setCyvilContract(chkCyvilContract.isSelected());
					dispose();
				} catch (IllegalArgumentException iae) {
					dispose();
				}
			}
		});
	}

}
