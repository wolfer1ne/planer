package views;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import javax.swing.JTabbedPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import data.SpecializationEntry;
import data.primitives.SemesterBasicInfo;
import data.primitives.SemesterType;
import data.primitives.StudyYear;
import data.primitives.SubjectType;

import java.awt.Font;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class pnlSemesterInfo extends JPanel {
	private static final Logger logger = LogManager.getLogger();
	private JTabbedPane tbpSpecs;
	
	List<SpecializationEntry> specializations;
	private pnlYearBasicInfo pnlBasicInfo;
	private JSplitPane splitPane;

	/**
	 * Create the panel.
	 */
	private pnlSemesterInfo() {
		setLayout(new BorderLayout(0, 0));
	}
	
	public void
	saveData()
	{
		pnlBasicInfo.saveData();
		
		if (null != tbpSpecs) {
			int cnt = tbpSpecs.getTabCount();
			for (int i = 0; i < cnt; i++) {
				pnlSpecializationBasicInfo pnl = (pnlSpecializationBasicInfo) tbpSpecs.getComponentAt(i);
				pnl.saveData();
			}
		}
	}
	
	public
	pnlSemesterInfo(SemesterBasicInfo basicInfo,
			StudyYear sy,
			SubjectType st,
			List<SpecializationEntry> specializations)
	{
		this();

		pnlBasicInfo = new pnlYearBasicInfo(basicInfo);
		
		// Brak specjalizacji
		if (null == specializations || specializations.size() == 1 || sy == StudyYear.I) {	
			add(pnlBasicInfo);
			return;
		} else {	
			splitPane = new JSplitPane();
			splitPane.setLeftComponent(pnlBasicInfo);	
			splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
			splitPane.setResizeWeight(0.6);
			add(splitPane);
			
			tbpSpecs = new JTabbedPane(JTabbedPane.TOP);
			tbpSpecs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
			tbpSpecs.setFont(new Font("Tahoma", Font.BOLD, 13));
			splitPane.setRightComponent(tbpSpecs);
			
			for (SpecializationEntry se : specializations) {
				if (se.getName().equals(misc.Utils.EMPTY_SPEC)) continue;
				tbpSpecs.add(se.getName(), new pnlSpecializationBasicInfo(se.getSemesterInfo(sy, st)));
			}
		}
	}
}
