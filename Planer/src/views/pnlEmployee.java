package views;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Component;
import java.awt.Font;
import java.awt.BorderLayout;
import java.awt.Insets;
import javax.swing.JTextField;
import java.awt.Dimension;
import javax.swing.JButton;

public class pnlEmployee extends JPanel {

	/**
	 * Create the panel.
	 */
	public pnlEmployee() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 93, 52, 101, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblDanePracownika = new JLabel("DANE PRACOWNIKA");
		lblDanePracownika.setFont(new Font("Tahoma", Font.BOLD, 20));
		GridBagConstraints gbc_lblDanePracownika = new GridBagConstraints();
		gbc_lblDanePracownika.gridwidth = 3;
		gbc_lblDanePracownika.insets = new Insets(0, 0, 5, 0);
		gbc_lblDanePracownika.gridx = 1;
		gbc_lblDanePracownika.gridy = 1;
		add(lblDanePracownika, gbc_lblDanePracownika);
		
		JLabel lblName = new JLabel("IMIĘ");
		lblName.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.fill = GridBagConstraints.VERTICAL;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 1;
		gbc_lblName.gridy = 3;
		add(lblName, gbc_lblName);
		
		JLabel lblNameInput = new JLabel("");
		lblNameInput.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblNameInput = new GridBagConstraints();
		gbc_lblNameInput.anchor = GridBagConstraints.WEST;
		gbc_lblNameInput.insets = new Insets(0, 0, 5, 5);
		gbc_lblNameInput.gridx = 3;
		gbc_lblNameInput.gridy = 3;
		add(lblNameInput, gbc_lblNameInput);
		
		JLabel lblSurname = new JLabel("NAZWISKO");
		lblSurname.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblSurname = new GridBagConstraints();
		gbc_lblSurname.anchor = GridBagConstraints.EAST;
		gbc_lblSurname.insets = new Insets(0, 0, 5, 5);
		gbc_lblSurname.gridx = 1;
		gbc_lblSurname.gridy = 4;
		add(lblSurname, gbc_lblSurname);
		
		JLabel lblSurnameInput = new JLabel("");
		lblSurnameInput.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblSurnameInput = new GridBagConstraints();
		gbc_lblSurnameInput.anchor = GridBagConstraints.WEST;
		gbc_lblSurnameInput.insets = new Insets(0, 0, 5, 5);
		gbc_lblSurnameInput.gridx = 3;
		gbc_lblSurnameInput.gridy = 4;
		add(lblSurnameInput, gbc_lblSurnameInput);
		
		JLabel lblDegree = new JLabel("STOPIEŃ NAUKOWY");
		lblDegree.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblDegree = new GridBagConstraints();
		gbc_lblDegree.anchor = GridBagConstraints.EAST;
		gbc_lblDegree.insets = new Insets(0, 0, 5, 5);
		gbc_lblDegree.gridx = 1;
		gbc_lblDegree.gridy = 5;
		add(lblDegree, gbc_lblDegree);
		
		JLabel lblDegreeInput = new JLabel("");
		lblDegreeInput.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblDegreeInput = new GridBagConstraints();
		gbc_lblDegreeInput.anchor = GridBagConstraints.WEST;
		gbc_lblDegreeInput.insets = new Insets(0, 0, 5, 5);
		gbc_lblDegreeInput.gridx = 3;
		gbc_lblDegreeInput.gridy = 5;
		add(lblDegreeInput, gbc_lblDegreeInput);
		
		JLabel lblDepartment = new JLabel("ZAKŁAD");
		lblDepartment.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblDepartment = new GridBagConstraints();
		gbc_lblDepartment.anchor = GridBagConstraints.EAST;
		gbc_lblDepartment.insets = new Insets(0, 0, 5, 5);
		gbc_lblDepartment.gridx = 1;
		gbc_lblDepartment.gridy = 6;
		add(lblDepartment, gbc_lblDepartment);
		
		JLabel lblDepartmentInput = new JLabel("");
		lblDepartmentInput.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblDepartmentInput = new GridBagConstraints();
		gbc_lblDepartmentInput.anchor = GridBagConstraints.WEST;
		gbc_lblDepartmentInput.insets = new Insets(0, 0, 5, 5);
		gbc_lblDepartmentInput.gridx = 3;
		gbc_lblDepartmentInput.gridy = 6;
		add(lblDepartmentInput, gbc_lblDepartmentInput);
		
		JLabel lblPosition = new JLabel("STANOWISKO");
		lblPosition.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblPosition = new GridBagConstraints();
		gbc_lblPosition.anchor = GridBagConstraints.EAST;
		gbc_lblPosition.insets = new Insets(0, 0, 5, 5);
		gbc_lblPosition.gridx = 1;
		gbc_lblPosition.gridy = 7;
		add(lblPosition, gbc_lblPosition);
		
		JLabel lblPositionInput = new JLabel("");
		lblPositionInput.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblPositionInput = new GridBagConstraints();
		gbc_lblPositionInput.anchor = GridBagConstraints.WEST;
		gbc_lblPositionInput.insets = new Insets(0, 0, 5, 5);
		gbc_lblPositionInput.gridx = 3;
		gbc_lblPositionInput.gridy = 7;
		add(lblPositionInput, gbc_lblPositionInput);
		
		JLabel lblStaffResource = new JLabel("MINIMUM KADROWE");
		lblStaffResource.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblStaffResource = new GridBagConstraints();
		gbc_lblStaffResource.anchor = GridBagConstraints.EAST;
		gbc_lblStaffResource.insets = new Insets(0, 0, 5, 5);
		gbc_lblStaffResource.gridx = 1;
		gbc_lblStaffResource.gridy = 8;
		add(lblStaffResource, gbc_lblStaffResource);
		
		JLabel lblStaffResourceInput = new JLabel("");
		lblStaffResourceInput.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblStaffResourceInput = new GridBagConstraints();
		gbc_lblStaffResourceInput.anchor = GridBagConstraints.WEST;
		gbc_lblStaffResourceInput.insets = new Insets(0, 0, 5, 5);
		gbc_lblStaffResourceInput.gridx = 3;
		gbc_lblStaffResourceInput.gridy = 8;
		add(lblStaffResourceInput, gbc_lblStaffResourceInput);
		
		JLabel lblJobTime = new JLabel("WYMIAR ETATU");
		lblJobTime.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblJobTime = new GridBagConstraints();
		gbc_lblJobTime.anchor = GridBagConstraints.EAST;
		gbc_lblJobTime.insets = new Insets(0, 0, 5, 5);
		gbc_lblJobTime.gridx = 1;
		gbc_lblJobTime.gridy = 9;
		add(lblJobTime, gbc_lblJobTime);
		
		JLabel lblJobTimeInput = new JLabel("");
		lblJobTimeInput.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblJobTimeInput = new GridBagConstraints();
		gbc_lblJobTimeInput.anchor = GridBagConstraints.WEST;
		gbc_lblJobTimeInput.insets = new Insets(0, 0, 5, 5);
		gbc_lblJobTimeInput.gridx = 3;
		gbc_lblJobTimeInput.gridy = 9;
		add(lblJobTimeInput, gbc_lblJobTimeInput);
		
		JButton btnEdit = new JButton("EDYCJA");
		btnEdit.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_btnEdit = new GridBagConstraints();
		gbc_btnEdit.insets = new Insets(0, 0, 0, 5);
		gbc_btnEdit.gridx = 2;
		gbc_btnEdit.gridy = 11;
		add(btnEdit, gbc_btnEdit);

	}

}
