package views;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import java.awt.BorderLayout;
import javax.swing.JTabbedPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import data.SpecializationEntry;
import data.primitives.InstituteBasicInfo;
import data.primitives.StudyYear;

import java.awt.Font;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;

public class pnlYearInfo extends JPanel implements ChangeListener {
	private static final Logger logger = LogManager.getLogger();
	private JTabbedPane tbpYears;
	private JTabbedPane tbpSpecs;
	
	List<SpecializationEntry> specializations;

	/**
	 * Create the panel.
	 */
	private pnlYearInfo() {
		setLayout(new BorderLayout(0, 0));
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setResizeWeight(0.5);
		add(splitPane);
		
		tbpYears = new JTabbedPane(JTabbedPane.TOP);
		tbpYears.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		tbpYears.setFont(new Font("Tahoma", Font.BOLD, 13));
		splitPane.setLeftComponent(tbpYears);
		
		tbpSpecs = new JTabbedPane(JTabbedPane.TOP);
		tbpSpecs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
		tbpSpecs.setFont(new Font("Tahoma", Font.BOLD, 13));
		splitPane.setRightComponent(tbpSpecs);

	}
	
	public
	pnlYearInfo(Map<StudyYear, InstituteBasicInfo> basicInfo, List<SpecializationEntry> specializations)
	{
		this();
		this.specializations = specializations;
			
		for (Entry<StudyYear, InstituteBasicInfo> pair : basicInfo.entrySet()) {
			tbpYears.add(pair.getKey().toString(), new pnlYearBasicInfo(pair.getValue()));
		}
		
		redrawSpecs(StudyYear.I);
		
		tbpYears.addChangeListener(this);		
		tbpYears.setSelectedIndex(0);
	}
	
	void
	redrawSpecs(StudyYear se)
	{
		hideSpecs();
		logger.debug("Dodawanie panelu specjalizacji");
		for (SpecializationEntry ee : specializations) {
			if (ee.getName().equals(misc.Utils.EMPTY_SPEC)) continue;
			
			/*for (Entry<StudyYear, InstituteBasicInfo> pair : ee.getBasicInfo().entrySet()) {
				tbpSpecs.add(ee.getName(), new pnlSpecializationBasicInfo(pair.getValue()));
			}*/
			tbpSpecs.add(ee.getName(), new pnlSpecializationBasicInfo(ee.getYearBasicInfo(se)));		
		}
	}
	
	public
	void saveAll()
	{
		int tabs = tbpYears.getTabCount();
		for (int i = 0; i < tabs; i++) {
			pnlYearBasicInfo comp = (pnlYearBasicInfo) tbpYears.getComponentAt(i);
			comp.saveData();
		}
		
		tabs = tbpSpecs.getTabCount();
		for (int i = 0; i < tabs; i++) {
			pnlSpecializationBasicInfo comp = (pnlSpecializationBasicInfo) tbpSpecs.getComponentAt(i);
			comp.saveData();
		}
	}
	
	private
	void hideSpecs()
	{
		tbpSpecs.removeAll();
		/*while (tbpSpecs.getTabCount() > 0)
			tbpSpecs.remove(0);*/
	}
	
	private
	void yearOne()
	{
		pnlYearBasicInfo comp = (pnlYearBasicInfo) tbpYears.getComponentAt(0);
		comp.yearOneSettings();
	}
	
	private
	void yearTwo()
	{
		pnlYearBasicInfo comp = (pnlYearBasicInfo) tbpYears.getComponentAt(1);
		comp.yearTwoSettings();
	}
	
	private
	void yearThree()
	{
		pnlYearBasicInfo comp = (pnlYearBasicInfo) tbpYears.getComponentAt(2);
		comp.yearThreeAndFour();
	}
	
	private
	void yearFour()
	{
		pnlYearBasicInfo comp = (pnlYearBasicInfo) tbpYears.getComponentAt(3);
		comp.yearThreeAndFour();
	}

	@Override
	public void stateChanged(ChangeEvent e) {
		int idxTab = tbpYears.getSelectedIndex();
		if (idxTab == -1) return;
		
		switch (idxTab) {
		case 0: {
			hideSpecs();
			yearOne();
			break;
			}
		case 1: { 
			redrawSpecs(StudyYear.II);
			yearTwo();
			break;
			}
		case 2: {
			redrawSpecs(StudyYear.III);
			yearThree();
			break;
			}
		case 3: {
			redrawSpecs(StudyYear.IV);
			yearFour();
			break;
			}
		}
		
		logger.debug("idx: {}; specki: {}", idxTab, tbpSpecs.getTabCount()); 
	}
}
