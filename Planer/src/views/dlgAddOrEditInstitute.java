package views;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import app.Main;
import data.InstituteEntry;
import data.YearEntry;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.BoxLayout;
import javax.swing.JTextField;
import javax.swing.UIManager;
import java.awt.GridLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class dlgAddOrEditInstitute extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField txtField;
	private YearEntry ye;
	InstituteEntry ie;
	
	private static final Logger logger = LogManager.getLogger(dlgAddOrEditInstitute.class.getName());
	private JButton btnOk;
	private JLabel lblTitle;

	/**
	 * Launch the application.
	 */
	/*public static void main(String[] args) {
		try {
			dlgAddOrEditInstitute dialog = new dlgAddOrEditInstitute();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/

	/**
	 * Create the dialog.
	 */
	public dlgAddOrEditInstitute() {
		setSize(new Dimension(200, 100));
		setTitle("NOWY INSTYTUT");
		setBounds(100, 100, 220, 104);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			lblTitle = new JLabel("Nazwa nowego instytutu");
			lblTitle.setFont(new Font("Tahoma", Font.BOLD, 15));
			lblTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
			contentPanel.add(lblTitle, BorderLayout.NORTH);
		}
		{
			txtField = new JTextField();
			txtField.setFont(new Font("Tahoma", Font.BOLD, 11));
			contentPanel.add(txtField, BorderLayout.SOUTH);
			txtField.setColumns(20);
		}
		{
			JPanel buttonPane = new JPanel();
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			buttonPane.setLayout(new GridLayout(0, 2, 0, 0));
			{
				btnOk = new JButton("Dodaj");
				btnOk.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						try {
							logger.info("Dodawanie nowego instytutu ({}); ROK = {}",txtField.getText(), ye.getName());
							ye.addInstitute(txtField.getText());
							Main.t.generateTree();
							dispose();
						} catch (IllegalArgumentException iae) {
							logger.info("BLAD: {}",txtField.getText(), ye.getName(), iae.getMessage());
							JOptionPane.showMessageDialog(null, iae.getMessage());
							dispose();
						}
					}
				});
				btnOk.setFont(new Font("Tahoma", Font.BOLD, 11));
				btnOk.setActionCommand("OK");
				buttonPane.add(btnOk);
				getRootPane().setDefaultButton(btnOk);
			}
			{
				JButton btnCancel = new JButton("Anuluj");
				btnCancel.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						dispose();
					}
				});
				btnCancel.setFont(new Font("Tahoma", Font.BOLD, 11));
				btnCancel.setActionCommand("Cancel");
				buttonPane.add(btnCancel);
			}
		}
	}

	public dlgAddOrEditInstitute(YearEntry ye) {
		this();
		this.ye = ye;
	}
	
	public dlgAddOrEditInstitute(YearEntry ye, InstituteEntry ie) {
		this();
		this.ye = ye;
		this.ie = ie;
		
		setTitle("EDYCJA INSTYTUTU");
		lblTitle.setText("Zmiana: " + ie.getName());
		txtField.setText(ie.getName());
		btnOk.setText("EDYTUJ");
		misc.Utils.removeListeners(btnOk);
		
		btnOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					logger.info("Edycja instytutu ({})", ie.getName());
					if (ye.checkIfExists(txtField.getText())) {
						JOptionPane.showMessageDialog(null, "Taki instytut już istnieje");
					} else {
						ie.setName(txtField.getText());
						dispose();
					}
					dispose();
				} catch (IllegalArgumentException iae) {
					dispose();
				}
			}
		});
	}

}
