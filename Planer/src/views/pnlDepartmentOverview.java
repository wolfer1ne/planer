package views;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Font;
import javax.swing.BoxLayout;
import javax.swing.JScrollPane;

import data.DepartmentEntry;
import data.EmployeeEntry;
import data.SpecializationEntry;
import data.primitives.DataEntry;
import data.primitives.SemesterBasicInfo;
import data.primitives.StudyYear;
import data.primitives.SubjectType;
import generators.CollectiveRaport;
import generators.EmploymentPlant;
import generators.FullTimeRaport;
import generators.PartTimeRaport;
import models.EditButton;
import models.RemoveButton;

import javax.swing.JButton;
import javax.swing.JFileChooser;

import java.awt.event.ActionListener;
import java.util.Map.Entry;
import java.awt.event.ActionEvent;
import javax.swing.JTabbedPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.itextpdf.xmp.impl.Utils;

import adders.CollectiveRaportAdder;
import app.DataContainer;
import app.Main;

import javax.swing.JSplitPane;
import java.awt.GridLayout;

public class pnlDepartmentOverview extends JPanel implements ActionListener {

	private static final Logger logger = LogManager.getLogger();
	
	private JLabel lblTitle;
	private JLabel lblEmployees;
	private JPanel pnlEmployees;
	private JButton btnAddEmployee;
	private JButton btnDodajSpecjalizacje;
	DepartmentEntry de;
	private JLabel lblSpecjalizacje;
	private JPanel pnlSpecializations;
	private JScrollPane scrollPane_1;
	private JLabel lblInformacje;
	private JButton btnSave;
	pnlSemesterInfo p;
	private JTabbedPane tbpYearsInfo;
	private JButton btnCollectiveRaport;
	private JButton btnFullTimeSubsRaport;
	private JButton btnPartTimeSubsReport;
	
	void
	saveSemestersInfo()
	{
		int cnt = tbpYearsInfo.getTabCount();
		for (int i = 0; i < cnt; i++) {
			JTabbedPane jtp = (JTabbedPane) tbpYearsInfo.getComponentAt(i);
			int cnt2 = jtp.getTabCount();
			for (int j = 0; j < cnt2; j++) {
				pnlSemesterInfo sem = (pnlSemesterInfo) jtp.getComponentAt(j);
				sem.saveData();
			}
		}
	}
	
	void
	recreateSemesterInfo()
	{
		saveSemestersInfo();
		logger.debug("Odświerzanie paneli z semestrami");
		tbpYearsInfo.removeAll();
		for (StudyYear sy : StudyYear.values()) {
			JTabbedPane jtp = new JTabbedPane();
			for (SubjectType st : SubjectType.values()) {
				SemesterBasicInfo sbi = de.getSemesterInfo(sy, st);
				jtp.add(st.toString(), new pnlSemesterInfo(sbi, sy, st, de.getSpecializations()));
			}
			tbpYearsInfo.add(sy.toString(), jtp);
		}
	}

	/**
	 * Create the panel.
	 */
	public pnlDepartmentOverview() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{230, 0, 82, 77, 0, 139, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 135, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		lblTitle = new JLabel("[DEPARTMENT PLACEHOLDER]");
		lblTitle.setFont(new Font("Tahoma", Font.BOLD, 20));
		GridBagConstraints gbc_lblTitle = new GridBagConstraints();
		gbc_lblTitle.gridwidth = 7;
		gbc_lblTitle.insets = new Insets(0, 0, 5, 0);
		gbc_lblTitle.gridx = 0;
		gbc_lblTitle.gridy = 0;
		add(lblTitle, gbc_lblTitle);
		
		btnAddEmployee = new JButton("DODAJ PRACOWNIKA");
		btnAddEmployee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new dlgAddOrEditEmployee(de).setVisible(true);
			}
		});
		btnAddEmployee.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_btnAddEmployee = new GridBagConstraints();
		gbc_btnAddEmployee.insets = new Insets(0, 0, 5, 5);
		gbc_btnAddEmployee.gridx = 0;
		gbc_btnAddEmployee.gridy = 1;
		add(btnAddEmployee, gbc_btnAddEmployee);
		
		btnPartTimeSubsReport = new JButton("NIESTACJONARNE");
		btnPartTimeSubsReport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (de.getEmployees().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Brak pracowników");
					return;
				}
				
				try {
					JFileChooser jfc = misc.Utils.getPdfSaveChooser();
					int retrival = jfc.showSaveDialog(null);
					if (retrival == JFileChooser.APPROVE_OPTION) {
						logger.debug("Przedmioty nietacjonarne, plik: {}", jfc.getSelectedFile().toString());
						PartTimeRaport ptr = new PartTimeRaport(de, Main.t.getSelectedYear().toString());
						ptr.Generate(jfc.getSelectedFile().toString());
						JOptionPane.showMessageDialog(null,
								"Wygenerowano do pliku: " + jfc.getSelectedFile().toString());
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null,
							"Błąd w trakcie tworzenia tabeli " +e.getMessage());
				}
			}
		});
		
		btnFullTimeSubsRaport = new JButton("STACJONARNE");
		btnFullTimeSubsRaport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (de.getEmployees().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Brak pracowników");
					return;
				}
				
				try {
					JFileChooser jfc = misc.Utils.getPdfSaveChooser();
					int retrival = jfc.showSaveDialog(null);
					if (retrival == JFileChooser.APPROVE_OPTION) {
						logger.debug("Przedmioty stacjonarne, plik: {}", jfc.getSelectedFile().toString());
						FullTimeRaport ftr = new FullTimeRaport(de, Main.t.getSelectedYear().toString());
						ftr.Generate(jfc.getSelectedFile().toString());
						JOptionPane.showMessageDialog(null,
								"Wygenerowano do pliku: " + jfc.getSelectedFile().toString());
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null,
							"Błąd w trakcie tworzenia tabeli " +e.getMessage());
				}
			}
		});
		
		btnCollectiveRaport = new JButton("TABELA ZBIORCZA");
		btnCollectiveRaport.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (de.getEmployees().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Brak pracowników");
					return;
				}
				
				try {
					JFileChooser jfc = misc.Utils.getPdfSaveChooser();
					int retrival = jfc.showSaveDialog(null);
					if (retrival == JFileChooser.APPROVE_OPTION) {
						logger.debug("Tabela zbiorcza, plik pliku: {}", jfc.getSelectedFile().toString());
						CollectiveRaport cr = new CollectiveRaport(null, de.getEmployees());
						cr.Generate(jfc.getSelectedFile().toString());
						JOptionPane.showMessageDialog(null,
								"Wygenerowano do pliku: " + jfc.getSelectedFile().toString());
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null,
							"Błąd w trakcie tworzenia tabeli " +e.getMessage());
				}
			}
		});
		
		JButton btnEmploymentPlan = new JButton("PLAN ZATRUDNIENIA");
		btnEmploymentPlan.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (de.getEmployees().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Brak pracowników");
					return;
				}
				
				try {
					JFileChooser jfc = misc.Utils.getPdfSaveChooser();
					int retrival = jfc.showSaveDialog(null);
					if (retrival == JFileChooser.APPROVE_OPTION) {
						logger.debug("Plan zatrudnienia, plik: {}", jfc.getSelectedFile().toString());
						EmploymentPlant er = new EmploymentPlant(de, Main.t.getSelectedYear().toString());
						er.Generate(jfc.getSelectedFile().toString());
						JOptionPane.showMessageDialog(null,
								"Wygenerowano do pliku: " + jfc.getSelectedFile().toString());
					}
				} catch (Exception e) {
					JOptionPane.showMessageDialog(null,
							"Błąd w trakcie tworzenia tabeli " +e.getMessage());
				}
			}
		});
		btnEmploymentPlan.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_btnEmploymentPlan = new GridBagConstraints();
		gbc_btnEmploymentPlan.insets = new Insets(0, 0, 5, 5);
		gbc_btnEmploymentPlan.gridx = 2;
		gbc_btnEmploymentPlan.gridy = 1;
		add(btnEmploymentPlan, gbc_btnEmploymentPlan);
		btnCollectiveRaport.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_btnCollectiveRaport = new GridBagConstraints();
		gbc_btnCollectiveRaport.insets = new Insets(0, 0, 5, 5);
		gbc_btnCollectiveRaport.gridx = 3;
		gbc_btnCollectiveRaport.gridy = 1;
		add(btnCollectiveRaport, gbc_btnCollectiveRaport);
		btnFullTimeSubsRaport.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_btnFullTimeSubsRaport = new GridBagConstraints();
		gbc_btnFullTimeSubsRaport.insets = new Insets(0, 0, 5, 5);
		gbc_btnFullTimeSubsRaport.gridx = 4;
		gbc_btnFullTimeSubsRaport.gridy = 1;
		add(btnFullTimeSubsRaport, gbc_btnFullTimeSubsRaport);
		btnPartTimeSubsReport.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_btnPartTimeSubsReport = new GridBagConstraints();
		gbc_btnPartTimeSubsReport.insets = new Insets(0, 0, 5, 5);
		gbc_btnPartTimeSubsReport.gridx = 5;
		gbc_btnPartTimeSubsReport.gridy = 1;
		add(btnPartTimeSubsReport, gbc_btnPartTimeSubsReport);
		
		lblEmployees = new JLabel("PRACOWNICY");
		lblEmployees.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_lblEmployees = new GridBagConstraints();
		gbc_lblEmployees.insets = new Insets(0, 0, 5, 5);
		gbc_lblEmployees.gridx = 0;
		gbc_lblEmployees.gridy = 2;
		add(lblEmployees, gbc_lblEmployees);
		
		lblInformacje = new JLabel("INFORMACJE");
		lblInformacje.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_lblInformacje = new GridBagConstraints();
		gbc_lblInformacje.gridwidth = 2;
		gbc_lblInformacje.insets = new Insets(0, 0, 5, 5);
		gbc_lblInformacje.gridx = 2;
		gbc_lblInformacje.gridy = 2;
		add(lblInformacje, gbc_lblInformacje);
		
		btnSave = new JButton("ZAPISZ");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				logger.info("Zapis danych o ilości studentów/grup itp.");
				//p.saveAll();
				saveSemestersInfo();
			}
		});
		btnSave.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_btnSave = new GridBagConstraints();
		gbc_btnSave.gridwidth = 2;
		gbc_btnSave.insets = new Insets(0, 0, 5, 5);
		gbc_btnSave.gridx = 4;
		gbc_btnSave.gridy = 2;
		add(btnSave, gbc_btnSave);
		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridwidth = 2;
		gbc_scrollPane.gridheight = 3;
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.insets = new Insets(0, 0, 0, 5);
		gbc_scrollPane.gridx = 0;
		gbc_scrollPane.gridy = 3;
		add(scrollPane, gbc_scrollPane);
		
		pnlEmployees = new JPanel();
		scrollPane.setViewportView(pnlEmployees);
		pnlEmployees.setLayout(new GridLayout(0, 2, 0, 0));
		
		tbpYearsInfo = new JTabbedPane(JTabbedPane.TOP);
		GridBagConstraints gbc_tbpYearsInfo = new GridBagConstraints();
		gbc_tbpYearsInfo.gridwidth = 5;
		gbc_tbpYearsInfo.insets = new Insets(0, 0, 5, 0);
		gbc_tbpYearsInfo.fill = GridBagConstraints.BOTH;
		gbc_tbpYearsInfo.gridx = 2;
		gbc_tbpYearsInfo.gridy = 3;
		add(tbpYearsInfo, gbc_tbpYearsInfo);
		
		lblSpecjalizacje = new JLabel("SPECJALIZACJE");
		lblSpecjalizacje.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_lblSpecjalizacje = new GridBagConstraints();
		gbc_lblSpecjalizacje.gridwidth = 2;
		gbc_lblSpecjalizacje.insets = new Insets(0, 0, 5, 5);
		gbc_lblSpecjalizacje.gridx = 2;
		gbc_lblSpecjalizacje.gridy = 4;
		add(lblSpecjalizacje, gbc_lblSpecjalizacje);
		
		btnDodajSpecjalizacje = new JButton("DODAJ SPECJALIZACJE");
		btnDodajSpecjalizacje.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					new dlgAddOrEditSpecialization(de).setVisible(true);
				} catch (IllegalArgumentException iae) {
					return;
				}
				recreateSemesterInfo();
			}
		});
		btnDodajSpecjalizacje.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_btnDodajSpecjalizacje = new GridBagConstraints();
		gbc_btnDodajSpecjalizacje.gridwidth = 2;
		gbc_btnDodajSpecjalizacje.insets = new Insets(0, 0, 5, 5);
		gbc_btnDodajSpecjalizacje.gridx = 4;
		gbc_btnDodajSpecjalizacje.gridy = 4;
		add(btnDodajSpecjalizacje, gbc_btnDodajSpecjalizacje);
		
		scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.gridwidth = 5;
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridx = 2;
		gbc_scrollPane_1.gridy = 5;
		add(scrollPane_1, gbc_scrollPane_1);
		
		pnlSpecializations = new JPanel();
		scrollPane_1.setViewportView(pnlSpecializations);
		pnlSpecializations.setLayout(new GridLayout(0, 2, 0, 0));

	}

	public pnlDepartmentOverview(DepartmentEntry nodeInfo) {
		this();
		de = nodeInfo;
		
		lblTitle.setText(nodeInfo.getName());
		
		for (EmployeeEntry ee : de.getEmployees()) {
			/*JLabel employee = new JLabel(ee.toString());
			employee.setFont(new Font("Tahoma", Font.BOLD, 15));
			pnlEmployees.add(employee);*/
			
			EditButton eBtn = new EditButton(ee.toString(), ee);
			eBtn.setFont(new Font("Tahoma", Font.BOLD, 20));
			eBtn.addActionListener(this);
			pnlEmployees.add(eBtn);	
			
			RemoveButton rBtn = new RemoveButton(ee);
			rBtn.addActionListener(this);
			pnlEmployees.add(rBtn);
		}
		
		for (SpecializationEntry se : de.getSpecializations()) {
			if (se.getName().equals(misc.Utils.EMPTY_SPEC)) continue;
			
			/*JLabel spec = new JLabel(se.toString());
			spec.setFont(new Font("Tahoma", Font.BOLD, 15));
			pnlSpecializations.add(spec);*/
			
			EditButton eBtn = new EditButton(se.toString(), se);
			eBtn.setFont(new Font("Tahoma", Font.BOLD, 20));
			eBtn.addActionListener(this);
			pnlSpecializations.add(eBtn);		
			
			RemoveButton rBtn = new RemoveButton(se);
			rBtn.addActionListener(this);
			pnlSpecializations.add(rBtn);
			
		}
		
		for (StudyYear sy : StudyYear.values()) {
			JTabbedPane jtp = new JTabbedPane();
			for (SubjectType st : SubjectType.values()) {
				SemesterBasicInfo sbi = de.getSemesterInfo(sy, st);
				jtp.add(st.toString(), new pnlSemesterInfo(sbi, sy, st, de.getSpecializations()));
			}
			tbpYearsInfo.add(sy.toString(), jtp);
		}
		
		logger.info("Kierunek {}; pracownicy {}; specjalizacje {}",
				de.getName(),
				de.getEmployees().size(),
				de.getSpecializations().size());
	}

	@Override
	public void actionPerformed(ActionEvent ae) {
		Object e = ae.getSource();
		if (e instanceof RemoveButton) {
			RemoveButton obj = (RemoveButton) e;
			
			if (obj.getObject() instanceof EmployeeEntry) {
				EmployeeEntry ee = (EmployeeEntry) obj.getObject();
				logger.info("Usuwanie pracownika {}", ee.toString());
				
				if (de.removeEmployee(ee)) {
					pnlEmployees.remove(obj);
					Main.t.generateTree();
				}	
			}
			else if (obj.getObject() instanceof SpecializationEntry) {
				SpecializationEntry se = (SpecializationEntry) obj.getObject();
				logger.info("Usuwanie specjalizacji {}", se.toString());
				
				if (DepartmentEntry.checkIfSpecUsed(de.getEmployees(), se)) {
					logger.warn("Specjalizacja {} do usunięcia w użyciu", se.getName());
					JOptionPane.showMessageDialog(null, "Specjalizacja w użyciu, nie można jej usunąć");
					return;
				} else {
					saveSemestersInfo();
					if (de.removeSpec(se)) {
						pnlSpecializations.remove(obj);
						recreateSemesterInfo();
						Main.t.generateTree();
					}
				}
			}
		}
		else if (e instanceof EditButton) {
			EditButton obj = (EditButton) e;
			
			if (obj.getObject() instanceof SpecializationEntry) {
				SpecializationEntry se = (SpecializationEntry) obj.getObject();
				logger.info("Edycja specjalizacji {}", se.toString());
				
				new dlgAddOrEditSpecialization(de, se).setVisible(true);
			}
			else if (obj.getObject() instanceof EmployeeEntry) {
				EmployeeEntry ee = (EmployeeEntry) obj.getObject();
				logger.info("Edycja pracownika {}", ee.toString());
				
				new dlgAddOrEditEmployee(ee).setVisible(true);
			}
		}
	}
}
