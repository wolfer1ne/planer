package views;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Font;
import javax.swing.JButton;

public class pnlSubjectOrSpecializationOverview extends JPanel {

	/**
	 * Create the panel.
	 */
	public pnlSubjectOrSpecializationOverview() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 90, 50, 90, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblInfo = new JLabel("DANE ");
		lblInfo.setFont(new Font("Tahoma", Font.BOLD, 20));
		GridBagConstraints gbc_lblInfo = new GridBagConstraints();
		gbc_lblInfo.gridwidth = 3;
		gbc_lblInfo.insets = new Insets(0, 0, 5, 5);
		gbc_lblInfo.gridx = 1;
		gbc_lblInfo.gridy = 1;
		add(lblInfo, gbc_lblInfo);
		
		JLabel lblInstitute = new JLabel("INSTYTUT");
		lblInstitute.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblInstitute = new GridBagConstraints();
		gbc_lblInstitute.anchor = GridBagConstraints.EAST;
		gbc_lblInstitute.insets = new Insets(0, 0, 5, 5);
		gbc_lblInstitute.gridx = 1;
		gbc_lblInstitute.gridy = 3;
		add(lblInstitute, gbc_lblInstitute);
		
		JLabel lblInstituteInput = new JLabel("");
		lblInstituteInput.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblInstituteInput = new GridBagConstraints();
		gbc_lblInstituteInput.anchor = GridBagConstraints.WEST;
		gbc_lblInstituteInput.insets = new Insets(0, 0, 5, 5);
		gbc_lblInstituteInput.gridx = 3;
		gbc_lblInstituteInput.gridy = 3;
		add(lblInstituteInput, gbc_lblInstituteInput);
		
		JLabel lblDepartment = new JLabel("KIERUNEK");
		lblDepartment.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblDepartment = new GridBagConstraints();
		gbc_lblDepartment.anchor = GridBagConstraints.EAST;
		gbc_lblDepartment.insets = new Insets(0, 0, 5, 5);
		gbc_lblDepartment.gridx = 1;
		gbc_lblDepartment.gridy = 4;
		add(lblDepartment, gbc_lblDepartment);
		
		JLabel lblDepartmentInput = new JLabel("");
		lblDepartmentInput.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblDepartmentInput = new GridBagConstraints();
		gbc_lblDepartmentInput.anchor = GridBagConstraints.WEST;
		gbc_lblDepartmentInput.insets = new Insets(0, 0, 5, 5);
		gbc_lblDepartmentInput.gridx = 3;
		gbc_lblDepartmentInput.gridy = 4;
		add(lblDepartmentInput, gbc_lblDepartmentInput);
		
		JLabel lblName = new JLabel("NAZWA");
		lblName.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblName = new GridBagConstraints();
		gbc_lblName.anchor = GridBagConstraints.EAST;
		gbc_lblName.insets = new Insets(0, 0, 5, 5);
		gbc_lblName.gridx = 1;
		gbc_lblName.gridy = 5;
		add(lblName, gbc_lblName);
		
		JLabel lblNameInput = new JLabel("");
		lblNameInput.setFont(new Font("Tahoma", Font.BOLD, 14));
		GridBagConstraints gbc_lblNameInput = new GridBagConstraints();
		gbc_lblNameInput.anchor = GridBagConstraints.WEST;
		gbc_lblNameInput.insets = new Insets(0, 0, 5, 5);
		gbc_lblNameInput.gridx = 3;
		gbc_lblNameInput.gridy = 5;
		add(lblNameInput, gbc_lblNameInput);
		
		JButton btnEdit = new JButton("EDYCJA");
		btnEdit.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_btnEdit = new GridBagConstraints();
		gbc_btnEdit.insets = new Insets(0, 0, 0, 5);
		gbc_btnEdit.gridx = 2;
		gbc_btnEdit.gridy = 7;
		add(btnEdit, gbc_btnEdit);

	}

}
