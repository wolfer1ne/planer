package views;

import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Font;
import javax.swing.JSplitPane;
import javax.swing.JScrollPane;
import javax.swing.BoxLayout;

public class pnlDepartmentInfo extends JPanel {

	/**
	 * Create the panel.
	 */
	public pnlDepartmentInfo() {
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 50, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 1.0, 0.0, Double.MIN_VALUE};
		setLayout(gridBagLayout);
		
		JLabel lblInfo = new JLabel("[KIERUNEK]");
		lblInfo.setFont(new Font("Tahoma", Font.BOLD, 20));
		GridBagConstraints gbc_lblInfo = new GridBagConstraints();
		gbc_lblInfo.gridwidth = 3;
		gbc_lblInfo.insets = new Insets(0, 0, 5, 5);
		gbc_lblInfo.gridx = 1;
		gbc_lblInfo.gridy = 1;
		add(lblInfo, gbc_lblInfo);
		
		JLabel lblDescription = new JLabel("LISTA PRZEDMIOTÓW I SPECJALIZACJI");
		lblDescription.setFont(new Font("Tahoma", Font.BOLD, 15));
		GridBagConstraints gbc_lblDescription = new GridBagConstraints();
		gbc_lblDescription.gridwidth = 3;
		gbc_lblDescription.insets = new Insets(0, 0, 5, 5);
		gbc_lblDescription.gridx = 1;
		gbc_lblDescription.gridy = 2;
		add(lblDescription, gbc_lblDescription);
		
		JLabel lblSubjects = new JLabel("PRZEDMIOTY");
		lblSubjects.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblSubjects = new GridBagConstraints();
		gbc_lblSubjects.insets = new Insets(0, 0, 5, 5);
		gbc_lblSubjects.gridx = 1;
		gbc_lblSubjects.gridy = 3;
		add(lblSubjects, gbc_lblSubjects);
		
		JLabel lblSpecializations = new JLabel("SPECJALIZACJE");
		lblSpecializations.setFont(new Font("Tahoma", Font.BOLD, 11));
		GridBagConstraints gbc_lblSpecializations = new GridBagConstraints();
		gbc_lblSpecializations.insets = new Insets(0, 0, 5, 5);
		gbc_lblSpecializations.gridx = 3;
		gbc_lblSpecializations.gridy = 3;
		add(lblSpecializations, gbc_lblSpecializations);
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setEnabled(false);
		splitPane.setResizeWeight(0.5);
		GridBagConstraints gbc_splitPane = new GridBagConstraints();
		gbc_splitPane.gridwidth = 3;
		gbc_splitPane.insets = new Insets(0, 0, 5, 5);
		gbc_splitPane.fill = GridBagConstraints.BOTH;
		gbc_splitPane.gridx = 1;
		gbc_splitPane.gridy = 4;
		add(splitPane, gbc_splitPane);
		
		JScrollPane spSpecialization = new JScrollPane();
		splitPane.setRightComponent(spSpecialization);
		
		JPanel pnlSpecialization = new JPanel();
		spSpecialization.setViewportView(pnlSpecialization);
		pnlSpecialization.setLayout(new BoxLayout(pnlSpecialization, BoxLayout.X_AXIS));
		
		JScrollPane spSubjects = new JScrollPane();
		splitPane.setLeftComponent(spSubjects);
		
		JPanel pnlSubjects = new JPanel();
		spSubjects.setViewportView(pnlSubjects);
		pnlSubjects.setLayout(new BoxLayout(pnlSubjects, BoxLayout.X_AXIS));

	}

}
