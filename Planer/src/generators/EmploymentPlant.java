package generators;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import adders.StudiesTimeAdder;
import data.DepartmentEntry;
import data.EmployeeEntry;
import data.SubjectEntry;
import data.primitives.SemesterType;
import data.primitives.SubjectType;
import misc.PdfUtils;

public class EmploymentPlant extends BasicRaport {
	private static Logger logger = LogManager.getLogger();

	final static String hdrTxt1 = "Plan zatrudnienia na rok akademicki";
	final static String hdrTxt2 = "według pracowników";
	
	final static String departmentTxt = "Zakład";
	final static String staffRes = "Pracownicy minimum kadrowego kierunku";
	final static String contractTxt = "Pozostali pracownicy Zakładu zatrudnieni na podstawie umowy o pracę";
	
	final static String departmentCell = "kierunek studiów";
	final static String yearCell = "rok studiów";
	final static String subjectFormCell = "rodzaj zajęć";
	final static String groupsAndStudentsCell = "liczba grup/studentów";
	final static String weeklyHoursCell = "liczba godzin tygodniowo";
	final static String semesterHoursCell = "liczba godzin w semestrze";
	
	final static String hoursSum = "Łączna liczba godzin:";
	final static String hoursSep = "w tym:";
	final static String fullTimeHours = "na studiach stacjonarnych:";
	final static String partTimeHours = "na studiach niestacjonarnych:";
	
	final static String summaryRowTxt = "Podsumowanie godzin semestru";
	
	Font defaultFont, tableFont, subjectTableFont;
	
	DepartmentEntry de;
	String year;
	
	public EmploymentPlant(DepartmentEntry de, String year) {
		super(null, "Plan zatrudnienia");
		doc = new Document(PageSize.A4.rotate());
		InitFonts();
		
		this.de = de;
		this.year = year;
	}
	
	Paragraph HeaderText(String year) {
		Paragraph headerPar = new Paragraph();
        Chunk hdrTxt = new Chunk(
        		hdrTxt1 + " " + year + " " + hdrTxt2,
        		defaultFont);
        headerPar.add(hdrTxt);
        return headerPar;
	}
	
	Paragraph DepartmentText(String dep) {
		Paragraph headerPar = new Paragraph();
        Chunk hdrTxt = new Chunk(
        		departmentTxt + " " + dep,
        		defaultFont);
        headerPar.add(hdrTxt);
        return headerPar;
	}
	
	Paragraph createPar(String txt) {
		Paragraph headerPar = new Paragraph();
        Chunk hdrTxt = new Chunk(txt, defaultFont);
        headerPar.add(hdrTxt);
        return headerPar;
	}
	
	Paragraph staffResTxt() {
		return createPar(staffRes);
	}
	
	Paragraph contractText() {
		return createPar(contractTxt);
	}
	
	String employeeFullTitle(EmployeeEntry ee) {
		return ee.getDegree() + " " + ee.getName() + " " + ee.getSurname();
	}
	
	PdfPTable HoursInfo(long fullTime, long partTime) {
		PdfPTable tbl = new PdfPTable(2);
		tbl.setSpacingBefore(2);
		tbl.setWidthPercentage(100);
		//tbl.setHorizontalAlignment(Element.ALIGN_LEFT);
		
		try {
			tbl.setWidths(new float[] {1, 3});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		PdfPCell cell = new PdfPCell(new Phrase(hoursSum, subjectTableFont));
		misc.PdfUtils.noBorder(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase(String.valueOf(fullTime + partTime), subjectTableFont));
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		misc.PdfUtils.noBorder(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase(hoursSep, subjectTableFont));
		misc.PdfUtils.noBorder(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase(" ", subjectTableFont));
		misc.PdfUtils.noBorder(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase(fullTimeHours, subjectTableFont));
		misc.PdfUtils.noBorder(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase(String.valueOf(fullTime), subjectTableFont));
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		misc.PdfUtils.noBorder(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase(partTimeHours, subjectTableFont));
		misc.PdfUtils.noBorder(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase(String.valueOf(partTime), subjectTableFont));
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		misc.PdfUtils.noBorder(cell);
		tbl.addCell(cell);
	
		return tbl;
	}
	
	private void addDepartmentHdrCell(PdfPTable tbl) { 
		tbl.addCell(headerCell(departmentCell));
	}

	private void addYearHdrCell(PdfPTable tbl) {
		tbl.addCell(headerCell(yearCell));
	}
	
	private void addSubjectFormCell(PdfPTable tbl) {
		tbl.addCell(headerCell(subjectFormCell));
	}
	
	private void addGroupsCell(PdfPTable tbl) {
		tbl.addCell(headerCell(groupsAndStudentsCell));
	}
	
	private void addWeeklyHoursCell(PdfPTable tbl) {
		tbl.addCell(headerCell(weeklyHoursCell));
	}
	
	private void addSemesterHoursCell(PdfPTable tbl) {
		tbl.addCell(headerCell(semesterHoursCell));
	}
	
	private void addCommonCells(PdfPTable tbl) {
		addDepartmentHdrCell(tbl);
		addYearHdrCell(tbl);
		addSubjectFormCell(tbl);
		addGroupsCell(tbl);
	}
	
	private void addSubjectTableHeaderFullTimeEntries(PdfPTable tbl) {
		addCommonCells(tbl);
		addWeeklyHoursCell(tbl);
		addSemesterHoursCell(tbl);
	}
	
	private void addSubjectTableHeaderPartTimeEntries(PdfPTable tbl) {
		addCommonCells(tbl);
		addWeeklyHoursCell(tbl);
		addSemesterHoursCell(tbl);
	}
	
	PdfPCell headerCell(String text) {
		PdfPCell cell = new PdfPCell(new Phrase(text, subjectTableFont));
		PdfUtils.centerCell(cell);
		cell.setRotation(90);
		return cell;
	}
	
	PdfPTable SubjectTableHeader() {
		PdfPTable tbl = new PdfPTable(14);
		tbl.setSpacingBefore(2);
		tbl.setWidthPercentage(100);
		/*try {
			tbl.setWidths(new float[] { 0.5f, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}*/
		
		PdfPCell cell = new PdfPCell(new Phrase("L.p.", subjectTableFont));
		PdfUtils.centerCell(cell);
		cell.setRowspan(2);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Przedmiot", subjectTableFont));
		PdfUtils.centerCell(cell);
		cell.setRowspan(2);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Studia stacjonarne", subjectTableFont));
		PdfUtils.centerCell(cell);
		cell.setColspan(6);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Studia niestacjonarne", subjectTableFont));
		PdfUtils.centerCell(cell);
		cell.setColspan(6);
		tbl.addCell(cell);
		
		addSubjectTableHeaderFullTimeEntries(tbl);
		addSubjectTableHeaderPartTimeEntries(tbl);       

        tbl.completeRow();
        return tbl;
	}

	@Override
	public
	boolean Generate(String fileName) {
		boolean ret = true;
		try {
			logger.info("Generowanie tabeli zbiorczej");
			PdfWriter.getInstance(doc, new FileOutputStream(fileName));
			doc.open();
			
			doc.add(HeaderText(this.year));
			doc.add(DepartmentText(misc.Utils.prepareDepartment(de.getName())));
			
			generateStaffResourceEntries(doc, de.getEmployees());
			generateCyvilContractEntries(doc, de.getEmployees());
			
		} catch (Exception e) {
			logger.fatal("Bład w trakcie tworzenia planu zatrudnienia: {}", e.getMessage());
			ret = false;
		} finally {
			doc.close();
		}
		return ret;
	}

	private void generateCyvilContractEntries(Document doc, List<EmployeeEntry> employees) throws DocumentException {
		doc.add(contractText());
		
		StudiesTimeAdder sta = null;
		for (EmployeeEntry ee : employees) {
			if (ee.isCyvilContract()) continue;
			
			sta = new StudiesTimeAdder(ee.getSubjects());
			// Personalia
			doc.add(createPar(employeeFullTitle(ee)));
			// Dane godzinowe
			doc.add(HoursInfo(sta.getFullTimeStudies(), sta.getPartTimeStudies()));
			// Zimowe
			doc.add(semesterTxt(" zimowy"));
			doc.add(generateWinterTable(ee.getSubjects(), sta));
			// Letnie
			doc.add(semesterTxt(" letni"));
			doc.add(generateSummerTable(ee.getSubjects(), sta));
			
			// Nowa strona
			doc.newPage();
		}
	}

	private void generateStaffResourceEntries(Document doc, List<EmployeeEntry> employees) throws DocumentException {
		doc.add(staffResTxt());
		
		StudiesTimeAdder sta = null;
		for (EmployeeEntry ee : employees) {
			if (!ee.isCyvilContract()) continue;
			
			sta = new StudiesTimeAdder(ee.getSubjects());
			// Personalia
			doc.add(createPar(employeeFullTitle(ee)));
			// Dane godzinowe
			doc.add(HoursInfo(sta.getFullTimeStudies(), sta.getPartTimeStudies()));
			// Zimowe
			doc.add(semesterTxt(" zimowy"));
			doc.add(generateWinterTable(ee.getSubjects(), sta));
			// Letnie
			doc.add(semesterTxt(" letni"));
			doc.add(generateSummerTable(ee.getSubjects(), sta));
			
			// Nowa strona
			doc.newPage();
		}
	}
	
	PdfPTable generateWinterTable(List<SubjectEntry> subjects, StudiesTimeAdder sta) {
		List<SubjectEntry> winter = new ArrayList<SubjectEntry>();
		for (SubjectEntry se : subjects) {
			if (se.getSemType() == SemesterType.WINTER) {
				winter.add(se);
			}
		}
		return generateTable(winter, " zimowego",
				sta.getFullTimeWinterSemesterHours(),
				sta.getPartTimeWinterSemesterHours());
	}
	
	PdfPTable generateSummerTable(List<SubjectEntry> subjects, StudiesTimeAdder sta) {
		List<SubjectEntry> summer = new ArrayList<SubjectEntry>();
		for (SubjectEntry se : subjects) {
			if (se.getSemType() == SemesterType.SUMMER) {
				summer.add(se);
			}
		}
		return generateTable(summer, " letniego",
				sta.getFullTimeSummerSemesterHours(),
				sta.getPartTimeSummerSemesterHours());
	}
	
	PdfPTable generateTable(List<SubjectEntry> subjects, String txt, long x, long y) {
		PdfPTable tbl = SubjectTableHeader();
		
		if (subjects.isEmpty()) {
			misc.PdfUtils.addEmptyRow(tbl, 14);
			summaryRow(tbl, txt, x, y);
			return tbl;
		}
		
		int index = 1;
		while (!subjects.isEmpty()) {
			SubjectEntry se = subjects.get(0);
			SubjectEntry s1 = null;
			
			addSubjectNameToTable(tbl, se, index);
			
			if (se.getSubType() == SubjectType.FULLTIME) {
				s1 = getPartTimeSub(subjects, se);
			} else {
				s1 = getFullTimeSub(subjects, se);
			}
			
			if (s1 != null) {
				addSubs(tbl, se, s1);
				subjects.remove(se);
				subjects.remove(s1);
			} else {
				addSingleSub(tbl, se);
				subjects.remove(se);
			}
			index += 1;
		}
		
		summaryRow(tbl, txt, x, y);
		return tbl;
	}

	Paragraph semesterTxt(String txt) {
		return createPar("Semestr " + txt);
	}

	@Override
	void InitFonts() {
		defaultFont = FontFactory.getFont("garamond bold", "Cp1250", true);		
		tableFont = new Font(defaultFont);
		
		subjectTableFont = new Font(defaultFont);
		subjectTableFont.setSize(8.0f);
	}
	
	boolean equalSub(SubjectEntry s1, SubjectEntry s2) {
		return s1.getSemNum() == s2.getSemNum()
				&& s1.getSubForm() == s2.getSubForm()
				&& s1.getSemType() == s2.getSemType()
				&& s1.getSpec().getName().equals(s2.getSpec().getName())
				&& s1.getName().equals(s2.getName());
	}
	
	SubjectEntry getSubEntry(List<SubjectEntry> subjects, SubjectEntry sub, SubjectType st) {
		for (SubjectEntry se : subjects) {
			if (equalSub(se, sub) && (se.getSubType() == st)) {
				return se;
			}
		}
		return null;
	}
	
	SubjectEntry getPartTimeSub(List<SubjectEntry> subjects, SubjectEntry se) {
		return getSubEntry(subjects, se, SubjectType.PARTTIME);
	}
	
	SubjectEntry getFullTimeSub(List<SubjectEntry> subjects, SubjectEntry se) {
		return getSubEntry(subjects, se, SubjectType.FULLTIME);
	}
	
	void addSingleSub(PdfPTable tbl, SubjectEntry se) {
		if (se.getSubType() == SubjectType.FULLTIME) {
			addFullTimeSubjectDataToTable(tbl, se);
			addEmptySubCells(tbl, 6);
		} else {
			addEmptySubCells(tbl, 6);
			addPartTimeSubjectDataToTable(tbl, se);   
		}
	}
	
	void addSubs(PdfPTable tbl, SubjectEntry s1, SubjectEntry s2) {
		if (s1.getSubType() == SubjectType.FULLTIME) {
			addFullTimeSubjectDataToTable(tbl, s1);
			addPartTimeSubjectDataToTable(tbl, s2);
		} else {
			addFullTimeSubjectDataToTable(tbl, s2);
			addPartTimeSubjectDataToTable(tbl, s1);   
		} 	
	}
	
	void addEmptySubCells(PdfPTable tbl, int cells) {
		for (int i = 0; i < cells; i++) {
			PdfPCell cell = new PdfPCell(new Phrase("-", subjectTableFont));
			PdfUtils.centerCell(cell);
			tbl.addCell(cell);
		}
	}
	
	void addFullTimeSubjectDataToTable(PdfPTable tbl, SubjectEntry se) {
		addCommonSubjectDataToTable(tbl, se);
		addSubjectWeeklyHours(tbl, se);
		addSubjectSemesterHours(tbl, se);
	}
	
	void addPartTimeSubjectDataToTable(PdfPTable tbl, SubjectEntry se) {
		addCommonSubjectDataToTable(tbl, se);
		addSubjectWeeklyHours(tbl, se);
		addSubjectSemesterHours(tbl, se);
	}
	
	void addSubjectSemesterHours(PdfPTable tbl, SubjectEntry se) {
		// godzin w semestrze
		PdfPCell cell;
		cell = new PdfPCell(new Phrase(String.valueOf(se.getHoursSemester()), subjectTableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
	}
	
	void addSubjectWeeklyHours(PdfPTable tbl, SubjectEntry se) {
		// godzin tygodniowo
		PdfPCell cell;
		cell = new PdfPCell(new Phrase(String.valueOf(se.getHoursWeekly()).toString(), subjectTableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
	}
	
	void addCommonSubjectDataToTable(PdfPTable tbl, SubjectEntry se) {
		PdfPCell cell;
		// Kierunek
		cell = new PdfPCell(new Phrase(de.getName(), subjectTableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		// Rok
		cell = new PdfPCell(new Phrase(se.getStudyYear().toString(), subjectTableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		// Rodzaj zajęć TODO
		cell = new PdfPCell(new Phrase(se.getSubForm().toString(), subjectTableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		// Lb grup/studentów
		String val = String.format("%d/%d", se.getNumOfGroups(), se.getNumOfStudents());
		cell = new PdfPCell(new Phrase(val, subjectTableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);	
	}

	void addSubjectNameToTable(PdfPTable tbl, SubjectEntry se, int idx) {
		PdfPCell cell;
		// L.p.
		cell = new PdfPCell(new Phrase(String.valueOf(idx), subjectTableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		// Przedmiot
		cell = new PdfPCell(new Phrase(se.getName(), subjectTableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
	}
	
	void summaryRow(PdfPTable tbl, String txt, long x, long y) {
		PdfPCell cell = new PdfPCell(new Phrase(summaryRowTxt + txt, subjectTableFont));
		PdfUtils.centerCell(cell);
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell.setColspan(7);
		misc.PdfUtils.noBorder(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase(String.valueOf(x),
				subjectTableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell();
		cell.setColspan(5);
		misc.PdfUtils.noBorder(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase(String.valueOf(y),
				subjectTableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
	}

}
