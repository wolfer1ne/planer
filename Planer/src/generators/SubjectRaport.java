package generators;

import java.io.FileOutputStream;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import data.DepartmentEntry;
import data.EmployeeEntry;
import data.SpecializationEntry;
import data.SubjectEntry;
import data.primitives.SemesterBasicInfo;
import data.primitives.StudyYear;
import data.primitives.SubjectType;
import misc.PdfUtils;

public abstract class SubjectRaport extends BasicRaport {
	private static Logger logger = LogManager.getLogger();
	
	DepartmentEntry de;
	String depName;
	String academicYear;
	SubjectType subType;
	
	Font tableFont, defaultFont;
	PdfPTable table;
	
	final static String hdrSubTitle = "PLAN ZAJĘĆ DYDAKTYCZNYCH W ROKU AKADEMICKIM";
	final static String commonSubsRow = "Przedmioty wspólne dla całego roku";
	final static String specSubsTitle = "Przedmioty ze specjalności";
	final static String specHdr = "specjalność:";
	
	final static String studentsAmmountTxt = "Liczba studentów -";
	
	final static String groupPrefixTxt = "Liczba grup";
	final static String groupsAuditoryTxt = groupPrefixTxt + " audytoryjnych -";
	final static String groupsLabolatoryTxt = groupPrefixTxt + " labolatoryjnych -";
	final static String groupsForeignLanguagesTxt = groupPrefixTxt + " z języka obcego -";
	final static String groupsPETxt = groupPrefixTxt + " z WF-u - ";
	final static String groupsSeminaryTxt = groupPrefixTxt + " seminaryjnych - ";
	
	final static String subjectNameTxt = "Nazwa przedmiotu";
	
	public SubjectRaport(DepartmentEntry de, String academicYear, SubjectType st) {
		super(null, null);
		doc = new Document(PageSize.A4.rotate());
		InitFonts();
		this.de = de;
		this.depName = misc.Utils.prepareDepartment(de.getName()).toUpperCase();
		this.academicYear = academicYear;
		this.subType = st;
	}

	Paragraph HeaderText() {
		Paragraph headerPar = new Paragraph();
		headerPar.setAlignment(Element.ALIGN_CENTER);
        Chunk hdrTxt = new Chunk("ZAKŁAD " + depName,
        		tableFont);
        headerPar.add(hdrTxt);
        return headerPar;
	}
	
	Paragraph headerSubTitle() {
		Paragraph headerPar = new Paragraph();
        Chunk hdrTxt = new Chunk(hdrSubTitle + " " + academicYear
        		+ "  studia " + subType.toString(),
        		tableFont);
        headerPar.add(hdrTxt);
        return headerPar;
	}
	
	PdfPTable createTable() {
		PdfPTable tbl = new PdfPTable(6);
		tbl.setSpacingBefore(20);
		tbl.setWidthPercentage(100);
		
		return tbl;
	}
	
	PdfPCell sbiYearOne(SemesterBasicInfo sbi)
	{
		PdfPCell cell = basicSbi(sbi);
		cell.addElement(langsEntry(sbi.getGroupsForeignLanguages()));
		cell.addElement(peEntry(sbi.getGroupsPE()));
		return cell;
	}
	
	PdfPCell sbiYearTwo(SemesterBasicInfo sbi) {
		PdfPCell cell = basicSbi(sbi);
		cell.addElement(langsEntry(sbi.getGroupsForeignLanguages()));
		return cell;
	}
	
	PdfPCell sbiYearThreeFour(SemesterBasicInfo sbi) {
		PdfPCell cell = basicSbi(sbi);
		cell.addElement(seminaryEntry(sbi.getGroupsSeminary()));
		return cell;
	}
	
	PdfPCell basicSbi(SemesterBasicInfo sbi) {
		PdfPCell cell = new PdfPCell();
		cell.addElement(studentsEntry(sbi.getAmmountOfStudents()));
		cell.addElement(auditoryEntry(sbi.getGroupsAuditory()));
		cell.addElement(labolatoryEntry(sbi.getGroupsLabolatory()));
		cell.setColspan(6);
		return cell;
	}	
	
	Phrase studentsEntry(long val) {
		return basicSbiEntry(studentsAmmountTxt, val);
	}
	
	Phrase auditoryEntry(long val) {
		return basicSbiEntry(groupsAuditoryTxt, val);
	}
	
	Phrase labolatoryEntry(long val) {
		return basicSbiEntry(groupsLabolatoryTxt, val);
	}
	
	Phrase langsEntry(long val) {
		return basicSbiEntry(groupsForeignLanguagesTxt, val);
	}
	
	Phrase peEntry(long val) {
		return basicSbiEntry(groupsPETxt, val);
	}
	
	Phrase seminaryEntry(long val) {
		return basicSbiEntry(groupsSeminaryTxt, val);
	}
	
	Phrase basicSbiEntry(String txt, long val) {
		return new Phrase (txt + " " + String.valueOf(val), tableFont);
	}
	
	void
	addTableHdr(PdfPTable tbl)
	{
		PdfPCell cell = new PdfPCell(new Phrase("Przedmiot", tableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Forma zajęć", tableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Semestr", tableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Liczba godzin", tableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Liczba grup", tableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Stopień/tytuł naukowy imię i nazwisko", tableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
	}
	
	void addSubjectNameRow(PdfPTable tbl) {
		PdfPCell cell = new PdfPCell(new Phrase(subjectNameTxt, tableFont));
		cell.setColspan(6);
		tbl.addCell(cell);
	}
	
	void
	addSpecSubsTxt(PdfPTable tbl) {
		PdfPCell cell = textRow(specSubsTitle);
		tbl.addCell(cell);
	}
	
	void
	addCommonSubsTxt(PdfPTable tbl) {
		PdfPCell cell = textRow(commonSubsRow);
		tbl.addCell(cell);
	}
	
	void
	addYearRow(PdfPTable tbl, StudyYear sy) {
		PdfPCell cell = textRow("ROK " + sy.toString());	
		tbl.addCell(cell);
	}
	
	PdfPCell
	textRow(String txt) {
		PdfPCell cell = new PdfPCell(new Phrase(txt, tableFont));
		PdfUtils.centerCell(cell);
		cell.setColspan(6);
		return cell;
	}
	
	Phrase
	createSpecPhrase(SpecializationEntry se) {
		return new Phrase(specHdr + " " + se.getName(), tableFont);
	}
	PdfPCell
	createSpecCell(SpecializationEntry se, StudyYear sy) {
		PdfPCell cell = new PdfPCell();
		SemesterBasicInfo sbi = se.getSemesterInfo(sy, subType);
		cell.addElement(createSpecPhrase(se));
		cell.addElement(studentsEntry(sbi.getAmmountOfStudents()));
		cell.addElement(auditoryEntry(sbi.getGroupsAuditory()));
		cell.addElement(labolatoryEntry(sbi.getGroupsLabolatory()));
		cell.setColspan(6);
		return cell;
	}
	
	void
	addTableHdrNumbers(PdfPTable tbl) {
		PdfPCell cell;
		for (int i = 0; i < 6; i++) {
			cell = new PdfPCell(new Phrase(String.valueOf(i), tableFont));
			PdfUtils.centerCell(cell);
			tbl.addCell(cell);
		}
	}

	void
	addSubjectToTable(PdfPTable tbl, SubjectEntry se, String degree, String fullname) {
		// Przedmiot
		PdfPCell cell;
        cell = new PdfPCell(new Phrase(se.getName(), tableFont));
        PdfUtils.centerCell(cell);
        tbl.addCell(cell);
        
        // Forma zajęć
        cell = new PdfPCell(new Phrase(se.getSubForm().toString(), tableFont));
        PdfUtils.centerCell(cell);
        tbl.addCell(cell);
        
        // Semestr
        cell = new PdfPCell(new Phrase(se.getSemNum().toString(), tableFont));
        PdfUtils.centerCell(cell);
        tbl.addCell(cell);
        
        // Liczba godzin
        cell = new PdfPCell(new Phrase(String.valueOf(se.getHoursSemester()), tableFont));
        PdfUtils.centerCell(cell);
        tbl.addCell(cell);
        
        // Liczba grup
        cell = new PdfPCell(new Phrase(String.valueOf(se.getNumOfGroups()), tableFont));
        PdfUtils.centerCell(cell);
        tbl.addCell(cell);
        
        cell = new PdfPCell(new Phrase(degree + " " + fullname, tableFont));
        PdfUtils.centerCell(cell);
        tbl.addCell(cell);
	}
	@Override
	void InitFonts() {
		tableFont = FontFactory.getFont("garamond bold", "Cp1250", true);
	}
	
	void
	addCommonSubs(PdfPTable tbl, StudyYear sy, List<EmployeeEntry> employees) {
		int c = 0;
		if (sy != StudyYear.I) {
			addCommonSubsTxt(tbl);
			addSubjectNameRow(tbl);
		} else {
			addSubjectNameRow(tbl);
		}
		for (EmployeeEntry ee : de.getEmployees()) {
			for (SubjectEntry se : ee.getSubjects()) {
				if (se.getSubType() != subType) continue;
				// INNY ROK
				if (misc.Utils.SemToYear(se.getSemNum()) != sy) continue;
					if (null == se.getSpec() || se.getSpec().getName().equals(misc.Utils.EMPTY_SPEC)) {
						addSubjectToTable(table, se, ee.getDegree(),
								ee.getName() + " " + ee.getSurname());
						c += 1;
				}
			}
		}
		if (c == 0) {
			misc.PdfUtils.addEmptyRow(tbl, 6);
		}
	}

	@Override
	public
	boolean Generate(String fileName) {
		logger.info("Generowanie raportu - studia {}",
				subType.toString());
		
		try {
			PdfWriter.getInstance(doc, new FileOutputStream(fileName));

			doc.open();
			
			doc.add(HeaderText());
			doc.add(headerSubTitle());
			
			table = createTable();
			addTableHdr(table);
			addTableHdrNumbers(table);
			
			addYearRow(table, StudyYear.I);
			table.addCell(sbiYearOne(de.getSemesterInfo(StudyYear.I, subType)));
			addCommonSubs(table, StudyYear.I, de.getEmployees());
			
			addYearRow(table, StudyYear.II);
			table.addCell(sbiYearTwo(de.getSemesterInfo(StudyYear.II, subType)));
			addCommonSubs(table, StudyYear.II, de.getEmployees());
			addSpecSubs(table, StudyYear.II, de.getEmployees(), de.getSpecializations());
			
			addYearRow(table, StudyYear.III);
			table.addCell(sbiYearThreeFour(de.getSemesterInfo(StudyYear.III, subType)));
			addCommonSubs(table, StudyYear.III, de.getEmployees());
			addSpecSubs(table, StudyYear.III, de.getEmployees(), de.getSpecializations());
			
			addYearRow(table, StudyYear.IV);
			table.addCell(sbiYearThreeFour(de.getSemesterInfo(StudyYear.IV, subType)));
			addSpecSubs(table, StudyYear.IV, de.getEmployees(), de.getSpecializations());
			
			doc.add(table);
			doc.close();
		} catch (Exception e) {
			e.printStackTrace();
			doc.close();
			return false;
		}
		
		return true;
	}

	private void addSpecSubs(PdfPTable table, StudyYear ii, List<EmployeeEntry> employees,
			List<SpecializationEntry> specializations) {
		int c = 0;
		
		addSpecSubsTxt(table);
		
		for (SpecializationEntry spec : specializations) {
			if (spec.getName().equals(misc.Utils.EMPTY_SPEC)) continue;	
			
			table.addCell(createSpecCell(spec, ii));
			addSubjectNameRow(table);
			for (EmployeeEntry ee : employees) {
				for (SubjectEntry se : ee.getSubjects()) {
					if (se.getSubType() != subType) continue;
					if (se.getSpec().getName().equals(spec.getName())) {
						if (misc.Utils.SemToYear(se.getSemNum()) != ii) continue;
						addSubjectToTable(table, se, ee.getDegree(),
								ee.getName() + " " + ee.getSurname());
						c += 1;
					}
				}
			}
			if (c == 0) {
				misc.PdfUtils.addEmptyRow(table, 6);
			}
			c = 0;
		}
		
	}
} 