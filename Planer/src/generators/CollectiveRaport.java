package generators;

import java.io.FileOutputStream;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import adders.CollectiveRaportAdder;
import adders.StudiesTimeAdder;
import data.EmployeeEntry;
import misc.PdfUtils;

public class CollectiveRaport extends BasicRaport {
	private static Logger logger = LogManager.getLogger();
	
	Font tableFont, defaultFont;
	
	PdfPTable table;
	
	List <EmployeeEntry> employees;
	
	final static String STAFF_RES = "Minimum kadrowe";
	final static String CYVIL_CONTRACT_HDR = "Pozostali pracownicy zakładu zatrudnieni na podstawie umowy o dzieło";

	public CollectiveRaport(String fileNameOutputFormat, List <EmployeeEntry> employees) {
		super(null, "Tabela zbiorcza");
		doc = new Document(PageSize.A4.rotate());
		this.employees = employees;
		InitFonts();			
	}
	
	void
	convertEntryToTable(PdfPTable table, EmployeeEntry ee, StudiesTimeAdder sta)
	{
		// Imie i nazwisko
		PdfPCell cell;
        cell = new PdfPCell(new Phrase(ee.getName() + " " + ee.getSurname(), tableFont));
        PdfUtils.centerCell(cell);
        table.addCell(cell);
        
        // Wymiar etatu
        cell = new PdfPCell(new Phrase(String.valueOf(ee.getJobTime()), tableFont));
        PdfUtils.centerCell(cell);
        table.addCell(cell);
        
        // STACJONARNE
        cell = new PdfPCell(new Phrase(String.valueOf(sta.getFullTimeStudies()), tableFont));
        PdfUtils.centerCell(cell);
        table.addCell(cell);
        
        // NIESTACJONARNE
        cell = new PdfPCell(new Phrase(String.valueOf(sta.getPartTimeStudies()), tableFont));
        PdfUtils.centerCell(cell);
        table.addCell(cell);
        
        // RAZEM
        cell = new PdfPCell(new Phrase(String.valueOf(sta.getSum()), tableFont));
        PdfUtils.centerCell(cell);
        table.addCell(cell);
        
        // PONAD STACJONARNE
        cell = new PdfPCell(new Phrase(String.valueOf(sta.getSumOverFullTimeStudies()), tableFont));
        PdfUtils.centerCell(cell);
        table.addCell(cell);
        
        // PONAD NIESTACJONARNE
        cell = new PdfPCell(new Phrase(String.valueOf(sta.getSumOverPartTimeStudies()), tableFont));
        PdfUtils.centerCell(cell);
        table.addCell(cell);
	}
	
	PdfPTable
	createTable()
	{
		PdfPTable table = new PdfPTable(7);
		createTblHeader(table);
		addHdrTxt(table, STAFF_RES);
		return table;
	}

	@Override
	public boolean Generate(String fileName) {
		boolean ret = true;
		try {
			logger.info("Generowanie tabeli zbiorczej");
			PdfWriter.getInstance(doc, new FileOutputStream(fileName));
			doc.open();
			
			table = createTable();
			table.setSpacingBefore(20);
	        table.setWidthPercentage(100);
			
			CollectiveRaportAdder cra = new CollectiveRaportAdder();
			
			// Minimum kadrowe
			for (EmployeeEntry ee : employees) {
				if (ee.isCyvilContract()) continue;
				logger.debug("Minimum kadrowe; Konwertowanie {}", ee.toString());
				StudiesTimeAdder sta = new StudiesTimeAdder(ee);
				convertEntryToTable(table, ee, sta);
				cra.add(sta);
			}
			
			// Pozostali pracownicy
			addHdrTxt(table, CYVIL_CONTRACT_HDR);
			for (EmployeeEntry ee : employees) {
				if (!ee.isCyvilContract()) continue;
				logger.debug("Pozostali pracownicy; Konwertowanie {}", ee.toString());
				StudiesTimeAdder sta = new StudiesTimeAdder(ee);
				convertEntryToTable(table, ee, sta);
				cra.add(sta);
			}
			
			addSummaryRow(table, cra);
			doc.add(table);
			logger.info("Tabela zbiorcza wygenerowana");
			
		} catch (Exception e) {
			logger.fatal("Bład w trakcie tworzenia tabeli zbiorczej: {}", e.getMessage());
			ret = false;
		} finally {
			doc.close();
		}
		return ret;
	}
	
	private void addSummaryRow(PdfPTable table, CollectiveRaportAdder cra) {
		// Imie i nazwisko
				PdfPCell cell;
		        cell = new PdfPCell(new Phrase("RAZEM", tableFont));
		        PdfUtils.centerCell(cell);
		        table.addCell(cell);
		        
		        // Wymiar etatu
		        cell = new PdfPCell(new Phrase(String.valueOf(cra.getSumJobTime()), tableFont));
		        PdfUtils.centerCell(cell);
		        table.addCell(cell);
		        
		        // STACJONARNE
		        cell = new PdfPCell(new Phrase(String.valueOf(cra.getSumFullTimeStudies()), tableFont));
		        PdfUtils.centerCell(cell);
		        table.addCell(cell);
		        
		        // NIESTACJONARNE
		        cell = new PdfPCell(new Phrase(String.valueOf(cra.getSumPartTimeStudies()), tableFont));
		        PdfUtils.centerCell(cell);
		        table.addCell(cell);
		        
		        // RAZEM
		        cell = new PdfPCell(new Phrase(String.valueOf(cra.sum()), tableFont));
		        PdfUtils.centerCell(cell);
		        table.addCell(cell);
		        
		        // PONAD STACJONARNE
		        cell = new PdfPCell(new Phrase(String.valueOf(cra.getSumOverFullTimeStudies()), tableFont));
		        PdfUtils.centerCell(cell);
		        table.addCell(cell);
		        
		        // PONAD NIESTACJONARNE
		        cell = new PdfPCell(new Phrase(String.valueOf(cra.getSumOverPartTimeStudies()), tableFont));
		        PdfUtils.centerCell(cell);
		        table.addCell(cell);
	}

	void
	addHdrTxt(PdfPTable table, String hdr)
	{
		PdfPCell cell;
        cell = new PdfPCell(new Phrase(hdr, tableFont));
        PdfUtils.centerCell(cell);
        cell.setColspan(7);
        table.addCell(cell); // 1/1
	}
	
	void
	createTblHeader(PdfPTable table)
	{
		PdfPCell cell;
        cell = new PdfPCell(new Phrase("Imię i nazwisko", tableFont));
        PdfUtils.centerCell(cell);
        cell.setRowspan(2);
        table.addCell(cell); // 1/1
        
        cell = new PdfPCell(new Phrase("Wymiar etatu", tableFont));
        cell.setRowspan(2);
        PdfUtils.centerCell(cell);
        table.addCell(cell); // 1/1
        
        cell = new PdfPCell(new Phrase("St. stacjonarne", tableFont));
        cell.setRowspan(2);
        PdfUtils.centerCell(cell);
        table.addCell(cell); // 1/1
        
        cell = new PdfPCell(new Phrase("St. niestacjonarne", tableFont));
        cell.setRowspan(2);
        PdfUtils.centerCell(cell);
        table.addCell(cell); // 1/1
        
        cell = new PdfPCell(new Phrase("Razem", tableFont));
        cell.setRowspan(2);
        PdfUtils.centerCell(cell);
        table.addCell(cell); // 1/1
        
        cell = new PdfPCell(new Phrase("ponadwymiarowe", tableFont));
        PdfUtils.centerCell(cell);
        cell.setColspan(2);
        table.addCell(cell); // 1/1
        
        cell = new PdfPCell(new Phrase("St. stacjonarne", tableFont));
        PdfUtils.centerCell(cell);
        table.addCell(cell); // 1/1
        
        cell = new PdfPCell(new Phrase("St. niestacjonarne", tableFont));
        PdfUtils.centerCell(cell);
        table.addCell(cell); // 1/1   
	}

	@Override
	void InitFonts() {
		tableFont = FontFactory.getFont("garamond bold", "Cp1250", true);
	}

	/*public static void main(String[] args) {
		PdfUtils.registerFonts();
		CollectiveRaport s = new CollectiveRaport(null, null);
		s.Generate("TABELA ZBIORCZA.pdf");
	}*/

}
