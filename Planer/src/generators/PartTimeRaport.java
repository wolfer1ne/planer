package generators;

import data.DepartmentEntry;
import data.primitives.SubjectType;

public class PartTimeRaport extends SubjectRaport {

	public PartTimeRaport(DepartmentEntry de, String academicYear) {
		super(de, academicYear, SubjectType.PARTTIME);
	}
}