package generators;

import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.TabStop.Alignment;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import adders.CollectiveRaportAdder;
import adders.StudiesTimeAdder;
import app.Main;
import data.DepartmentEntry;
import data.EmployeeEntry;
import data.SubjectEntry;
import data.primitives.EmployeeActivity;
import data.primitives.SemesterType;
import data.primitives.SubjectType;
import misc.PdfUtils;

public class IndividualRaport extends BasicRaport {
	
	private static final Logger logger = LogManager.getLogger();
	
	private static final String degreeWithNameAndSurname = "Stopień/tytuł naukowy, imię i nazwisko:";
	String imagePath;
	Font defaultFont, infoFont, subjectTableFont;
	
	List<SubjectEntry> winter = new ArrayList<SubjectEntry>();
	List<SubjectEntry> summer = new ArrayList<SubjectEntry>();	
	
	final static String tableTitleBegin = "A. Zakres i wymiar zajęć dydaktycznych - semestr";
	final static String headerTxt = "Szczegółowy zakres i wymiar zajęć dydaktycznych oraz"
    		+ " obowiązków pozadydaktycznych i organizacyjnych w roku akademickim";
	
	final static String nondidacticTblHdr = "B. Obowiązki pozadydaktyczne";
	final static String organizationalTblHdr = "C. Obowiązki organizacyjne";
	final static String miscTblHdr = "D. Wymiar zniżki pensum dydaktycznego z innych tytułów";
	
	final static String DateAndSignTxt = "Data i podpis pracownika";
	final static String DateAndSignDepartmentHeadMasterTxt = "Data i podpis Dyrektora Instytutu/Kierownika studium";
	final static String AcceptSignTxt = "Akceptacja Prorektora ds. Studiów";
	
	final static String summaryRowTxt = "Podsumowanie godzin semestru";
	
	DepartmentEntry de;
	EmployeeEntry ee;
	
	StudiesTimeAdder sta;
	
	public IndividualRaport(DepartmentEntry de, EmployeeEntry ee, String imgPath) {
		super(null, "Raport indywidualny");
		doc = new Document(PageSize.A4);
		this.de = de;
		this.ee = ee;
		this.imagePath = imgPath;
		InitFonts();
		
		sta = new StudiesTimeAdder(ee.getSubjects());
		splitSubjects();
	}

	void InitFonts() {
		defaultFont = FontFactory.getFont("garamond bold", "Cp1250", true);
		//defaultFont.setSize(12.0f);
		
		infoFont = new Font(defaultFont);
		infoFont.setSize(9.0f);
		
		subjectTableFont = new Font(defaultFont);
		subjectTableFont.setSize(8.0f);
	}
	
	void splitSubjects() {
		for (SubjectEntry se : ee.getSubjects()) {
			if (se.getSemType() == SemesterType.WINTER) {
				winter.add(se);
			} else {
				summer.add(se);
			}
		}
	}
	
	boolean equalSub(SubjectEntry s1, SubjectEntry s2) {
		return s1.getSemNum() == s2.getSemNum()
				&& s1.getSubForm() == s2.getSubForm()
				&& s1.getSemType() == s2.getSemType()
				&& s1.getSpec().getName().equals(s2.getSpec().getName())
				&& s1.getName().equals(s2.getName());
	}
	
	SubjectEntry getSubEntry(SubjectEntry sub, SubjectType st) {
		for (SubjectEntry se : ee.getSubjects()) {
			if (equalSub(se, sub) && (se.getSubType() == st)) {
				return se;
			}
		}
		return null;
	}
	
	SubjectEntry getPartTimeSub(SubjectEntry se) {
		return getSubEntry(se, SubjectType.PARTTIME);
	}
	
	SubjectEntry getFullTimeSub(SubjectEntry se) {
		return getSubEntry(se, SubjectType.FULLTIME);
	}
	
	void addSubjectNameToTable(PdfPTable tbl, SubjectEntry se, int idx) {
		PdfPCell cell;
		// L.p.
		cell = new PdfPCell(new Phrase(String.valueOf(idx), infoFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		// Przedmiot
		cell = new PdfPCell(new Phrase(se.getName(), infoFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
	}
	
	void addSubjectDataToTable(PdfPTable tbl, SubjectEntry se) {
		PdfPCell cell;
		// Kierunek
		cell = new PdfPCell(new Phrase(de.getName(), infoFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		// Rok
		cell = new PdfPCell(new Phrase(se.getStudyYear().toString(), infoFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		// Rodzaj zajęć TODO
		cell = new PdfPCell(new Phrase(se.getSubForm().toString(), infoFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		// Lb grup/studentów
		String val = String.format("%d/%d", se.getNumOfGroups(), se.getNumOfStudents());
		cell = new PdfPCell(new Phrase(val, infoFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		// godzin tygodniowo
		cell = new PdfPCell(new Phrase(String.valueOf(se.getHoursWeekly()).toString(), infoFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		// godzin w semestrze
		cell = new PdfPCell(new Phrase(String.valueOf(se.getHoursSemester()), infoFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
	}

	void addEmptySubCells(PdfPTable tbl) {
		for (int i = 0; i < 6; i++) {
			PdfPCell cell = new PdfPCell(new Phrase("-", infoFont));
			PdfUtils.centerCell(cell);
			tbl.addCell(cell);
		}
	}
	
	Image LogoImage() throws DocumentException, MalformedURLException, IOException {
		Image image = Image.getInstance(imagePath);
		return image;
	}
	
	Paragraph HeaderText() {
		Paragraph headerPar = new Paragraph();
		headerPar.setAlignment(Element.ALIGN_CENTER);
        Chunk hdrTxt = new Chunk(headerTxt, defaultFont);
        headerPar.add(hdrTxt);
        return headerPar;
	}
	
	Paragraph TableHeader(String text) {
		Paragraph p = new Paragraph();
		Chunk txt = new Chunk(text, infoFont);
		p.add(txt);
		return p;
	}
	
	String employeeFullTitle() {
		return ee.getDegree() + " " + ee.getName() + " " + ee.getSurname();
	}
	
	PdfPTable InfoTable() {
		PdfPTable table = new PdfPTable(2);
        table.setSpacingBefore(20);
        table.setWidthPercentage(100);
        
        try {
        	table.setWidths(new float[] {3, 2});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
        
        PdfPCell cell;
        cell = new PdfPCell(new Phrase(
        		degreeWithNameAndSurname + " " + employeeFullTitle(),
        		infoFont));
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell); // 1/1

        
        cell = new PdfPCell(new Phrase(
        		"Stanowisko: " + ee.getPosition(),
        		infoFont));
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell); // 1/3
     
        cell = new PdfPCell(new Phrase(
        		"Zakład " + misc.Utils.prepareDepartment(de.getName()),
        		infoFont));
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell); // 2/1
        
        cell = new PdfPCell(new Phrase(
        		"Wymiar etatu: " + String.valueOf(ee.getJobTime()) + " godzin",
        		infoFont));
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell); // 2/3
              
        String t = String.format("Minimum kadrowe kierunku: %s", ee.isCyvilContract() ? "TAK" : "NIE");
        cell = new PdfPCell(new Phrase(t, infoFont));
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell); // 3/1
        
        cell = new PdfPCell();
        cell.setBorder(Rectangle.NO_BORDER);
        table.addCell(cell); // 3/4
        
        return table;
	}
	
	void addSubjectTableHeaderEntries(PdfPTable tbl) {
		PdfPCell cell;
		cell = new PdfPCell(new Phrase("kierunek" + Chunk.NEWLINE+ "studiów", subjectTableFont));
		PdfUtils.centerCell(cell);
		cell.setRotation(90);
        tbl.addCell(cell);
        
        cell = new PdfPCell(new Phrase("rok studiów", subjectTableFont));
        PdfUtils.centerCell(cell);
		cell.setRotation(90);
        tbl.addCell(cell);
        
        cell = new PdfPCell(new Phrase("rodzaj zajęć", subjectTableFont));
        PdfUtils.centerCell(cell);
		cell.setRotation(90);
        tbl.addCell(cell);
        
        cell = new PdfPCell(new Phrase("liczba grup"+ Chunk.NEWLINE+"/ studentów", subjectTableFont));
        PdfUtils.centerCell(cell);
		cell.setRotation(90);
        tbl.addCell(cell);
        
        cell = new PdfPCell(new Phrase("godzin"+ Chunk.NEWLINE+"tygodniowo", subjectTableFont));
        PdfUtils.centerCell(cell);
		cell.setRotation(90);
        tbl.addCell(cell);
        
        cell = new PdfPCell(new Phrase("godzin"+ Chunk.NEWLINE+"w semestrze", subjectTableFont));
        PdfUtils.centerCell(cell);
		cell.setRotation(90);
        tbl.addCell(cell);
	}
	
	PdfPTable SubjectTableHeader() {
		PdfPTable tbl = new PdfPTable(14);
		tbl.setSpacingBefore(2);
		tbl.setWidthPercentage(100);
		try {
			tbl.setWidths(new float[] { 0.5f, 4, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 });
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		PdfPCell cell = new PdfPCell(new Phrase("L.p.", subjectTableFont));
		PdfUtils.centerCell(cell);
		cell.setRowspan(2);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Przedmiot", subjectTableFont));
		PdfUtils.centerCell(cell);
		cell.setRowspan(2);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Studia stacjonarne", subjectTableFont));
		PdfUtils.centerCell(cell);
		cell.setColspan(6);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Studia niestacjonarne", subjectTableFont));
		PdfUtils.centerCell(cell);
		cell.setColspan(6);
		tbl.addCell(cell);
		
		addSubjectTableHeaderEntries(tbl);
		addSubjectTableHeaderEntries(tbl);       

        tbl.completeRow();
        return tbl;
	}
	
	PdfPTable createDutyTable() {
		PdfPTable tbl = new PdfPTable(2);
		tbl.setSpacingBefore(2);
		tbl.setWidthPercentage(100);
		try {
			tbl.setWidths(new float[] { 0.5f, 4});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		PdfPCell cell = new PdfPCell(new Phrase("L.p.", subjectTableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase("Rodzaj obowiązków", subjectTableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		return tbl;
	}
	
	@Override
	public
	boolean Generate(String fileName) {
		boolean ret = true;
		try {
			PdfWriter.getInstance(doc, new FileOutputStream(fileName));
			doc.open();
			
			Image i = LogoImage();
			if (null != i) {
				doc.add(i);
			}
			doc.add(HeaderText());
			doc.add(InfoTable());
			
			doc.add( Chunk.NEWLINE );
			
			doc.add(TableHeader(tableTitleBegin + " zimowy"));
			PdfPTable tblw = SubjectTableHeader();
			addSubjectsToTable(tblw, winter);
			addWinterSummaryRow(tblw);
			doc.add(tblw);
			
			doc.add( Chunk.NEWLINE );
			
			doc.add(TableHeader(tableTitleBegin + " letni"));
			PdfPTable tbls = SubjectTableHeader();
			addSubjectsToTable(tbls, summer);
			addSummerSummaryRow(tbls);
			doc.add(tbls);
			
			doc.add( Chunk.NEWLINE );
			
			doc.add(TableHeader(nondidacticTblHdr));
			PdfPTable tblNond = createDutyTable();
			addDuties(tblNond, ee.getNondidactical());
			doc.add(tblNond);
			
			doc.add( Chunk.NEWLINE );
			
			doc.add(TableHeader(organizationalTblHdr));
			PdfPTable tblOrg = createDutyTable();
			addDuties(tblOrg, ee.getOrganizational());
			doc.add(tblOrg);
			
			doc.add( Chunk.NEWLINE );
			
			doc.add(TableHeader(miscTblHdr));
			PdfPTable tblMisc = createDutyTable();
			addDuties(tblMisc, ee.getMiscActivities());
			doc.add(tblMisc);
			
			doc.add( Chunk.NEWLINE );
			
			doc.add(createSignLines());
		} catch (Exception e) {
			logger.fatal("Raport indywidualny blad: {}", e.getMessage());
			ret = false;
		} finally {
			doc.close();
		}
		return ret;
	}
	
	private PdfPTable createSignLines() {
		PdfPTable tbl = new PdfPTable(5);
		tbl.setSpacingBefore(2);
		tbl.setWidthPercentage(100);
		try {
			tbl.setWidths(new float[] {1.5f, 0.5f, 3, 0.5f, 2});
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		PdfPCell cell = new PdfPCell(new Phrase(DateAndSignTxt, subjectTableFont));
		cell.setBorder(Rectangle.TOP);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_TOP);
		cell.setBorderWidth(0.5f);
		tbl.addCell(cell);
		
		cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		tbl.addCell(cell);
		
		cell = new PdfPCell(
				new Phrase(
				DateAndSignDepartmentHeadMasterTxt,
				subjectTableFont
				));
		
		cell.setBorder(Rectangle.TOP);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_TOP);
		cell.setBorderWidth(0.5f);
		tbl.addCell(cell);
		
		cell = new PdfPCell();
		cell.setBorder(Rectangle.NO_BORDER);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase(AcceptSignTxt, subjectTableFont));
		cell.setBorder(Rectangle.TOP);
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setVerticalAlignment(Element.ALIGN_TOP);
		cell.setBorderWidth(0.5f);
		tbl.addCell(cell);
		
		return tbl;
	}

	private void addDuties(PdfPTable tbl, List<EmployeeActivity> duties) {
		if (duties.isEmpty()) {
			addEmptyDutyRow(tbl);
			return;
		}
		
		int index = 1;
		for (EmployeeActivity ea : duties) {
			PdfPCell cell = new PdfPCell(new Phrase(String.valueOf(index), subjectTableFont));
			tbl.addCell(cell);
			cell = new PdfPCell(new Phrase(ea.getName(), subjectTableFont));
			tbl.addCell(cell);
			index += 1;
		}
	}

	private void addEmptyDutyRow(PdfPTable tbl) {	
		PdfPCell cell = new PdfPCell(new Phrase("1.", subjectTableFont));
		tbl.addCell(cell);
		cell = new PdfPCell();
		tbl.addCell(cell);
	}
	
	private void addEmptySubjectRow(PdfPTable tbl) {
		tbl.addCell(new PdfPCell(new Phrase("1.", subjectTableFont)));
		for (int i = 0; i < 13; i++) {
			tbl.addCell(new PdfPCell(new Phrase(" ")));
		}
	}

	void addSubjectsToTable(PdfPTable tbl, List<SubjectEntry> subjects) {
		if (subjects.isEmpty()) {
			addEmptySubjectRow(tbl);
			return;
		}
		
		int index = 1;
		while (!subjects.isEmpty()) {
			SubjectEntry se = subjects.get(0);
			SubjectEntry s1 = null;
			
			addSubjectNameToTable(tbl, se, index);
			
			if (se.getSubType() == SubjectType.FULLTIME) {
				s1 = getPartTimeSub(se);
			} else {
				s1 = getFullTimeSub(se);
			}
			
			if (s1 != null) {
				addSubs(tbl, se, s1);
				subjects.remove(se);
				subjects.remove(s1);
			} else {
				addSingleSub(tbl, se);
				subjects.remove(se);
			}
			index += 1;
		}
	}
	
	void addSingleSub(PdfPTable tbl, SubjectEntry se) {
		if (se.getSubType() == SubjectType.FULLTIME) {
			addSubjectDataToTable(tbl, se);
			addEmptySubCells(tbl);
		} else {
			addEmptySubCells(tbl);
			addSubjectDataToTable(tbl, se);   
		}
	}
	
	void addSubs(PdfPTable tbl, SubjectEntry s1, SubjectEntry s2) {
		if (s1.getSubType() == SubjectType.FULLTIME) {
			addSubjectDataToTable(tbl, s1);
			addSubjectDataToTable(tbl, s2);
		} else {
			addSubjectDataToTable(tbl, s2);
			addSubjectDataToTable(tbl, s1);   
		} 	
	}
	
	void
	addWinterSummaryRow(PdfPTable tbl) {
		summaryRow(tbl,
				" zimowego",
				sta.getFullTimeWinterSemesterHours(),
				sta.getPartTimeWinterSemesterHours());
	}
	
	void
	addSummerSummaryRow(PdfPTable tbl) {
		summaryRow(tbl,
				" letniego",
				sta.getFullTimeSummerSemesterHours(),
				sta.getPartTimeSummerSemesterHours());
	}
	
	void summaryRow(PdfPTable tbl, String txt, long x, long y) {
		PdfPCell cell = new PdfPCell(new Phrase(summaryRowTxt + txt, subjectTableFont));
		PdfUtils.centerCell(cell);
		cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell.setColspan(7);
		misc.PdfUtils.noBorder(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase(String.valueOf(x),
				subjectTableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell();
		cell.setColspan(5);
		misc.PdfUtils.noBorder(cell);
		tbl.addCell(cell);
		
		cell = new PdfPCell(new Phrase(String.valueOf(y),
				subjectTableFont));
		PdfUtils.centerCell(cell);
		tbl.addCell(cell);
	}
}
