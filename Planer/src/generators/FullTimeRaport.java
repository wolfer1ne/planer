package generators;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import data.DepartmentEntry;
import data.primitives.SubjectType;

public class FullTimeRaport extends SubjectRaport {
	private static Logger logger = LogManager.getLogger();

	public FullTimeRaport(DepartmentEntry de, String year) {
		super(de, year, SubjectType.FULLTIME);
	}
}