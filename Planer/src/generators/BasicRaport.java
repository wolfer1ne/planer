package generators;

import com.itextpdf.text.Document;

public abstract class BasicRaport {
	String fileNameOutputFormat; // format nazwy pliku wyjściowego
	String raportName; // nazwa generatora raportu
	
	Document doc;
	
	public BasicRaport(String fileNameOutputFormat, String raportName) {
		super();
		this.fileNameOutputFormat = fileNameOutputFormat;
		this.raportName = raportName;
	}
	
	abstract boolean Generate(String fileName);
	abstract void InitFonts();
}