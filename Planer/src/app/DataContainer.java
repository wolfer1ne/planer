package app;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import data.YearEntry;

public class DataContainer {
	private static Logger logger = LogManager.getLogger(DataContainer.class.getName());
	List <YearEntry> years = new ArrayList<YearEntry>();

	public boolean addYear(String year)
	throws IllegalArgumentException {
		if (null == year) return false;
		for (YearEntry ye : years) {
			if (ye.getName().equals(year)) 
				throw new IllegalArgumentException("Rok " + year + " już istnieje");
		}
		return years.add(new YearEntry(year));
	}
	
	public YearEntry getYear(String year)
	throws IllegalArgumentException {
		if (null == year) return null;
		for (YearEntry ie : years) {
			if (ie.getName().equals(year)) 
				return ie;
		}
		throw new IllegalArgumentException("Rok " + year + " nie istnieje");
	}
	
	public void newData() {
		years.clear();
		logger.info("clear; dlugosc: {}", this.getYearsAmmount());
	}
	
	public static boolean saveData(String fileName, DataContainer dc) throws IOException {
		Writer writer = null;
		BufferedWriter out = null;
		try {
			writer = new OutputStreamWriter(new FileOutputStream(fileName), StandardCharsets.UTF_8);
		//try (Writer writer = new FileWriter(fileName)) {
		    Gson gson = new GsonBuilder()
		    		.enableComplexMapKeySerialization()
		    		.setPrettyPrinting()
		    		.create();
		    gson.toJson(dc, writer);
		    writer.close();
		} catch (IOException e) {
			throw e;
		}
		return true;	
	}
	
	public static DataContainer loadData(String fileName)
	throws FileNotFoundException {
		BufferedReader br = null;
		//br = new BufferedReader(new FileReader(fileName));
		try {
			br = new BufferedReader(new InputStreamReader(
				    new FileInputStream(fileName), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Gson gson = new Gson();
		return gson.fromJson(br, DataContainer.class);
	}

	public List<YearEntry> getYears() {
		return years;
	}

	public int getYearsAmmount() {
		return years.size();
	}

	public boolean checkIfExists(String text) {
		for (YearEntry ye : years) {
			if (ye.getName().equals(text)) return true;
		}
		return false;
	}
}
