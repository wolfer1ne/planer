package app;

import data.DepartmentEntry;
import data.EmployeeEntry;
import data.InstituteEntry;
import data.SpecializationEntry;
import data.YearEntry;
import data.primitives.SemesterNumber;
import data.primitives.SemesterType;
import data.primitives.SubjectForm;

public interface DataAdder {
	public boolean addStudyYear (String val);
	public boolean addInstitute (YearEntry year, String name);
	public boolean addDepartment (InstituteEntry institute, String name);
	
	public boolean addSubject (DepartmentEntry department, String name);
	public boolean addSpecialization (DepartmentEntry department, String name);
	
	public boolean addEmployee (String name, String surname, String degree,
			DepartmentEntry department, String position, 
			String staffResource, String jobTime);
	
	public boolean addSubjectEntry (DepartmentEntry department,
			EmployeeEntry employee, SpecializationEntry spec,
			String name, SemesterType semType, SemesterNumber semNum,
			SubjectForm subForm, long groupsAmmount, long studentsAmmount,
			long hoursWeekly);
}
