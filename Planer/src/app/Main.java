package app;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import misc.PdfUtils;
import models.planerTree;
import views.frmMain;
import views.pnlEmployeeOverview;

public class Main {
	private static final Logger logger = LogManager.getLogger(Main.class.getName());

	static public DataContainer dc = new DataContainer();
	public static frmMain frame = new frmMain();
	public static planerTree t;
	
	public static void main(String[] args) {
		logger.info("start aplikacji");
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PdfUtils.registerFonts();
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					frmMain frame = new frmMain();
					//DataContainer dc = new DataContainer();
					//dc = DataContainer.loadData("TEST3.json");
					//planerTree t = new planerTree(dc, frame.splitPane);
					t = new planerTree(dc, frame.splitPane);
					frame.splitPane.setLeftComponent(t.getTree());
					t.refresh();
					frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}