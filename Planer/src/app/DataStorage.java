package app;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import data.DepartmentEntry;
import data.EmployeeEntry;
import data.InstituteEntry;
import data.SpecializationEntry;
import data.YearEntry;
import data.primitives.SemesterNumber;
import data.primitives.SemesterType;
import data.primitives.SubjectForm;

public class DataStorage implements DataAdder {
	private static final Logger logger = LogManager.getLogger(DataStorage.class.getName());
	List <YearEntry> years = new ArrayList<YearEntry>();
	
	@Override
	public boolean addStudyYear(String val) {
			YearEntry ye = new YearEntry(val);
			logger.info("Dodawanie nowego roku: {}", ye.getName());
			return years.add(ye);
	}

	@Override
	public boolean addInstitute(YearEntry year, String name) {
			logger.info("Dodawanie instytutu {} do roku {}", name, year.getName());
			return year.addInsitute(name);
	}

	@Override
	public boolean addDepartment(InstituteEntry institute, String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addSubject(DepartmentEntry department, String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addSpecialization(DepartmentEntry department, String name) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addEmployee(String name, String surname, String degree, DepartmentEntry department, String position,
			String staffResource, String jobTime) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean addSubjectEntry(DepartmentEntry department, EmployeeEntry employee, SpecializationEntry spec,
			String name, SemesterType semType, SemesterNumber semNum, SubjectForm subForm, long groupsAmmount,
			long studentsAmmount, long hoursWeekly) {
		// TODO Auto-generated method stub
		return false;
	}
}
