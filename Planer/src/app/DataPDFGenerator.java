package app;

public interface DataPDFGenerator {
	// Tabela zbiorcza
	public boolean generateSummaryTable (String fileName);
	
	// Tabela indywidualna
	public boolean generateIndividualTable (String name, String surname);
	
	// Przedmioty stacjonarne
	public boolean generateFullTimeStudiesTable (String department);
	
	// Przedmioty niestacjonarne
	public boolean generatePartTimeStudiesTable (String department);
	
	// Plan zatrudnienia wg pracownikow
	public boolean generateEmployeesDepartmentPlanTable (String department); 
}
