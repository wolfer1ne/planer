package tests;

import java.io.FileNotFoundException;
import java.io.IOException;

import app.DataContainer;
import data.DepartmentEntry;
import data.EmployeeEntry;
import data.InstituteEntry;
import data.SubjectEntry;
import data.YearEntry;
import data.primitives.SemesterNumber;
import data.primitives.SemesterType;
import data.primitives.StudyYear;
import data.primitives.SubjectForm;
import data.primitives.SubjectType;

public class AddBasicEntries {
	public static void main(String[] args) {
		DataContainer container = new DataContainer();
		try {
			container.addYear("2018");
			container.addYear("2019");
			container.addYear("2018"); // THROW
		} catch (IllegalArgumentException iae) {
			System.out.println(iae.toString());
		}
		
		YearEntry ye = null;
		try {
			ye = container.getYear("2018");
			ye.addInstitute("Humanistyczny");
			ye.addInstitute("Politechniczny");
			ye.addInstitute("Zdrowia i Gospodarki");
			ye.addInstitute("Politechniczny"); // THROW
		} catch (IllegalArgumentException iae) {
			System.out.println(iae.toString());
		}
		
		try {
			InstituteEntry ie = ye.getInstitute("HUMANISTYCZNY"); // THROW
		} catch (IllegalArgumentException iae) {
			System.out.println(iae.toString());
		}
		
		try {
			InstituteEntry ie = ye.getInstitute("Humanistyczny");
			ie.addDepartment("Dwujęzyczny studia dla tłumaczy");
			ie.addDepartment("Filologia angielska");
			ie.addDepartment("Pedagogika");
			ie.addDepartment("Polonistyka");
			ie.addDepartment("Międzynarodowa komunikacja językowa");
			ie.addDepartment("Pedagogika"); // THROW
		} catch (IllegalArgumentException iae) {
			System.out.println(iae.toString());
		}
		
		try {
			InstituteEntry ie = ye.getInstitute("Politechniczny");
			ie.addDepartment("Budownictwo");
			ie.addDepartment("Energetyka");
			ie.addDepartment("Górnictwo i geologia");
			ie.addDepartment("Informatyka");
			ie.addDepartment("Inżynieria środowiska");
			ie.addDepartment("Mechanika i budowa maszyn");
			ie.addDepartment("Zarządzanie");
			ie.addDepartment("Zarządzanie"); // THROW
		} catch (IllegalArgumentException iae) {
			System.out.println(iae.toString());
		}
		
		try {
			InstituteEntry ie = ye.getInstitute("Zdrowia i Gospodarki");
			ie.addDepartment("Produkcja i bezpieczeństwo żywności");
			ie.addDepartment("Pielęgniarstwo");
			ie.addDepartment("Rolnictwo");
			ie.addDepartment("Towaroznawstwo");
			ie.addDepartment("Turystyka i rekreacja");
			ie.addDepartment("Wychowanie fizyczne");
			ie.addDepartment("Zielarstwo");
			ie.addDepartment("Rolnictwo"); // THROW
		} catch (IllegalArgumentException iae) {
			System.out.println(iae.toString());
		}
		
		
		try {
			YearEntry y = container.getYear("2018");
			InstituteEntry i = y.getInstitute("Politechniczny");
			DepartmentEntry d = i.getDepartment("Informatyka");
			
			d.addSpecialization("Sieciowe systemy informacyjne");
			d.addSpecialization("Bezpieczeństwo systemów informatycznych");
			d.addSpecialization("Technologie internetowe i bazy danych");
			d.addSpecialization("Informatyka praktyczna");
			d.addSpecialization("Informatyka praktyczna"); // THROW
		} catch (IllegalArgumentException iae) {
			System.out.println(iae.toString());
		}
		
		try {
			YearEntry y = container.getYear("2018");
			InstituteEntry i = y.getInstitute("Politechniczny");
			DepartmentEntry d = i.getDepartment("Informatyka");
			
			try {
				d.addEmployee("Krystian","Jedziniak", "inż.", "nauczyciel", "380", "380");
			} catch (IllegalArgumentException iae) {
				iae.printStackTrace();
			}
			
			EmployeeEntry ee = d.getEmployee("Krystian", "Jedziniak");
			SubjectEntry se = new SubjectEntry("Programowanie");
			se.setSubType(SubjectType.FULLTIME);
			se.setSubForm(SubjectForm.C);
			se.setSemType(SemesterType.WINTER);
			se.setStudyYear(StudyYear.I);
			se.setSemNum(SemesterNumber.I);
			ee.addSubject(se);
		} catch (IllegalArgumentException | NullPointerException iae) {
			System.out.println(iae.toString());
			System.out.println(iae.getCause().toString());
			System.out.println(iae.getCause().getMessage());
		}
		
		
		System.out.println(ye.toString());
		
		try {
			DataContainer.saveData("TEST3.json", container);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		/*
		 *
		try {
			container = DataContainer.loadData("TEST.json");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(container.getYear("2018").toString());
		*/
	}
}