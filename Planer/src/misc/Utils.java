package misc;

import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.UIManager;
import javax.swing.filechooser.FileNameExtensionFilter;

import data.SpecializationEntry;
import data.primitives.EmployeeActivity;
import data.primitives.EmployeeActivityType;
import data.primitives.SemesterNumber;
import data.primitives.SemesterType;
import data.primitives.StudyYear;

public class Utils {
	public static final String EMPTY_SPEC = "BRAK";
	public static final String LOGO_FILE_NAME = "pwsz-logo.png";
	
	public static SemesterType SemNumToSemType(SemesterNumber n) {
		if (SemesterNumber.I == n
			|| SemesterNumber.III == n
			|| SemesterNumber.V == n
			|| SemesterNumber.VII == n) {
			return SemesterType.WINTER;
		}
		return SemesterType.SUMMER;
	}

	public static EmployeeActivity makeNondidacticActivity (String v) {
		return new EmployeeActivity(v, EmployeeActivityType.NONDIDACTIC);
	}
	
	public static EmployeeActivity makeOrganizationalActivity (String v) {
		return new EmployeeActivity(v, EmployeeActivityType.ORGANIZATIONAL);
	}
	
	public static EmployeeActivity makeMiscActivity (String v) {
		return new EmployeeActivity(v, EmployeeActivityType.MISC);
	}
	
	public static StudyYear SemToYear(SemesterNumber sn) {
		switch (sn) {
			case I:
			case II: return StudyYear.I;
			case III:
			case IV: return StudyYear.II;
			case V:
			case VI: return StudyYear.III;
			case VII: return StudyYear.IV;
			default: throw new IllegalArgumentException();
		}
	}
	
	public static SpecializationEntry getEmptySpecializationEntry() {
		return new SpecializationEntry(EMPTY_SPEC);
	}
	
	public static
	Icon getDefaultRemoveIcon()
	{
		return UIManager.getIcon("OptionPane.errorIcon");
	}
	
	public static
	Icon getDefaultEditIcon()
	{
		return UIManager.getIcon("OptionPane.questionIcon");
	}
	
	public static void
	removeListeners(JButton btn)
	{
		ActionListener[] al = btn.getActionListeners();
		for (ActionListener a : al) {
			btn.removeActionListener(a);
		}
	}
	
	public static
	JFileChooser getPdfSaveChooser()
	{
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogType(JFileChooser.SAVE_DIALOG);
		chooser.setFileFilter(new FileNameExtensionFilter("PDF File","pdf"));
		return chooser;
	}
	
	public static
	String prepareDepartment(String depName) {
		if (depName.endsWith("yka")) {
			return replaceLast(depName, "yka", "yki");
		}
		return depName;
	}
	
	private static String replaceLast(String string, String from, String to) {
	     int lastIndex = string.lastIndexOf(from);
	     if (lastIndex < 0) return string;
	     String tail = string.substring(lastIndex).replaceFirst(from, to);
	     return string.substring(0, lastIndex) + tail;
	}
}
