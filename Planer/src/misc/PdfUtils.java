package misc;

import com.itextpdf.text.Element;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;

public final class PdfUtils {
	public static void centerCellHorizotal(PdfPCell c) {
		c.setHorizontalAlignment(Element.ALIGN_CENTER);
	}
	
	public static void centerCellVertical(PdfPCell c) {
		c.setVerticalAlignment(Element.ALIGN_MIDDLE);
	}
	
	public static void centerCell(PdfPCell c) {
		PdfUtils.centerCellHorizotal(c);
		PdfUtils.centerCellVertical(c);
	}
	
	public static void noBorder(PdfPCell c) {
		c.setBorder(Rectangle.NO_BORDER);
	}
	
	public static void registerFonts() {
		FontFactory.register("c:/windows/fonts/garabd.ttf", "garamond bold");
	}
	
	public static void addEmptyRow(PdfPTable tbl, int cols) {
		for (int i = 0; i < cols; i++) {
			tbl.addCell(new Phrase(" "));
		}
	}
}
