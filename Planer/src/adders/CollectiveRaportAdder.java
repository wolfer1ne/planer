package adders;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import data.EmployeeEntry;

public class CollectiveRaportAdder {
	private static Logger logger = LogManager.getLogger();
	
	long sumJobTime;
	long sumFullTimeStudies;
	long sumPartTimeStudies;
	long sumOverFullTimeStudies;
	long sumOverPartTimeStudies;
	
	public CollectiveRaportAdder() {}
	
	public CollectiveRaportAdder(List <EmployeeEntry> employees) {
		for (EmployeeEntry ee : employees) {
			StudiesTimeAdder sta = new StudiesTimeAdder(ee.getSubjects());
			
			sumFullTimeStudies += sta.getFullTimeStudies();
			sumPartTimeStudies += sta.getPartTimeStudies();
			
			sumJobTime += ee.getJobTime();
		}
		checkOverHours();
	}
	
	void checkOverHours() {
		if (sumFullTimeStudies < sumJobTime) {
			sumOverFullTimeStudies = 0;
		} else {
			sumOverFullTimeStudies = sumFullTimeStudies - sumJobTime;
		}
		
		/*if (sumPartTimeStudies < sumJobTime) {
			sumOverPartTimeStudies = 0;
		} else {
			sumOverPartTimeStudies = sumPartTimeStudies - sumJobTime;
		}*/
	}
	
	public long sum() {
		return sumFullTimeStudies + sumPartTimeStudies;
	}

	public long getSumJobTime() {
		return sumJobTime;
	}

	public long getSumFullTimeStudies() {
		return sumFullTimeStudies;
	}

	public long getSumPartTimeStudies() {
		return sumPartTimeStudies;
	}

	public long getSumOverFullTimeStudies() {
		return sumOverFullTimeStudies;
	}

	public long getSumOverPartTimeStudies() {
		return sumOverPartTimeStudies;
	}

	public void add(StudiesTimeAdder sta) {
		sumJobTime += sta.getJobTime();
		sumFullTimeStudies += sta.getFullTimeStudies();
		sumPartTimeStudies += sta.getPartTimeStudies();
		sumOverFullTimeStudies += sta.getSumOverFullTimeStudies();
		sumOverPartTimeStudies += sta.getSumOverPartTimeStudies();
	}

}
