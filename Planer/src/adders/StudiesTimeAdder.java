package adders;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import data.EmployeeEntry;
import data.SubjectEntry;
import data.primitives.SemesterType;
import data.primitives.SubjectType;

public class StudiesTimeAdder {
	private static Logger logger = LogManager.getLogger();
	
	long jobTime;
	
	long fullTimeStudies;
	long partTimeStudies;
	
	long fullTimeWinterSemesterHours;
	long partTimeWinterSemesterHours;
	
	long fullTimeSummerSemesterHours;
	long partTimeSummerSemesterHours;
	
	long sumOverFullTimeStudies;
	long sumOverPartTimeStudies;
	
	public StudiesTimeAdder() {}
	
	public StudiesTimeAdder(List<SubjectEntry> subjects) {
		for (SubjectEntry se : subjects) {
			if (se.getSemType() == SemesterType.WINTER) {
				if (se.getSubType() == SubjectType.FULLTIME) {
					fullTimeWinterSemesterHours += se.getHoursSemester();
				} else {
					partTimeWinterSemesterHours += se.getHoursSemester();
				}
			} else {
				if (se.getSubType() == SubjectType.PARTTIME) {
					partTimeSummerSemesterHours += se.getHoursSemester();
				} else {
					fullTimeSummerSemesterHours += se.getHoursSemester();
				}
			}
		}
	}
	
	public StudiesTimeAdder(EmployeeEntry ee) {
		this(ee.getSubjects());
		
		jobTime += ee.getJobTime();
		
		if (getFullTimeStudies() < jobTime) {
			sumOverFullTimeStudies = 0;
		} else {
			sumOverFullTimeStudies = getFullTimeStudies() - jobTime;
		}

		sumOverPartTimeStudies = getPartTimeStudies();
		logger.debug("\t{}:{}", sumOverFullTimeStudies, sumOverPartTimeStudies);
	}
	
	public long getJobTime() {
		return jobTime;
	}
	
	public long getSumOverFullTimeStudies() {
		return sumOverFullTimeStudies;
	}

	public long getSumOverPartTimeStudies() {
		return sumOverPartTimeStudies;
	}
	public long getSum() {
		return getFullTimeStudies() + getPartTimeStudies();
	}
	public long getFullTimeStudies() {
		return fullTimeWinterSemesterHours + fullTimeSummerSemesterHours;
	}
	public long getPartTimeStudies() {
		return partTimeWinterSemesterHours + partTimeSummerSemesterHours;
	}
	public long getFullTimeWinterSemesterHours() {
		return fullTimeWinterSemesterHours;
	}
	public long getPartTimeWinterSemesterHours() {
		return partTimeWinterSemesterHours;
	}
	public long getFullTimeSummerSemesterHours() {
		return fullTimeSummerSemesterHours;
	}
	public long getPartTimeSummerSemesterHours() {
		return partTimeSummerSemesterHours;
	}
}
